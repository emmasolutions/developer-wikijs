---
title: React Native Integration
description: 
published: true
date: 2024-12-21T11:52:05.366Z
tags: sdk, react, react-native
editor: markdown
dateCreated: 2022-01-26T09:59:04.412Z
---

# Installation
Installation using Yarn or NPM:
- Yarn: `yarn add emma-react-native-sdk`
- NPM: `npm install emma-react-native-sdk`

In case the RN version is lower than 0.60 (if it is higher the linking is automatic):
- Run the following command to link EMMA to the project `react-native link emma-react-native-sdk`.
- If the above command does not work you have to link EMMA to the project manually. Drag the `.xcodeproj` file from the `node_modules/emma-react-native-sdk/ios/` path and drop it in the Xcode Libraries folder (you need to have the project open in Xcode for this).

# Android integration
It is important that in `AndroidManifest.xml` the `MainActivity` activity has as attribute `launchMode="singleTask"`:

```xml
<activity
    android:name=".MainActivity"
    android:label="@string/app_name"
    android:configChanges="keyboard|keyboardHidden|orientation|screenSize|uiMode"
    android:launchMode="singleTask"
    android:windowSoftInputMode="adjustResize">
```
## Push

To integrate the push you have to add the EMMA push service in the `AndroidManifest.xml` of the Android app, inside the `<application>` tag, this service is the one that mounts and displays the push notification:

```xml
<service
    android:name="io.emma.android.push.EMMAFcmMessagingService"
    android:enabled="true"
    android:exported="false">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT"/>
    </intent-filter>
</service>
```
Indicates the use of the `google-services` plugin in `android/buidl.gradle`:

```groovy
buildscript {
    ....
    repositories {
        ....
        google()
    }
    dependencies {
       ...
        classpath 'com.google.gms:google-services:4.3.15'
    }
}
```

Once the service is included we add the push dependencies and apply the `google-services` plugin in the `android/app/build.gradle` file:

```groovy
dependencies {
    ....
    implementation 'com.google.firebase:firebase-messaging:21.0.1'
}

apply plugin: 'com.google.gms.google-services'
```

Important to add the `google-services.json` obtained in the [Firebase](https://console.firebase.google.com/) console in the `android/app/` path.

For more information about the push go [here](https://developer.emma.io/en/android/integracion-sdk#push-notification-integration)

## POWLINK y Deeplinking

To use POWLINK or Deeplinking in Android it is necessary to add the following activity to the `AndroidManifest.xml` of the app, inside `<application>`:

```xml
  <activity
            android:name="io.emma.android.activities.EMMADeepLinkActivity"
            android:noHistory="true"
            android:theme="@android:style/Theme.NoDisplay"
						android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.VIEW"/>
                
                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.BROWSABLE"/>

                <data android:scheme="${DEEPLINK_SCHEME}"/>
            </intent-filter>

            <intent-filter android:autoVerify="true">
                <action android:name="android.intent.action.VIEW"/>

                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.BROWSABLE"/>

                <data
                    android:host="${CUSTOM_POWLINK_DOMAIN}.powlink.io"
                    android:scheme="https"/>

                <data
                    android:host="${CUSTOM_SHORT_POWLINK_DOMAIN}.pwlnk.io"
                    android:scheme="https"/>

            </intent-filter>
        </activity>
```
- Replace ${DEEPLINK_SCHEME} with the deeplink scheme of the app, for example emmarn (from emmarn://).
- Replace ${CUSTOM_POWLINK_DOMAIN} and ${CUSTOM_SHORT_POWLINK_DOMAIN} by the domains configured in the EMMA dashboard app preferences section.

For more information about POWLINK domains go [here](https://developer.emma.io/en/android/integracion-sdk#powlink-integration). 

Under the activity, in the same `AndroidManifest.xml`, you must also add the following `<meta-data>` (replacing PACKAGE_NAME by the path where MainActivity is):

```xml
<meta-data
    android:name="io.emma.DEEPLINK_OPEN_ACTIVITY"
    android:value="${PACKAGE_NAME}.MainActivity"/>
            
```

# iOS Integration

To download and install EMMA and its dependencies you have to run `pod install` in the `ios/` path of the app. If the project does not include `CocoaPods` it can be linked manually as explained in the Installation section of this article.

## Push

To integrate push notifications in iOS it is necessary to activate the `Capability > Push Notification`.

![ios_push_notification_capabilities](/react-native/ios_push_notification_capabilities.png)

Add in the `AppDelegate.m` or `AppDelegate.swift` the EMMA bridge for React Native.

### Tabs {.tabset}
#### AppDelegate.m

```objc
#import "AppDelegate.h"
	
// EMMA bridge import
#import <emma-react-native-sdk/EmmaReactNative.h>
```

#### AppDelegate.swift

```swift
// EMMA bridge import
import emma_react_native_sdk
```
###

Add in the same file the method to notify the delegate to the EMMA bridge and add the delegate methods to receive the notifications.

### Tabs {.tabset}
#### AppDelegate.m

```objc
#import AppDelegate.h

// EMMA bridge import
#import <emma-react-native-sdk/EmmaReactNative.h>


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

  //RCT Bridge initialization

  // Pass push delegate to EMMA bridge
  if (@available(iOS 10.0, *)) {
    [EmmaReactNative setPushNotificationsDelegate:self];
  }

  return YES;
}

//MARK: EMMA - Push methods
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
  [EmmaReactNative registerToken:deviceToken];
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler  API_AVAILABLE(ios(10.0)){
  [EmmaReactNative didReceiveNotificationResponse:response withActionIdentifier:response.actionIdentifier];
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler  API_AVAILABLE(ios(10.0)){
  [EmmaReactNative willPresentNotification:notification];
}


@end

```

#### AppDelegate.swift

```swift
import UIKit
import UserNotifications

// EMMA bridge import
import emma_react_native_sdk

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, RCTBridgeDelegate, UNUserNotificationCenterDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    //RCT Bridge initialization

    // Pass push delegate to EMMA bridge
    if #available(iOS 10.0, *) {
      EmmaReactNative.setPushNotificationsDelegate(self)
    }

    return true
  }

  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    EmmaReactNative.registerToken(deviceToken)
  }

  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    NSLog("Error registering notifications " + error.localizedDescription);
  }

  @available(iOS 10.0, *)
  func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
    EmmaReactNative.willPresent(notification)
    completionHandler([.badge, .sound])
  }

  @available(iOS 10.0, *)
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
     EmmaReactNative.didReceive(response, withActionIdentifier: response.actionIdentifier)
     completionHandler()
  }
}
```

## Multimedia content in push notifications

If we want the notifications to have multimedia content it is necessary to create an extension as explained in the following steps:

1. Create the new extension by adding it as target.

	![notification_extension_creation](/react-native/notification_extension_creation.png)

2. Add the name of the extension and click finish. In the alert that appears click on `cancel`.

	![notification_creation_name](/react-native/notification_creation_name.png)
	
3. Cancel the debug extension activation.
	
	![notification_extension_cancel](/react-native/notification_extension_cancel.png)

4. In the extension target select the minimum supported version, in the case of notification extension it would be iOS 10.0.

	![notification_extension_target](/react-native/notification_extension_target.png)
	

Now we have to link the EMMA dependency to the notification target in the `Podfile` file, this file is located in the `ios/` path of the project.

```ruby
target 'NotificationService' do
    pod 'eMMa', '~> 4.11.4'
end
```
Run `pod install` again in the `ios/` path to link the new dependency.

Finally, open the project with the generated `.xcworkspace` and in the `NotificationService.swift` file add the following code:

```swift
import UIKit
import UserNotifications
import EMMA_iOS

class NotificationService: UNNotificationServiceExtension {
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            EMMA.didReceiveNotificationRequest(request: request, withNotificationContent: bestAttemptContent) { (content) in
                contentHandler(bestAttemptContent)
            }
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
}
```

Finally, you just need to add the credentials of the app to be able to send notifications. To do so, go to the following [link](https://support.emma.io/hc/es/articles/115002811374-Integraci%C3%B3n-iOS#push).

## POWLINK and Deeplinking

Add in the `AppDelegate.m` or `AppDelegate.swift` the EMMA bridge for RN and the RN Linking library. If you are using Swift in the app, the Linking library is implemented in objc so it is necessary to add it in the `App-Bridging-Header.h` file created to bridge objc and Swift.

### Tabs {.tabset}
#### AppDelegate.h

```objc
#import "AppDelegate.h"

#import <React/RCTLinkingManager.h>
// EMMA bridge import
#import <emma-react-native-sdk/EmmaReactNative.h>

```

#### AppDelegate.swift

```swift
// EMMA bridge import
import emma_react_native_sdk

```
#### App-Bridging-Header.h

```objc
// Imports for EMMA
#import <React/RCTLinkingManager.h>
```
###

Add the following methods to `AppDelegate.m` or `AppDelegate.swift`.
- The `openURL` method collects all the deeplinks that open the application.
- The `continueUserActivity` method collects the Universal Links that open the application.

### Tabs {.tabset}
#### AppDelegate.m

```objc
#import AppDelegate.h

#import <React/RCTLinkingManager.h>
// EMMA bridge import
#import <emma-react-native-sdk/EmmaReactNative.h>


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

  //RCT Bridge initialization

  return YES;
}

//MARK: EMMA - Deeplinking
- (void)openURL:(NSURL*)url options:(NSDictionary<NSString *, id> *)options completionHandler:(void (^ __nullable)(BOOL success))completion {
  [EmmaReactNative handleLink:url];
  completion([RCTLinkingManager application:[UIApplication sharedApplication] openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]]);
}

//MARK: EMMA - Universal links
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler {
  if (userActivity.webpageURL) {
    [EmmaReactNative handleLink:userActivity.webpageURL];
  }
  return [RCTLinkingManager application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
}

```

#### AppDelegate.swift

```swift
import UIKit

// EMMA bridge import
import emma_react_native_sdk


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, RCTBridgeDelegate, UNUserNotificationCenterDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    //RCT Bridge initialization

    return true
  }

  func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
    EmmaReactNative.handleLink(url)
    return RCTLinkingManager.application(UIApplication.shared, open: url, options: options)
  }

  func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {

    if let webUrl = userActivity.webpageURL {
      EmmaReactNative.handleLink(webUrl)
    }

    return RCTLinkingManager.application(application, continue: userActivity, restorationHandler: restorationHandler)
  }
```
###

To add the POWLINK (Universal Link) go to the `Capabilities` section inside the app target in Xcode and add `Associate domains`, then add the POWLINK with the format `applinks:customsubdomain.powlink.io`, note that customdomain is the subdomain configured in the EMMA Dashboard.

![powlink_adding_capabilities](/react-native/powlink_adding_capabilities.png)

![added_powlink_capabilities](/react-native/added_powlink_capabilities.png)

In the case of the basic deeplink it is necessary to add the schema in the `Info` section inside the app target in Xcode and in `URL Types` add a new entry where you can define the app schema.

![added_urltype_info.png](/react-native/added_urltype_info.png)


For more information about powlink domains and how to configure domains in Dashboard go [here](https://developer.emma.io/en/android/integracion-sdk#powlink-integration).


## Localization

To use localization you need to add the following permissions in the `Info.plist` file of the App:

```xml
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
<key>NSLocationAlwaysUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
<key>NSLocationWhenInUseUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
```

## Requesting the IDFA

To request the IDFA it is necessary to add the following permission to the `Info.plist`:

```xml
<key>NSUserTrackingUsageDescription</key>
	<string>Uso del indentificador para redes de terceros</string>
```


# JS/TS Integration

The examples in this section are made on `tsx` files but are valid for `js` and `ts`.

To import the sdk into the code use the following import:
```typescript
import EmmaSdk from 'emma-react-native-sdk';
```

## Login

In the `App.tsx` file we add:

```typescript
const handleStartSession = async () => {
	console.log('Starting session...');
	try {
	  await EmmaSdk.startSession(startSessionParams);
	  console.log('Session started on', Platform.OS);
	} catch (err) {
	  console.error('Session failed to start', err);
	}
};

useEffect(() => {
	handleStartSession();
}, []);
```
In case you do not use the `useEffect` hook you can use the `componentDidMount` hook.

Configuration parameters for login:

| Paameter  |  Tipe | Description |
|---|---|---|
| *sessionKey*  | string  | Key for identification at login. |
| *apiUrl*  | string (Optional) | Add the url if a proxy is used.   |
| *queueTime*  | int (Optional)  | Time to empty the operation queue.  |
| *isDebug* | boolean (Optional) | Enable/disable logs in the SDK. |
| *customPowlinkDomains* | string[] (Optional) | Add the domains if the app uses custom domains in EMMA (the subdomains that are configured in the app preferences are not custom domains). |
| *customShortPowlinkDomains* | string[] (Optional) |  Add the short domains if the app uses custom domains in EMMA (the subdomains that are configured in the app preferences are not custom domains.)|
| *trackScreenEvents* | boolean (Optional)| Enable/disable automatic screen tracking. | 


## Push

Add the startPush in the `App.tsx` file after the startSession:

```typescript
const handleStartSession = async () => {
	console.log('Starting session...');
	try {
	  await EmmaSdk.startSession(startSessionParams);
	  EmmaSdk.startPush({
        classToOpen: '{PACKAGE_NAME}.MainActivity', //PACKAGE_NAME es el path absoluto de la activity
        iconResource: 'notification_icon',
     });
	  console.log('Session started on', Platform.OS);
	} catch (err) {
	  console.error('Session failed to start', err);
	}
};

useEffect(() => {
	handleStartSession();
}, []);
```
Parameters of the startPush:

| Parameter  |  Tipe | Description |
|---|---|---|
| *classToOpen*  | string  | It indicates the opening activity in the case of clicking on the notification, in RN the normal thing is to have only one main activity therefore we add this one, for it we always indicate it with the absolute path {packageName}.MainActivity. |
| *iconResource*  | string  | The icon that will contain the notification in the status bar and in the notification menu. The SDK looks for this icon in the drawable or drawable-xxx folder, so it is important that the name included in this parameter matches the drawable png.  |
| *color*  | string (Optional)| Color in hexadecimal format.  |
| *channelId* | string (Optional) | If the application uses an existing notification channel, otherwise it will create a new one. |
| *channelName* | string (Optional) |Add the name of the new channel. |

> The use of startPush is valid for Android and iOS, although the parameters are only used in Android since in iOS it takes the default values of the App.

{.is-info}

## POWLINK y Deeplinking

To obtain the deeplink or powlink once the application is open add the following methods:

```typescript
// Listen to deeplink requests
Linking.addEventListener('url', ({ url }) => setDeeplink(url));

// Detect application launchings from a deeplink
const handleInitialDeeplink = () =>
Linking.getInitialURL()
  .then((url) => setDeeplink(url || null))
  .catch(() => setDeeplink(null));
```

## Events

EMMA allows the sending of custom events and several default events, the default event `Open` is sent when logging into the SDK, but the login and registration events are events that must be added to the application code.

### Custom events

To send the events I use the following method:

```typescript
EmmaSdk.trackEvent({
	eventToken: '7b358954cf16bc2b7830bb5307f80f96',
	eventAttributes: { ReactNative: 'true' }, // optional
});
```

### Registration and login

To register and/or login it is necessary to send the user id in the app (Customer ID):

```typescript
const userParams: LoginRegisterUserParams = {
  userId: 'user#12345',
};

const handleLoginUser = () => {
	EmmaSdk.loginUser(userParams);
};

const handleRegisterUser = () => {
	EmmaSdk.registerUser(userParams);
};

```

## User properties

EMMA allows the sending of key/value properties associated with the device:

```typescript
EmmaSdk.trackUserExtraInfo({ userTags: { TAG: 'EMMA_EXAMPLE' } });
```

## Location

To measure the user's location add the following method:

```typescript
EmmaSdk.trackUserLocation();
```

The user's location will only be collected once at login.

## Purchasing

The purchase process consists of several methods: `startOrder`, `addProduct` and `trackOrder`.

```typescript
EmmaSdk.startOrder({
  orderId: 'EMMA',
  totalPrice: 100,
  customerId: 'EMMA',
  currencyCode: 'EUR',
});

// Add products
EmmaSdk.addProduct({
  productId: 'SDK',
  productName: 'SDK',
  quantity: 1,
  price: 1,
  extras: { ReactNative: 'working' },
});

// Commit order
EmmaSdk.trackOrder();
```

In the case of canceling a purchase that has already been made, use the `cancelOrder` method.

```typescript
EmmaSdk.cancelOrder('EMMA');
```

## In-app messaging

EMMA allows the use of various communication formats:

- Adball
- Banner
- Startview
- Strip
- Native Ad

> Banner format is only supported for Android.

{.is-info}

NativeAd format returns a JSON object containing the Native Ad. Example usage:
```typescript
	const type = INAPP_TYPE.NATIVE_AD
	const templateId = 'template1'
   try {
      const result = await EmmaSdk.inAppMessage({ type, templateId });
      // Process Native Ad result
      setNativeAds(result);
      console.log(`InApp ```${type}``` message`, result);
    } catch (err) {
      console.error('InApp message error', err);
    }
```

Any format other than Native Ad does not expect a result, as it is automatically injected into the view. Example usage:

```typescript
try {
  await EmmaSdk.inAppMessage({ type });
  console.log(`InApp ${type} message`);
} catch (err) {
  console.error('InApp message error', err);
}
```

## In-app events

The `Adball`, `Banner`, `Startview` and `Strip` formats when injected send the `Print` and `Click` events automatically when these actions are performed. In the case of NativeAd being a format managed by the developer to notify these actions these methods must be added:

```typescript
// When impression is done
EmmaSdk.sendInAppImpression({
    campaignId: nativeAd.id,
    type: IN_APP_TYPE.NATIVE_AD,
});
  
// When click is done
EmmaSdk.sendInAppClick({
    campaignId: nativeAd.id,
    type: IN_APP_TYPE.NATIVE_AD,
});
```

The `openNativeAd` method allows to open a NativeAd according to its configuration. If this method is used, there is no need to use the `sendInAppClick` as it is included internally.

```typescript
const params = {
	campaignId: nativeAd.id,
	showOn: nativeAd.showOn,
	cta, // extracted from fields
};

EmmaSdk.openNativeAd(params);
```

## GDPR

To control the use of the data protection law EMMA provides several methods to activate/deactivate the tracking of users.

```typescript
// Enable user tracking
EmmaSdk.enableUserTracking();

//Disable user tracking
EmmaSdk.disableUserTracking(false);
```

The `disableUserTracking` method provides a flag to permanently remove the user in case tracking is disabled. Note that this action is irreversible and all data associated with the user will be lost.

To check whether tracking is enabled or disabled use the following method:

```typescript
const isEnabled = await EmmaSdk.isUserTrackingEnabled();
```

## Sending the customer identifier

If the app does not use the login/registration events to notify the customerId you can send it with the following method:

```typescript
EmmaSdk.setCustomerId(customerId);
```

## Sending push token

In the case of not using the push methods of the EMMA SDK you can send the push token to send push notifications, in the case of adding the SDK methods you do not need to use this method.

```typescript
EmmaSdk.sendPushToken(token);
```

## Request IDFA (iOS only)

To request the IDFA identifier use the following method:

```typescript
EmmaSdk.requestTrackingWithIdfa();
```
## Request notification permission (Android only)

From Android 13 to receive notifications it is necessary to request a permission from the user. As of version 1.3.0, the following method has been added to request permission:

```typescript
 const permissionStatus = await EmmaSdk.requestNotificationPermission();
```

## Update conversion value in SKAdNetwork (iOS only)

For more information about SKAdNetwork see [here](https://developer.emma.io/es/ios/integracion-sdk#integraci%C3%B3n-de-skadnetwork-40)

Since version 1.5.0 support for SKAdNetwork 4.0 has been added.** The plugin performs attribution by default that does not require integration**. Installs and events triggered on the EMMA dashboard are automatically attributed, but sometimes an additional method is required to manually attribute.

To deactivate automatic mode:
```typescript
const startSessionParams = {
    ....
    skanCustomManagementAttribution: false
  }
  wait EmmaSdk.startSession(startSessionParams)
```
The methods to update the conversion value:

```typescript
// Available for iOS 15.5+
wait EmmaSdk.updatePostbackConversionValue(5);
```

```typescript
const conversionModel = {
  conversion value: 5,
  coarse value: 'high',
  lock window: false
}
// Available for iOS 16.1+
wait EmmaSdk.updatePostbackConversionValue(conversionModel);
```
conversionValue would be the conversion value that identifies a specific event on the platform. Values ​​range from 1 to 63.

coarseValue represents a second value of type string that can be: high, medium, low.

lockWindow if it is false means that it respects the terms of the sale of the tribution, if it is true the postback is offered before the end of the window.

