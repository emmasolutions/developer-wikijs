---
title: EMMA Flutter SDK
description: SDK de EMMA para flutter
published: true
date: 2025-02-16T21:24:24.787Z
tags: 
editor: markdown
dateCreated: 2022-01-26T09:58:52.827Z
---

# Installation

EMMA has native Flutter bindings that make up the EMMA Flutter SDK.

- [📦 EMMA Flutter SDK *Package published*](https://pub.dev/packages/emma_flutter_sdk)
{.links-list}

In the `pubspec.yaml` add the following line:

```yaml
dependencies:
  emma_flutter_sdk: 1.6.0
```

To import the library into the Dart code:

```dart
    import 'package:emma_flutter_sdk/emma_flutter_sdk.dart';
```

- [📱 Example App *Full EMMA implementation in Flutter*](https://github.com/EMMADevelopment/emma_flutter_sdk/tree/master/example)
{.links-list}

## Current implementation

- [x] Session Tracking (Start Session)
- [x] Event Tracking
- [x] Update User Profile
- [x] Default events
  - [x] Login
  - [x] Register
  - [x] Purchase
- [x] Powlink Support
- [x] Push System Support
  - [x] Receive Push
  - [ ] Notification Color
  - [x] Mark push as opened
  - [x] Rich Push
- [ ] InApp Messages
  - [x] NativeAd
  - [x] StartView
  - [x] Banner
  - [x] AdBall
  - [x] Strip
  - [ ] Dynamic Tab
  - [ ] Coupon
  
# Android integration

It is important that in `AndroidManifest.xml` the `MainActivity` Activity has as an attribute `launchMode="singleTask"`:

```xml
<activity
    android:name=".MainActivity"
    android:label="@string/app_name"
    android:configChanges="keyboard|keyboardHidden|orientation|screenSize|uiMode"
    android:launchMode="singleTask"
    android:windowSoftInputMode="adjustResize">
```

## Push

On Android we must include the notification icon as drawable.

1. Open Runner in Android Studio.
2. Place the icon files in the `/app/res/drawable` folder of the Android Studio project.

To integrate the push you have to add the EMMA push service in the `AndroidManifest.xml` of the Android app, inside the `<application>` tag. This service is the one that mounts and displays the push notification:

```xml
<service
    android:name="io.emma.android.push.EMMAFcmMessagingService"
    android:enabled="true"
    android:exported="false">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT"/>
    </intent-filter>
</service>
```

Indicates the use of the `google-services` plugin in `android/build.gradle`:

```groovy
buildscript {
    ....
    repositories {
        ....
        google()
    }
    dependencies {
       ...
        classpath 'com.google.gms:google-services:4.3.15'
    }
}
```

Once the service is included we add the push dependencies and apply the `google-services` plugin in the `android/app/build.gradle` file:

```groovy
dependencies {
    ....
    implementation 'com.google.firebase:firebase-messaging:21.0.1'
}

apply plugin: 'com.google.gms.google-services'
```

> Do not forget to add the `google-services.json` obtained in the [Firebase](https://console.firebase.google.com/) console in the path `android/app/`.
{.is-warning}

> You can find more information about Push [here](https://developer.emma.io/en/android/integracion-sdk#push-notification-integration).
{.is-info}

## POWLINK and Deeplinking

To use Powlink or Deeplinking in Android it is necessary to add the following activity to the `AndroidManifest.xml` of the app, inside `<application>`:

```xml
  <activity
            android:name="io.emma.android.activities.EMMADeepLinkActivity"
            android:noHistory="true"
            android:exported="true"
            android:theme="@android:style/Theme.NoDisplay">

            <intent-filter>
                <action android:name="android.intent.action.VIEW"/>

                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.BROWSABLE"/>

                <data android:scheme="${DEEPLINK_SCHEME}"/>
            </intent-filter>

            <intent-filter android:autoVerify="true">
                <action android:name="android.intent.action.VIEW"/>

                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.BROWSABLE"/>

                <data
                    android:host="${CUSTOM_POWLINK_DOMAIN}.powlink.io"
                    android:scheme="https"/>

                <data
                    android:host="${CUSTOM_SHORT_POWLINK_DOMAIN}.pwlnk.io"
                    android:scheme="https"/>

            </intent-filter>
        </activity>
```

- Replace `${DEEPLINK_SCHEME}` with the app's deeplink scheme, for example: emmarn (from emmarn://). The basic deeplink scheme is used to perform actions from In-App communication, also to use the landing deeplink redirection in AppTracker.
- Replace `${CUSTOM_POWLINK_DOMAIN}` and `${CUSTOM_SHORT_POWLINK_DOMAIN}` by the domains configured in the EMMA dashboard app preferences section.

Below the activity, in the same `AndroidManifest.xml`, you must also add the following `<meta-data>`:

```xml
<meta-data
    android:name="io.emma.DEEPLINK_OPEN_ACTIVITY"
    android:value="${PACKAGE_NAME}.MainActivity"/>

```

- Replace `${PACKAGE_NAME}` with the path to the `MainActivity` file, for example (com.example.MainActivity). This meta-data tells the EMMA SDK which Activity to open when receiving a deeplink, in the case of hybrid SDKs there is usually only one Activity.

######
- [📄 Configure Powlink and pwlnk *More information about powlink domains*](https://docs.emma.io/en/adquisicion/apptracker#configuring-powlink-and-pwlnk-stu)
{.links-list}

# iOS Integration

First of all, be sure when you start the project that the EMMA dependency is installed by running `flutter build ipa`.

## Push

To integrate push notifications in iOS you have to enable `Capability > Push Notification`.

In order to enrich the notifications with multimedia content, please refer to the [native documentation](https://developer.emma.io/en/ios/integracion-sdk#creating-the-notification-service-extension) for creating a new notification service.

## POWLINK y Deeplinking

To add the Powlink (Universal Link) go to the `Capabilities` section inside the app target in Xcode and add `Associate domains`, then add the Powlink with the format `applinks:customsubdomain.powlink.io`.

> Note that `customdomain` is the subdomain configured in the EMMA Dashboard.
{.is-warning}

![powlink_adding_capabilities](/react-native/powlink_adding_capabilities.png)

![added_powlink_capabilities](/react-native/added_powlink_capabilities.png)

In the case of the basic deeplink it is necessary to add the schema in the `Info` section inside the app target in Xcode and in `URL Types` add a new entry where you can define the app schema.

![added_urltype_info.png](/react-native/added_urltype_info.png)

For more information about POWLINK domains and how to configure domains in Dashboard go [here](https://docs.emma.io/en/adquisicion/apptracker#configuring-powlink-and-pwlnk-stu).

## Location

To use the localization it is necessary to add the following permissions in the `Info.plist` file of the App:

```xml
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
<key>NSLocationAlwaysUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
<key>NSLocationWhenInUseUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
```

## Apply for the IDFA

To request the IDFA it is necessary to add the following permission to the `Info.plist`:

```xml
<key>NSUserTrackingUsageDescription</key>
	<string>Uso del indentificador para redes de terceros</string>
```

# Integration in Dart
The examples in this section are performed on `dart` files.

To import the sdk into the code use the following *import*:

```dart
import 'package:emma_flutter_sdk/emma_flutter_sdk.dart';
```

## Login

In the `main.dart` file we add, for example:

```dart
 @override
  void initState() {
    super.initState();
    initPlatformState();
    initEMMA()
  }
  
  final startSessionParams = StartSession(
    sessionKey: "<EMMAKey>",
    queueTime: 10,
    isDebug: true,
    customShortPowlinkDomains: ["emma.shortLink.mycompany.com"],
    customPowlinkDomains: ["emma.link.mycompany.com"],
    trackScreenEvents: false,
    familiesPolicyTreatment: (Platform.isAndroid) ? true : false,
  );

  Future<void> initEMMA() async {
    await EmmaFlutterSdk.shared
        .startSession(startSessionParams);
  }
```

Configuration parameters for login:

| Parameter | Type | Description |
|---|---|---|
| *sessionKey* | String | [Key](https://docs.emma.io/en/configuracion#general-emma-key-and-api-key) for login identification. |
| *apiUrl* | String (Optional) | Add the URL if a proxy is used.   |
| *queueTime* | Int (Optional) | Time to empty the operation queue.  |
| *isDebug* | Boolean (Optional) | Enable/disable logging in the SDK. |
| *customPowlinkDomains* | String (Optional) | Add domains if the app uses custom domains in EMMA (subdomains that are configured in app preferences are not custom domains). |
| *customShortPowlinkDomains* | String (Optional) | Add the short domains if the app uses custom domains in EMMA (the subdomains that are configured in the app preferences are not custom domains).
| *trackScreenEvents* | Boolean (Optional)| Enable/disable automatic screen tracking. | 
| *familiesPolicyTreatment* | Boolean (Optional) (Android)| Enables/disables compliance with the families policy associated with the “Designed for Families” program. Only required for Android. |

## Push

Add the `startPush` in the `main.dart` file after the `startSession`:

```dart
  Future<void> initEMMA() async {
    await EmmaFlutterSdk.shared
        .startSession('<SESSION_KEY>', debugEnabled: true);

    await EmmaFlutterSdk.shared.startPushSystem('icimage');
  }
```

Parameters of the `startPushSystem`:

| Parameter | Type | Description |
|---|---|---|
| _notificationIcon_ | String | The icon that will contain the notification in the status bar and in the notification menu. The SDK looks for this icon in the drawable or drawable-xxx folder, so it is important that the name included in this parameter matches the drawable png. |
| _notificationChannelId_ | String (Optional) | If the application uses an existing notification channel, otherwise it will create a new one. |
| _notificationChannel_ | String (Optional) | Add the name of the new channel.  |

> The use of the `startPushSystem` is valid for Android and iOS, although the parameters are only used in Android since in iOS it takes the default values of the App.
{.is-info}

## POWLINK and Deeplinking

To get the deeplink or powlink once the application is open add the following *handler* after login:

```dart
    // The DeepLink/Powlink is caught in this handler.
    EmmaFlutterSdk.shared.setDeepLinkHandler((url) {
        print(url);
    });
```

## Events

EMMA allows the sending of custom events and several default events. For example, the default event `Open` is sent when logging into the SDK. However, the `login` and `registration` events are events that need to be added to the application code.

```dart
await EmmaFlutterSdk.shared.trackEvent("2eb78caf404373625020285e92df446b");
```

### Registration and login

To register and/or login it is necessary to send the user id in the app (Customer ID):

```dart
    await EmmaFlutterSdk.shared
                .loginUser("userflutter#23243", "emma@flutter.dev");
    

    await EmmaFlutterSdk.shared
                .registerUser("userflutter#23243", "emma@flutter.dev");
```

## User properties

EMMA allows the sending of key/value properties associated with the device:

```dart
await EmmaFlutterSdk.shared
        .trackExtraUserInfo({'TEST_TAG': 'TEST VALUE'});
```

## Location

To measure the user's location add the following method:

```dart
await EmmaFlutterSdk.shared.trackUserLocation();
```

The user's location will only be collected once when starting the app.

## Purchasing

The purchase process consists of several methods: `startOrder`, `addProduct` and `trackOrder`.

```dart
  // Start order
  var order = new EmmaOrder("EMMA", 100, "1");
  await EmmaFlutterSdk.shared.startOrder(order);
  // Add products
  var product = new EmmaProduct('SDK', 'SDK', 1, 100);
  await EmmaFlutterSdk.shared.addProduct(product);
  // Commit order
  await EmmaFlutterSdk.shared.trackOrder();
```

In the case of canceling a purchase that has already been made, use the `cancelOrder` method.

```dart
await EmmaFlutterSdk.shared.cancelOrder('EMMA');
```

## In-App messages

EMMA allows the use of various communication formats:

- NativeAd
- StartView
- AdBall
- Banner
- Strip

> Banner format is only supported on Android.
{.is-info}

### NativeAd

The NativeAd format returns a JSON object containing the NativeAd. Example usage:

```dart
  EmmaFlutterSdk.shared
      .setReceivedNativeAdsHandler((List<EmmaNativeAd> nativeAds) {
      nativeAds.forEach((nativeAd) {
        print(nativeAd.toMap());
    });
  });

  var request = new EmmaInAppMessageRequest(InAppType.nativeAd);
  request.batch = true;
  request.templateId = "template1";
  await EmmaFlutterSdk.shared.inAppMessage(request);
```

### All other formats

Any format other than NativeAd does not expect a result, as it is automatically injected into the view. Usage examples:

```dart
// StartView
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.startview));

// AdBall
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.adBall));

// Banner
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.banner));

// Strip
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.strip));
```

## In-App events

The `StartView`, `AdBall`, `Banner` and `Strip` formats, when injected, send the `Print` and `Click` events automatically when these actions are performed. In the case of `NativeAd`, being a format managed by the developer, these methods must be added to notify these actions:

```dart
// When impression is done
await EmmaFlutterSdk.shared
        .sendInAppImpression(InAppType.nativeAd, nativeAd.id);
  
// When click is done
await EmmaFlutterSdk.shared
        .sendInAppClick(InAppType.nativeAd, nativeAd.id);
```

The `openNativeAd` method allows to open a NativeAd according to its configuration. If this method is used, there is no need to use the `sendInAppClick` as it is included internally.

```dart
await EmmaFlutterSdk.shared.openNativeAd(nativeAd);
```

## Sending the customer identifier

If the app does not use the login/registration events to notify the `customerId`, you can send it with the following method:

```dart
await EmmaFlutterSdk.shared.setCustomerId("user#12345");
```

## Request IDFA (iOS only)

To request the IDFA identifier, use the following method:

```dart
if (Platform.isIOS) {
 await EmmaFlutterSdk.shared.requestTrackingWithIdfa();
} 
```

## Request permission for notifications (Android only)

Since Android 13 to receive notifications it is necessary to request permission from the user. As of version 1.2.0 the following method for requesting permission has been added:

```dart
if (Platform.isAndroid) {
  // Request notification permission for Android 13
 EmmaFlutterSdk.shared.setPermissionStatusHandler((status) {
   print('Notifications permission status: ' + status.toString());
 });
 
 await EmmaFlutterSdk.shared.requestNotificationsPermission();
}
```

## GDPR

To control the use of the data protection law EMMA provides several methods to enable/disable the tracking of users.

```dart
// Enable user tracking
await EmmaFlutterSdk.shared.enableUserTracking();

//Disable user tracking
await EmmaFlutterSdk.shared.disableUserTracking(false);
```

The `disableUserTracking` method provides a *flag* to permanently remove the user in case tracking is disabled.

> Please note that this action is irreversible and all data associated with the user will be lost.
{.is-warning}

To check whether tracking is enabled or disabled use the following method:

```dart
var isEnabled = await EmmaFlutterSdk.shared.isUserTrackingEnabled();
```
