---
title: EMMA REST API
description: EMMA API connection
published: true
date: 2024-12-21T10:55:06.041Z
tags: api, rest
editor: markdown
dateCreated: 2022-01-26T09:56:34.570Z
---

# API Connection

The EMMA API uses the OAuth2 authentication protocol. Authentication is done by obtaining a token with a user that has access to the apps we want to interact with via the API.

## API URL
The base URL of the api is `https://ws.emma.io`.

## Get the EMMA Key

Before connecting to the EMMA API we must know the `API Key` of our application.

See cobo to get the EMMA Key at https://docs.emma.io/en/configuracion#general-emma-key

## Get the session token

To obtain the token we must first make a call to the `/oauth2/login` endpoint with the following parameters:

```json
{
  "username": "username",
  "password": "password",
}
```

Example of a call using `cURL`

```bash
curl -X POST "https://ws.emma.io/oauth2/login" -H "Content-Type:application/json" -d "{\"username\": \"your-username\", \"password\": \"your-password\"}"

```

Sample response:

```json
{
  "access_token":"KCREUKQ01PzIlobcmxCtLJAjGC5VB8jiDD36ceHW1aXjpmnHTk4A64E7eyiT5CXAznuId8mFHtQEk3H68gY3YLfWu0FsTO5VPlc9GFOhZIv4IuPHRyo7fumkZJKILfwEaHcR4MYMOvoqtVdVnnjpRSSFv8j5HoZFyUIo9KU9UNsVqVqE4XH1MxTQGkpcmxbuwh4JFzhlfEEmISK17E80Fvml0HnFS4Yr92gOrOvMdSYmYtnC5BrL9evWpSSxgVHI",  
  "refresh_token":"4LVP2Bm0r8o4VDIOcdpzS2AuAEUdcjCDXBJjHCw99PXXcYnia4xN4DGGcAnVZHVlEKlBhPXInDlSsf5wtM7Rc3yJIqrQ6VpoOyUGAwHRplB65u3UEg4NS6DM1LAPXlzBIGqIz82CE6KMw6XUJJINMCpkGN8l32bTEfb87lyopHcRHT0lsvv2zpCibd3TbXvukI7kK6opnXnmxSYKiMloL4Lc9L8xbH9nideVq28FqUAJ1RNzQKsoSGuilUlz4h2M",
  "expires_in":"2021-08-06 11:33:05",
  "token_type":"Bearer"
}
```

## Authenticate an api call

For authenticated API calls we must send the `apiKey` header with the content of our API Key and the `Authorization` header with the content of the `token_type` and `access_token` that we have obtained from the `/oauth2/login` call.

Example:
```
Authorization: Bearer hLOQPeU3fXBdq45LiMgS3Q3L2BcjK9BzSNtSKdHK6gcF0oh5fhCGqjGNjwTAMNCjNbNMOkoupmHy5ifCAvb11PcXJsRmucAVcOfIftFDIQRAY2e6dhb21XHG6duKs30YMhAhWb8HaH1RK9hHA1DKhX6FjznATT7hTm28whAU0zpWQCq8jiT9mHY2Zp2FhraJGoAn90VBaDsRR80jAJrQ7IPKX3hoMFXs5fVLXP2krzHp01FSnfFKb0JEbaz9iFgB
```

### Example for sending a server 2 server event (cURL)

```bash
curl --location --request POST 'https://ws.emma.io/user/events' \
--header 'apiKey: 0051718341426c8c3dd2cdc7e155d44f' \
--header 'Authorization: Bearer hLOQPeU3fXBdq45LiMgS3Q3L2BcjK9BzSNtSKdHK6gcF0oh5fhCGqjGNjwTAMNCjNbNMOkoupmHy5ifCAvb11PcXJsRmucAVcOfIftFDIQRAY2e6dhb21XHG6duKs30YMhAhWb8HaH1RK9hHA1DKhX6FjznATT7hTm28whAU0zpWQCq8jiT9mHY2Zp2FhraJGoAn90VBaDsRR80jAJrQ7IPKX3hoMFXs5fVLXP2krzHp01FSnfFKb0JEbaz9iFgB' \
--header 'Content-Type: application/json' \
--data-raw '[
  {
    "token": "126tgsl9lm6402n42234",
    "attributes": {
      "spend": "20",
      "product": "red jacket"
    },
    "created_at": "2020-03-30 14:45:42",
    "emma_id": 5215,
    "device_id": "439e5015-xxxx-46ad-xxxx-08a30c85xxxx",
    "customer_id": "user123"
  }
]'
```
