---
title: EMMA user profile
description: Description of the response to the user info call. User profile values
published: true
date: 2024-12-30T10:49:49.509Z
tags: 
editor: markdown
dateCreated: 2022-01-26T09:59:07.832Z
---

# User profile fields (User Info)
> `id` Same unique ID as in the method (getUserID)

> `udid` Unique ID per app install (IDFA/AAID usually)

> `emma_build` Identifies the version of the EMMA SDK in use

> `email` Email of the user in case registration or login are integrated

> `customerid` ID assigned to the user after registration/login

> `device` User's device model

> `app_version` App version

> `os_version` Operating System version

> `os_build` OS build

> `token` Push identifier (token) 

> `latitude` Latitude of the user in case geolocation is allowed and measured

> `longitude` Longitude of the user if geolocation is allowed and measured 

> `emma_carrier` Telephony operator

> `emma_connection`	 Connection type (wifi or 3g)

> `emma_city`	City of the user in case geolocation is allowed and measured

> `emma_country`	Country of the user if geolocation is allowed and measured

> `emma_state`	Country of the user if geolocation is allowed and measured

> `emma_accept_language` 	 Language set in the app

> `emma_timezone_name`	 Device TimeZone

> `{tagX....tagY}`	User tags that are traced in the trackExtraUserInfo flame

> `eat_sub{x}`	Value taken by the eat_sub{x} parameter if used in a powlink

# User attribution information

## Description of the user attribution fields

| Name                | Type     | Description                                                                                                                                                                                                                                                           |
|------------------------|:--------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `status`                 | String   | It returns the status of the attribution. This can have several types: 
|  |   | - **pending:** Pending attribution (2-day window).
|  |   | - **campaign:** Attributed to a specific campaign. 
|  |   | - **organic:** Organic attribution (not linked to campaign).
| `campaign.id`           | Number   | Campaign ID.                                                                                                                                               
| `campaign.name`         | String   | Name of the campaign.                                                                                                                                                                                                                                                        |
| `campaign.clickParams`       | Dictionary| Click parameters.                                                                                                                                                                                                                                                        |
| `campaign.source.id`              | Number   | Source Id.                                                                                                                                                                                                                                                           |
| `campaign.source.name`           | String   | Source name.                                                                                                                                                                                                                                                         |
| `campaign.source.channel`           | String   | Channel name. Available from SDK version 4.15.0.                                                                                                                                                                                                                                                         |
| `campaign.source.params`          | Dictionary   | Static parameters added in the source. Available from SDK version 4.15.0.                                               
| `campaign.source.provider.id` | Number | Provider Id.
| `campaign.source.provider.name` | String | Provider name.
