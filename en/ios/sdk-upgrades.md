---
title: EMMA SDK iOS upgrades
description: 
published: true
date: 2025-02-10T09:42:36.703Z
tags: 
editor: markdown
dateCreated: 2022-01-26T09:59:01.424Z
---

# 4.15.2 - 10/02/2025

- **IMPROVE** Prism and StartView are now displayed in the correct order, StartView is always displayed on top of any format.

# 4.15.1 - 26/12/2024

- **IMPROVE** Implemented a solution to prevent StartView, AdBall and Strip from appearing simultaneously.
- **IMPROVE** Enhanced AdBall visualization when running in UIKit with a Strip on screen.
- **IMPROVE** Added padding to the AdBall in the bottom corners to improve UI/UX.
- **IMPROVE** Adjusted trashView dimensions to resolve issues with AdBall dragging.
- **FIX** Fixed an issue where rules were not executed when a Strip was active.

# 4.15.0 - 26/09/2024

- **NEW** Added new install attribution info callback in start session.
- **NEW** When a link with the parameter `st_open_browser=true` is opened in the WebView, the URL is launched in the browser.
- **IMPROVE** Sessions with times less than 10 seconds are discarded.

# 4.14.0 - 11/07/2024

- **NEW** Added new method `sendDismissedClick` to send clicks when inapp is dismissed.

# 4.13.1 - 21/03/2024

- **NEW** Added `PrivacyInfo.xcprivacy` and XCFramework signature.

# 4.13.0 - 05/02/2024

- **NEW** `setCurrencyCode` method is removed. `currencyCode` is selected in EMMA Dashboard. All prices sent in the order will not be converted by EMMA to a specific currency, they will be interpreted as a unit value.
- **NEW** Added banner offsets (top and bottom).
- **FIX** Minor bugs fixed.

# 4.12.3 - 17/01/2024

- **FIX** Fixed Strip not showing in SwiftUI based apps.

# 4.12.2 - 12/01/2024

- **FIX** Fixed `currencyCode` other than EUR not being sent in the `trackOrder` request.

# 4.12.1 - 04/07/2023

- **IMPROVE** Added `updatePostbackConversionValue` with `lockWindow` parameter.

# 4.12.0 - 05/06/2023

- **CHANGE** Disabled bitcode support and removed i386 and armv7 architectures. Apple deprecates bitcode with Xcode14: https://developer.apple.com/documentation/xcode-release-notes/xcode-14-release-notes
- **CHANGE** XCode14.1+ required: https://developer.apple.com/news/upcoming-requirements/?id=04252023a
- **CHANGE** Updated iOS deployment target to iOS 11. Supported in devices with iOS 11+.
- **IMPROVE** SDK sizes decreased from ~14mb to ~4mb.
- **IMPROVE** Support title in push notifications.
- **FIX** Some deeplinks within StartViews are executed before the StartView is closed, causing overlapping ViewControllers.
- **NEW** Support for SKAdNetwork 4.

# 4.11.4 - 23/03/2023

- **FIX** Sometimes the Strip is hidden when the ViewController is dismissed. The Strip is now displayed on all ViewControllers.
- **NEW** `closeStrip` method has also been added to give you the freedom to close the strip whenever you want.

# 4.11.3 - 17/01/2023

- **REMOVE** Removed support for Apple Search Ads `iAd.framework`. Only for iOS < 14.3, from version 4.10.2 `AdService.framework` is used.

# 4.11.2 - 15/11/2022

- **NEW** Close WebView from web with deeplink `://startview/close`.
- **FIX** Fixed bug added in 4.11.1, StartView and AdBall with rules don't work correctly.

# 4.11.1 - 20/10/2022

- **FIX** Fixed StartView, sometimes crashes when is inited outside the main thread.
- **FIX** Fixed crash whit nil dates in attribution process.

# 4.11.0 - 05/05/2022

- **NEW** Click types for inapp communications Adball and StartView.

# 4.10.3 - 24/03/2022

- **FIX** Fixed coupon end date not showing.

# 4.10.2 - 03/03/2022

- **FIX** Fixed critical crash produced in production only with app with SDK started and request ASA atribution. Versions 4.10.0 and 4.10.1 have been deleted due to affection.

# 4.10.1 - 07/02/2022
###### Deleted version

- **FIX** Fixed Apple Attribution with `AdServices.framework`. On iOS 14.3 to 14.8.1 it could produced a hang.

# 4.10.0 - 20/01/2022
###### Deleted version

- **NEW** Notifications with action buttons.

# 4.9.3 - 07/10/2021

- **FIX** Fixed problem banner dismiss `rootViewController` and minor bugs.

# 4.9.2 - 27/04/2021

- **FIX** In early versions of iOS 14 sometimes the IDFA was not obtained correctly.
- **FIX** Fixed bug `customerId` in purchases added in version 4.9.0.

# 4.9.1 - 18/03/2021

- **FIX** Fixed `EMMAEventRequest` as optional when instantiating.

# 4.9.0 - 13/03/2021

- **CHANGE** Changed deployment target to iOS 9.
- **NEW** SDK partially rewritten in Swift. Several APIs have been adapted to be more convenient in the Swift language.
- **NEW** New method to add inapp plugins.
- **REMOVE** Removed method `+(void)startSession:(NSString*)appKey withOptions:(NSDictionary*)launchOptions __attribute__((deprecated("Use startSession without options")));`.
- **REMOVE** Removed method `+(void) startPushSystem: (NSDictionary*) launchOptions __attribute__((deprecated("Use startPushSystem without parameters")));`.
- **REMOVE** Removed method `+(void)startOrder:(NSString*)orderId customerId:(NSString*)customerId totalPrice:(float)totalPrice coupon:(NSString*)coupon;`.
- **REMOVE** Removed method `+(void)registerUser:(NSString*)userId forMail: (NSString*)mail;` and replaced by optionals.
- **REMOVE** Removed method `+(void)loginUser:(NSString*)userId forMail:(NSString*)mail;` and replaced by optionals.
- **REMOVE** Removed method `+(void)addProduct:(NSString*)productId name:(NSString*)name qty:(float)qty price:(float)price;`.
- **REMOVE** Removed method `+(void)addRateAlertForAppStoreURL:(NSString*)appStoreURL;`.
- **REMOVE** Removed method `+(void) setRateAlertFreq: (int) hours;`.
- **REMOVE** Removed method `+(void) setRateAlertTitle: (NSString*) title;`.
- **REMOVE** Removed method `+(void) setRateAlertMessage: (NSString*) message;`.
- **REMOVE** Removed method `+(void) setRateAlertCancelButton: (NSString*)>cancelButtonText;`.
- **REMOVE** Removed method `+(void) setRateAlertRateItButton: (NSString*) rateItButtonText;`.
- **REMOVE** Removed method `+(void) setRateAlertLaterButton: (NSString*) laterButtonText;`.
- **REMOVE** Removed method `+(void) setRateAlertShowAfterUpdate:(BOOL) showAlert;`.
- **RENAME** Renamed method `EMMA.add(inAppDelegate: self)` to `EMMA.addInAppDelegate(delegate: self)`.
- **RENAME** Renamed method  `EMMA.trackEvent(eventRequest)` to `EMMA.trackEvent(request: eventRequest)`.
- **RENAME** Renamed method `EMMA.handleLink(url)` to `EMMA.handleLink(url: url)`.
- **RENAME** Renamed method `EMMA.handlePush(userInfo)` to `EMMA.handleLink(userInfo: userInfo)`.
- **RENAME** Renamed method `EMMA.openNativeAd(nativeAdCampaignId: campaignId)` to `EMMA.opeNativeAd(campaignId: campaignId)`.

# 4.8.1 - 07/01/2021

- **FIX** Fixed crash "Collection was mutated while being enumerated" in Campaign Controller.
- **FIX** Minor bugs fixed.

# 4.8.0 - 12/11/2020

- **IMPROVE** Changed the management of identifiers internally.
- **NEW** New method to obtain the IDFA by requesting the tracking permissions in iOS 14.
- **NEW** New method to update the customer id without sending the login or registration event.
- **FIX** Minor bugs and improvements.

```swift
 if #available(iOS 14.0, *) {
   EMMA.requestTrackingWithIdfa()
 }
        
 EMMA.setCustomerId("198284343")
```

# 4.6.7 - 11/09/2020
- **FIX** Fixed a crash that occurred in auto events with strange characters in the title.

# 4.6.6 - 23/07/2020
- **FIX** Fixes a crash introduced in version 4.6.5.

# 4.6.5 - 23/07/2020

- **IMPROVE** The delegate callbacks are stored in the instance.

# 4.6.4 - 01/07/2020

- **NEW** New method to obtain the deviceId.

# 4.6.3 - 28/05/2020

- **FIX** Fixed campaign parameters in rules.

# 4.6.2 - 07/04/2020

- **NEW** Click parameters for installation during campaign attribution.
- **FIX** Minor bugs

In SDK version 4.6.2 or higher the extension code has been added inside an SDK method. It replaces the entire contents of the file with the code below:

```swift
import UIKit
import UserNotifications
import EMMA_iOS

class NotificationService: UNNotificationServiceExtension {
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            EMMA.didReceive(request, with: bestAttemptContent) { (content) in
                contentHandler(bestAttemptContent)
            }
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
}
```
To add the SDK to the extension it is necessary to modify the Podfile file by adding the pod eMMa to the extension target:
```ruby
target 'ProjectName' do
  pod 'eMMa', '~> 4.6.2'
end

target 'EMMANotificationServiceExtension' do
  pod 'eMMa', '~> 4.6.2'
end
```

# Upgrade to version 4.6
The pushMessage and pushTag methods have been removed from the EMMAPushDelegate and the onPushOpen method has been added. This method is passed the currently opened push campaign. This campaign has attributes such as tag or message, which would replace the deleted methods.
```objc
 -(void)onPushOpen:(EMMAPush*)push
```

# Upgrading to version 4.5
Previous versions upgrading to 4.5 will need to make the following changes.

## Changes in Events

The following methods that were deprecated in version 4.4 have been removed in 4.5:

```objc
+(void) trackEvent:(NSString*)token 
+(void) trackEvent:(NSString *)token withAttributes: (NSDictionary*) attributtes
+(void) trackEventWithRequest:(EMMAEventRequest *) request
```

Both are replaced by the following method:

```objc
+(void) trackEvent:(EMMAEventRequest *) request
```
Example of use:
```swift
let eventRequest = EMMAEventRequest.init(token: "<token>")
// Optional: You can add your custom event attributes
eventRequest?.attributes = ["test_attribute":"test_value"]
// Optional. You can capture emma requests with this delegate
eventRequest?.requestDelegate = self
// Optional. Append your request ID to capture it later
eventRequest?.customId = "MY_EVENT_REQUEST"
        
EMMA.trackEvent(eventRequest)
```

## Changes in InApp

The following methods that were deprecated in 4.4 have been removed in 4.5:

```objc
+(void)inAppMessage:(InAppType)type andRequest:(EMMAInAppRequest*) request
+(void)inAppMessage:(InAppType)type andRequest:(EMMAInAppRequest*) request withDelegate:(id) delegate
```
Both are replaced by the following methods:
```objc
+(void)inAppMessage:(EMMAInAppRequest*) request
+(void)inAppMessage:(EMMAInAppRequest*) request withDelegate (id) delegate
```
Example of use:
```swift
import UIKit
import EMMA_iOS

class InAppExampleViewController: UIViewController, EMMAInAppMessageDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getStartView()
        // You can add as many request delegates as you want
        EMMA.add(inAppDelegate: self)
        
        //Check if any adball is showing
        if EMMA.isAdBallShowing() {
            print("There is an adball floating arround")
        }
    }
    
    // MARK: - EMMA InApp Messages Requests
    
    func getStartView() {
        let startViewinAppRequest = EMMAInAppRequest(type: .Startview)
        // Optional. You can filter by label
        startViewinAppRequest?.label = "<LABEL>"
        /*
         By default Startview presents on UIApplication.shared.delegate?.window?.rootViewController
         You can customize this behavior uncommenting following line
        */
        //EMMA.setRootViewController(UIViewController!)
        EMMA.inAppMessage(startViewinAppRequest)
    }
    
    func getBanner() {
        let bannerRequest = EMMAInAppRequest(type: .Banner)
        EMMA.inAppMessage(bannerRequest)
    }
    
    func getAdBall() {
        let adballRequest = EMMAInAppRequest(type: .Adball)
        EMMA.inAppMessage(adballRequest)
    }
    
    func getDynamicTabBar() {
        // You must define your UITabBarController
        // Uncomment following line!
        // EMMA.setPromoTabBarController(UITabBarController!)
        
        // Sets default promo tab index if not defined in EMMA Platform
        EMMA.setPromoTabBarIndex(5)
        
        // Sets a tab bar item to be shown if not defined in EMMA Platform
        // EMMA.setPromoTabBarItem(UITabBarItem!)
        
        let dynamicTabBarRequest = EMMAInAppRequest(type: .PromoTab)
        EMMA.inAppMessage(dynamicTabBarRequest)
    }
    
    func getStrip() {
        let stripRequest = EMMAInAppRequest(type: .Strip)
        EMMA.inAppMessage(stripRequest)
    }
    
    
    // MARK: - EMMA InApp Message Delegate
    
    func onShown(_ campaign: EMMACampaign!) {
        guard let c = campaign else {
            print("Error getting campaign info")
            return
        }
        print("Shown campaign \(c)")
    }
    
    func onHide(_ campaign: EMMACampaign!) {
        guard let c = campaign else {
            print("Error getting campaign info")
            return
        }
        print("Hide campaign \(c)")
    }
    
    func onClose(_ campaign: EMMACampaign!) {
        guard let c = campaign else {
            print("Error getting campaign info")
            return
        }
        print("Closed campaign \(c)")
    }
}
```

>If the message is to request a Native Ad it is mandatory to use EMMANativeAdRequest (subclass of EMMAInAppRequest) to add the templateId and its specific delegate.
{.is-info}

## Push changes

In this new version, improvements have been made to push notifications management.

The following methods are deprecated:

```objc
+(void) startPushSystem: (NSDictionary*) launchOptions
+(void) startSession:(NSString*)appKey withOptions:(NSDictionary*)launchOptions
```
Due to improvements in the SDK by simply adding the Push delegate layout, there is no need to notify the `launchOptions` to the SDK.