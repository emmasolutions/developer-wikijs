---
title: Privacy t
description: 
published: true
date: 2024-08-19T16:54:02.958Z
tags: apple, privacy statement, privacy manifest, update 2024
editor: markdown
dateCreated: 2024-08-19T16:54:02.958Z
---

# Privacy Manifest![privacy-manifest-example.png](/ios/privacy-manifest-example.png)

> At WWDC23, Apple introduced privacy nutrition labels, which provide users with a comprehensive overview of how an app handles data. These labels provide valuable information about the privacy practices of the apps users interact with.
{.is-info}

With privacy manifests, an app developer will be able to create a record of the categories of data it collects through its app independently and through the use of third-party dependencies, such as third-party SDKs used by the app. This information helps app developers to accurately represent privacy practices in their apps, including through the nutritional labels they provide to their users.

In many cases, app developers do not know what data third-party SDKs collect or what a specific SDK uses it for. The Privacy Manifesto allows third-party SDKs to easily pass this information to app developers, helping them to create accurate nutrition labels.

**Apple plans, as of 1 May 2024, to roll out this functionality to all new and updated apps**, EMMA has updated the iOS SDK across all platforms, the new version includes the Privacy Manifesto to facilitate the process of generating nutrition labels for app developers. This release also includes Apple's recommended signature to ensure binary integrity. 

> This SDK update is not mandatory, if you do not wish to update the SDK you can refer to the sample Privacy Manifesto below. Remember that submitting a Privacy Manifest to disclose EMMA SDK permissions is optional, but the APIs used in NSPrivacyAccessedAPITypes must be added to the app's PrivacyInfo.xcprivacy. See [list of Apple third-party SDKs that require a privacy manifest with permissions](https://developer.apple.com/support/third-party-SDK-requirements/)
{.is-info}

Below is a listing of the platform and SDK version for which the Privacy Manifest and signature are available:
| Platform           | Version |
|---------------|------------------------------------------|
| iOS Nativo    | >= 4.13.1                                |
| Cordova/IONIC | >= 1.7.1                                 |
| React Native  | >= 1.6.0                                 |
| Flutter       | >= 1.4.0                                 |

```xml
<dict>
    <key>NSPrivacyAccessedAPITypes</key>
    <array>
        <dict>
            <key>NSPrivacyAccessedAPIType</key>
            <string>NSPrivacyAccessedAPICategoryUserDefaults</string>
            <key>NSPrivacyAccessedAPITypeReasons</key>
            <array>
                <string>CA92.1</string>
            </array>
        </dict>
    </array>
</dict>
```

## Sample Privacy Manifest

This is an example of an EMMA SDK Privacy Manifest with the default configuration of the SDK, without adding additional methods, just starting the SDK with the *startSession* method as the only configured method. **It is possible that this Manifest will differ greatly from the needs of your app so it should not be taken literally.

![privacy-manifest-example.png](/ios/privacy-manifest-example.png)

Code snippet from the **example** of the Privacy Manifest:

```xml
<dict>
    <key>NSPrivacyCollectedDataTypes</key>
    <array>
        <dict>
            <key>NSPrivacyCollectedDataType</key>
            <string>NSPrivacyCollectedDataTypeOtherDataTypes</string>
            <key>NSPrivacyCollectedDataTypeLinked</key>
            <true/>
            <key>NSPrivacyCollectedDataTypeTracking</key>
            <true/>
            <key>NSPrivacyCollectedDataTypePurposes</key>
            <array>
                <string>NSPrivacyCollectedDataTypePurposeAnalytics</string>
                <string>NSPrivacyCollectedDataTypePurposeProductPersonalization</string>
            </array>
        </dict>
        <dict>
            <key>NSPrivacyCollectedDataType</key>
            <string>NSPrivacyCollectedDataTypeProductInteraction</string>
            <key>NSPrivacyCollectedDataTypeLinked</key>
            <true/>
            <key>NSPrivacyCollectedDataTypeTracking</key>
            <false/>
            <key>NSPrivacyCollectedDataTypePurposes</key>
            <array>
                <string>NSPrivacyCollectedDataTypePurposeAnalytics</string>
                <string>NSPrivacyCollectedDataTypePurposeProductPersonalization</string>
            </array>
        </dict>
        <dict>
            <key>NSPrivacyCollectedDataType</key>
            <string>NSPrivacyCollectedDataTypeDeviceID</string>
            <key>NSPrivacyCollectedDataTypeLinked</key>
            <true/>
            <key>NSPrivacyCollectedDataTypeTracking</key>
            <true/>
            <key>NSPrivacyCollectedDataTypePurposes</key>
            <array>
                <string>NSPrivacyCollectedDataTypePurposeDeveloperAdvertising</string>
                <string>NSPrivacyCollectedDataTypePurposeAppFunctionality</string>
                <string>NSPrivacyCollectedDataTypePurposeAnalytics</string>
                <string>NSPrivacyCollectedDataTypePurposeProductPersonalization</string>
            </array>
        </dict>
    </array>
    <key>NSPrivacyAccessedAPITypes</key>
    <array>
        <dict>
            <key>NSPrivacyAccessedAPIType</key>
            <string>NSPrivacyAccessedAPICategoryUserDefaults</string>
            <key>NSPrivacyAccessedAPITypeReasons</key>
            <array>
                <string>CA92.1</string>
            </array>
        </dict>
    </array>
</dict>
```


## Required reasons for use of APIs

Apple requires developers to declare the use of specific APIs. For more information, please refer to Apple's documentation [here](https://developer.apple.com/documentation/bundleresources/privacy_manifest_files/describing_use_of_required_reason_api?language=objc).

To clarify the use of APIs, application developers must declare the API category and specify the reasons for its use. Below are the APIs that are used by the EMMA SDK in all versions:

| API Category   | API type                              | reason  | Comments                                                                                                                         |
|--------------------|------------------------------------------|--------|---------------------------------------------------------------------------------------------------------------------------------------|
| [User defaults APIs](https://developer.apple.com/documentation/bundleresources/privacy_manifest_files/describing_use_of_required_reason_api) | NSPrivacyAccessedAPICategoryUserDefaults | CA92.1 | The SDK uses this API to store data related to installation, user identifiers and the lifecycle of the app.|


## Data linked to the user

The Personal Data that EMMA collects is tied to a user's device.

EMMA may provide you with the ability to associate the data it collects with an advertising ID or other device identifiers. Any association you make with EMMA data to unique user identifiers, such as user IDs or account IDs, is unique to your practice and is at your own discretion.

## Purposes of use of data collected by EMMA

The following table shows the purposes of use of data collected by the EMMA SDK.
Use the following strings as values for the NSPrivacyCollectedDataTypePurposes key in your NSPrivacyCollectedDataTypes dictionaries:


| Purpose of the data | Description | Does EMMA use this data for this purpose? | Value in the Privacy Notice  |
|------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| Third-party advertising | How to display third-party advertisements in your application or share data with entities that display third-party advertisements.    | EMMA may share data with third party ad serving entities (Google, Meta etc). Only if explicitly configured in the platform, EMMA by default does not send any data to third parties.| NSPrivacyCollectedDataTypePurposeThirdPartyAdvertising  |
|  Developer advertising or marketing | Such as displaying your own ads in your app, sending marketing communications directly to your users or sharing data with entities that will display your ads. | Optional - EMMA may share data with the customer or even with other entities to display in-app advertisements. EMMA will only do this option if it is configured in the platform.  | NSPrivacyCollectedDataTypePurposeDeveloperAdvertising   |
| Analytics | Use data to assess user behaviour, including to understand the effectiveness of existing product features, plan new features, or measure audience size or characteristics.                                               | Yes   | NSPrivacyCollectedDataTypePurposeAnalytics              |
| Product personalisation | Personalise what the user sees, such as a list of recommended products, publications or suggestions.| Opcional - EMMA puede recolectar ciertos datos para personalizar comunicaciones en la app (Pushes, Startview, Native Ad etc).  | NSPrivacyCollectedDataTypePurposeProductPersonalization |
| Application Functionality | Por ejemplo, para autenticar al usuario, habilitar funciones, prevenir fraudes, implementar medidas de seguridad, garantizar el tiempo de actividad del servidor, minimizar las fallas de las aplicaciones, mejorar la escalabilidad y el rendimiento o brindar atención al cliente | Yes    | NSPrivacyCollectedDataTypePurposeAppFunctionality       |
| Other purposes  | Any other purpose not listed.  | No  | NSPrivacyCollectedDataTypePurposeOther  

## Nutritional labels collected by EMMA

**The labels to be added depend on the client's app and how it is treating and providing data to EMMA**. The guidance provided is a guideline on how these tags should be added to the ‘Data Use’ section of the Privacy Manifesto. If you already have an app on AppleStore and you have already completed the mandatory Data Use form, you can take that form as a starting point to add the data to the Privacy Manifest. For proper integration, it is best to work with legal counsel to identify which labels your app uses. For more information on nutrition labels see Apple's documentation [here](https://developer.apple.com/documentation/bundleresources/privacy_manifest_files/describing_data_use_in_privacy_manifests).

Within the Privacy Manifest use the following strings as values for the key NSPrivacyCollectedDataType in the NSPrivacyCollectedDataTypes dictionaries:

**Contact information**

Any of the data in this table would be entities linked to the user. None would be used for Tracking purposes.

|Data type | Description| Does EMMA collect this data by default? | Should you include it in the Privacy Manifest? | Value in the Privacy Manifest |
|------------------------------|-------------------------------------------------------------|---------------------------------------|------------------------------------------------------------------------------|------------------------------------------------|
| Name | First name or surname.                                         | No | Only if sent as a label.                                              | NSPrivacyCollectedDataTypeName |
| Email | When email is not sent as a hash.                | No | Only if sent as a tag or in the loginUser or registerUser methods. | NSPrivacyCollectedDataTypeEmailAddress |
| Phone Number | When not sending the phone number in hash format.             | No | Only if sent as a label.                                          | NSPrivacyCollectedDataTypePhoneNumber |
| Physical Address | Home address, P.O. Box or similar. | No | Only if sent as a label.                                              | NSPrivacyCollectedDataTypePhysicalAddress | 
| Other contact information | Any other contact information outside the app.     | No | Only if sent as a label.                                              | NSPrivacyCollectedDataTypeOtherUserContactInfo |


**Health and fitness**

Any of the data in this table would be linked to the user. None would be used for Tracking purposes.
|Data type | Description| Does EMMA collect this data by default? | Should you include it in the Privacy Manifest? | Value in the Privacy Manifest |
|------------------------------|-------------------------------------------------------------|---------------------------------------|------------------------------------------------------------------------------|------------------------------------------------|
| Health | Health and medical data, including, but not limited to, data from the clinical medical records API, the HealthKit API, movement disorders APIs, or health-related human subjects research, or any other health or medical data provided by any other user. | No | Only if sent as a tag.                  | NSPrivacyCollectedDataTypeHealth |

| Fitness | Physical activity and exercise data, including but not limited to the Motion and Fitness API. | No | Only if sent as a tag. | NSPrivacyCollectedDataTypeFitness |

**Financial information**

Any of the data in this table would be entities linked to the user. None would be used for Tracking purposes.
|Data type | Description| Does EMMA collect this data by default? | Should you include it in the Privacy Manifest? | Value in the Privacy Manifest |
|------------------------------|-------------------------------------------------------------|---------------------------------------|------------------------------------------------------------------------------|------------------------------------------------|
| Payment information | such as payment method, payment card number or bank account number. If your application uses a payment service, the payment information is entered outside your application and you, as the developer, never have access to the payment information, it is not collected and does not need to be disclosed. | No | Only if submitted as a tag.                  | NSPrivacyCollectedDataTypePaymentInfo |
| Credit Information | Such as credit score | No | Only if sent as a tag. | NSPrivacyCollectedDataTypeCreditInfo |
| Other Financial Information | Such as salary, income, assets, debts, or any other financial information. | No | Only if sent as a tag. | NSPrivacyCollectedDataTypeOtherFinancialInfo |


**Location**

Any of the data in this table would be linked to the user. None of it would be used for tracking purposes.

|Data type | Description| Does EMMA collect this data by default? | Should you include it in the Privacy Manifest? | Value in the Privacy Manifest |
|------------------------------|-------------------------------------------------------------|---------------------------------------|------------------------------------------------------------------------------|------------------------------------------------|
Precise Location  | Information that describes the location of a user or device with equal or greater resolution than latitude and longitude with three or more decimal places.             | No | Only if the SDK method .trackLocation() is used. | NSPrivacyCollectedDataTypePreciseLocation | 
| Approximate Location| Information that describes the location of a user or device with a resolution lower than latitude and longitude with three or more decimal places, such as approximate location services. | No| No | NSPrivacyCollectedDataTypeCoarseLocation | 



**Sensitive Information**

Any of the data in this table would be linked to the user. None of it would be used for tracking purposes.

|Data type | Description| Does EMMA collect this data by default? | Should you include it in the Privacy Manifest? | Value in the Privacy Manifest |
|------------------------------|-------------------------------------------------------------|---------------------------------------|------------------------------------------------------------------------------|------------------------------------------------|
Sensitive information |  Like racial or ethnic data, sexual orientation, pregnancy or childbirth information, disability, religious or philosophical beliefs, union membership, political opinion, genetic information, or biometric data. | No |  Only if sent as a tag. | NSPrivacyCollectedDataTypeSensitiveInfo |



**Contacts**
|Data type | Description| Does EMMA collect this data by default? | Should you include it in the Privacy Manifest? | Value in the Privacy Manifest |
|------------------------------|-------------------------------------------------------------|---------------------------------------|------------------------------------------------------------------------------|------------------------------------------------|
Contacts |Like a contact list on the phone, address book, or the user's social graph. | No | No | NSPrivacyCollectedDataTypeContacts

**User Content**

Any of the data in this table would be linked to the user. None of it would be used for tracking purposes.









