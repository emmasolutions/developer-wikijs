---
title: Obtener Push Auth Key
description: 
published: true
date: 2020-11-11T16:59:50.933Z
tags: ios, sdk, apple, push
editor: markdown
dateCreated: 2020-11-11T16:55:26.280Z
---

# Obtain and configure Push Auth Key in Apple

To obtain the certificate, visit the [Apple Developers] account (https://developer.apple.com/account/).

Click on the Certificates, IDs and Profiles section.

![captura_de_pantalla_2018-11-12_a_las_0_02_25.png](/captura_de_pantalla_2018-11-12_a_las_0_02_25.png)

Once in Apple click on the Keys section and add a key.

![captura_de_pantalla_2018-11-12_a_las_0_02_52.png](/captura_de_pantalla_2018-11-12_a_las_0_02_52.png)

Add the name, select the APNs check and click continue.

![captura_de_pantalla_2018-11-12_a_las_0_13_18.png](/captura_de_pantalla_2018-11-12_a_las_0_13_18.png)

Confirm and download the .p8 file it generates.

![captura_de_pantalla_2018-11-12_a_las_0_05_43.png](/captura_de_pantalla_2018-11-12_a_las_0_05_43.png)

![captura_de_pantalla_2018-11-12_a_las_0_13_56.png](/captura_de_pantalla_2018-11-12_a_las_0_13_56.png)

# Configure Push Auth Key in EMMA

Add the file(.p8), the team ID and the bundle ID in EMMA -> App Preferences

![captura_de_pantalla_2020-11-10_a_las_10.32.39.png](/captura_de_pantalla_2020-11-10_a_las_10.32.39.png)

When adding the certificate it is necessary to enter the team ID to validate the certificate with Apple. We will add the bundle ID and the team ID to be used for sending notifications.

The bundle ID and team ID can be obtained as follows. The ID is equivalent to the bundle ID and the prefix is equivalent to the team ID.

![captura_de_pantalla_2018-11-12_a_las_10_30_59.png](/captura_de_pantalla_2018-11-12_a_las_10_30_59.png)

