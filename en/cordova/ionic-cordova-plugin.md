---
title: EMMA ionic SDK
description: Usage of EMMA Ionic Plugin
published: true
date: 2024-12-23T12:38:41.857Z
tags: ionic, cordova, plugin
editor: markdown
dateCreated: 2021-08-12T12:30:31.778Z
---

# Install
To add cordova plugin execute following command:
```bash
$ ionic cordova plugin add cordova-plugin-emma-sdk --variable ADD_PUSH=1
```
Once the installation is complete, `src/app/app.component.ts` add the following code: 

```tsx
import { Component } from "@angular/core";
import { AlertController, Platform } from "@ionic/angular";

declare var window: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private alertCtrl: AlertController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.initEMMA();
    });
  }

  initEMMA() {
    const EMMA = window.plugins.EMMA;

    const configuration= {
      sessionKey: '<SESSIONKEY>',
      debug: true
    };

    EMMA.startSession(configuration);
  } 
}
```




# Android integration

## Push (Optional)

### Dependencies
- Create the project in `Firebase` to be able to use **FCM**. https://firebase.google.com/docs/android/setup
- Add the Sender Id and Server Key provided by Firebase in the app configuration in EMMA dashboard. https://docs.emma.io/es/configuracion#preferencias-de-android
---

For the notification received in Android to have an image we have to add in the config.xml which will be the notification icon used.

*config.xml*
```xml
<resource-file src="resources/android/notification/drawable-mdpi-notification.png" target="app/src/main/res/drawable-mdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-hdpi-notification.png" target="app/src/main/res/drawable-hdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xhdpi-notification.png" target="app/src/main/res/drawable-xhdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xxhdpi-notification.png" target="app/src/main/res/drawable-xxhdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xxxhdpi-notification.png" target="app/src/main/res/drawable-xxxhdpi/notification.png" />

```

To activate the push it is necessary to add the configuration file `google-services.json` from the firebase project to the file` config.xml`

*config.xml*

```xml
<!-- Google services JSON from Firebase -->
<resource-file src="google-services.json" target="app/google-services.json" />
```

To be able to receive the push it is necessary to add the following service inside the AndroidManifest of the application by adding the following code in the `config.xml` file

*config.xml*

```xml
        <config-file parent="/manifest/application" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
            <service android:enabled="true" android:exported="false" android:name="io.emma.android.push.EMMAFcmMessagingService">
                <intent-filter>
                    <action android:name="com.google.firebase.MESSAGING_EVENT" />
                </intent-filter>
            </service>
        </config-file>
```

To initialize the push in the App, it is necessary to call the startPush method once the `deviceReady` event has been received within the `app.component.ts` file.

## Attribution, DeepLink and AppLink (Optional)

To integrate attribution it is necessary to configure DeepLink and AppLink in the app.

> {custom_scheme} must be replaced with the chosen schema name. Normally the chosen name has to do with the name of the app.

> { {basic_deeplink_scheme} replace with the basic deeplink schema e.g. emmaionic.

> {config_id} must be replaced by the id of the widget tag inside the config.xml file e.g. my.company.com.

In order for the app to open when running a DeepLink or AppLink, the following needs to be added to config.xml:

- Add the preference to open the app in singleTask mode to config.xml

```xml
<config-file parent="/manifest/application" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
   <activity android:exported="true" android:name="io.emma.android.activities.EMMADeepLinkActivity" android:noHistory="true" android:theme="@android:style/Theme.NoDisplay">
     <intent-filter autoVerify="true">
       <action android:name="android.intent.action.VIEW" />
       <category android:name="android.intent.category.DEFAULT" />
       <category android:name="android.intent.category.BROWSABLE" />
       <data android:host="{custom_scheme}.powlink.io" android:scheme="https" />
     </intent-filter>
     <intent-filter>
       <action android:name="android.intent.action.VIEW" />
       <category android:name="android.intent.category.DEFAULT" />
       <category android:name="android.intent.category.BROWSABLE" />
       <data android:scheme="{basic_deeplink_scheme}" />
     </intent-filter>
   </activity>
	 <meta-data android:name="io.emma.DEEPLINK_OPEN_ACTIVITY" android:value="{config_id}.MainActivity" />
</config-file>
```
Once this is added in the configuration it is necessary to add the event in the main component. This method is where the logic to process the link is added.

```tsx
this.platform.ready().then(() => {
      document.addEventListener('onDeepLink', (event) => {
      //process deeplink and applink
      });
...
```
**In case you are using your own deeplink callback**, ignore the previous part and add the opening intent-filters to your configuration activity:

```xml
     <intent-filter autoVerify="true">
       <action android:name="android.intent.action.VIEW" />
       <category android:name="android.intent.category.DEFAULT" />
       <category android:name="android.intent.category.BROWSABLE" />
       <data android:host="{custom_scheme}.powlink.io" android:scheme="https" />
     </intent-filter>
     <intent-filter>
       <action android:name="android.intent.action.VIEW" />
       <category android:name="android.intent.category.DEFAULT" />
       <category android:name="android.intent.category.BROWSABLE" />
       <data android:scheme="{basic_deeplink_scheme}" />
     </intent-filter>
```
Add the EMMA.handleLink method to process the link and send the click to EMMA.
```tsx
    document.addEventListener('myCustomCallback', (event) => {
      const url = event.url;
      EMMA.handleLink(url);
    });
```



## Location (Optional)

To activate the localization it is necessary to add the following permissions to the config.xml

```xml
        <config-file parent="/manifest" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
            <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
            <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
        </config-file>
```

Once the location has been added, it can be activated with the `EMMA.trackLocation()` method.

# iOS integration

## Push (Optional)


- Activate push capabilities in the project
https://developer.emma.io/en/ios/integracion-sdk#enable-push-capabilities
- Configure the Push Auth Key in EMMA
https://developer.emma.io/en/ios/integracion-sdk#push-auth-key
- Rich Push configuration
https://developer.emma.io/en/ios/integracion-sdk#rich-push

To activate the Push functionality you will only have to call the `EMMA.startPush()` method in `onDeviceReady`.

## Attribution, Deeplink and Universal Link

To correctly configure this functionality we will use the same dependencies as the Powlink integration in the native SDK: [https://developer.emma.io/es/ios/integracion-sdk#dependencias-1](https://developer.emma.io/es/ios/integracion-sdk#dependencias-1)

Once the dependencies are configured, it is necessary to add the event in the main component...
```tsx
    document.addEventListener('onDeepLink', (event) => {
      const url = event.url;
    });
```

## SKAdNetwork 4.0

To integrate SKAdNetwork see the native documentation [here](https://developer.emma.io/en/ios/integracion-sdk#skadnetwork-40-integration).

## Location.
Para usar la localización es necesario añadir los siguientes permisos en el archivo `Info.plist` de la App:
```xml
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
<key>NSLocationAlwaysUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
<key>NSLocationWhenInUseUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>

```
## Request IDFA

To request the IDFA, the following permission needs to be added to the `Info.plist`:

```xml
<key>NSUserTrackingUsageDescription</key>
	<string>Uso del indentificador para redes de terceros</string>

```


## TS Integration

## Login

We start the session using the startSession method.

```tsx
EMMA.startSession(configuration);
```

The configuration parameters for the login:

| Parámetro | Tipo | Descripción |
| --- | --- | --- |
| sessionKey | string | Key for identification at login. |
| apiUrl | string (Optional) | Add the url if a proxy is used. |
| queueTime | int (Optional) | Time to empty the transaction queue. |
| isDebug | boolean (Optional) | Enable/disable logging in the SDK. |
| customPowlinkDomains | string[] (Optional) |Add domains if the app uses custom domains in EMMA (subdomains that are configured in app preferences are not custom domains). |
| customShortPowlinkDomains | string[] (Optional) |  Add the short domains if the app uses custom domains in EMMA (the subdomains that are configured in the app preferences are not custom domains). |
| trackScreenEvents | boolean (Optional) | Enable/disable automatic screen tracking. |

## Push

Add the startPush method after startSession
```tsx
    const pushOptions = {
      classToOpen: 'io.emma.cordova.exampleionic.MainActivity',
      iconResource: 'notification'
    };

    this.EMMA.startPush(pushOptions);
```

StartPush parameters:
| Parámetro | Tipo | Descripción |
| --- | --- | --- |
| classToOpen | string | Indicates the opening activity in the case of clicking on the notification, in RN it is normal to have only one main activity so we add this one, for this we always indicate it with the absolute path {packageName}.MainActivity. | 
| iconResource | string | The icon that will contain the notification in the status bar and in the notification menu. The SDK looks for this icon in the drawable or drawable-xxx folder, so it is important that the name included in this parameter matches the drawable png. |
| color | string (Optional) | Colour in hexadecimal format. |
| channelId | string (Optional) |  If the application uses an existing notification channel, otherwise it will create a new one. |
| channelName | string (Optional) | Add the name of the new channel. |

> El uso del startPush es válido para Android e iOS, aunque los parámetros solo sean usados en Android ya que en iOS coge los valores por defecto de la App.
> 


## Events

EMMA allows the sending of custom events and several default events, the default `Open` event is sent when logging into the SDK, but the login and registration events are events that need to be added to the application code.

#### Custom events

I use the following method to send events:

```tsx
eventToken : EventParams ={
    eventRequest: "7b358954cf16bc2b7830bb5307f80f96",
    eventAttributes: {IONIC: "working"}

trackEvent(){
    this.EMMA.trackEvent(this.eventToken);
  }
```

#### Registration and login

To register and/or log in you need to send the user id in the app (Customer ID):

```tsx
EMMA.registerUser({
      userId:"emma",
	    email:"email.prueba@emma.io"
    });
EMMA.loginUser({
      userId:"emma",
	    email:"email@emma.io"
    });
```

## User properties

EMMA allows the sending of key/value properties associated with the device:
```tsx
trackUserExtraInfo(){
    this.EMMA.trackUserExtraInfo({TAG : "EMMA_EXAMPLE"});
}
```
## Location
To measure the user's location add the following method:

```tsx
EMMA.trackLocation();
```
The user's location will only be collected once at login.
```tsx
EMMA.startOrder({
    orderId: "EMMA",
    totalPrice: 100,
    customerId: "EMMA", 
    //opcionales
    coupon: "coupon",
    extras: {IONIC: "Working"}
});

EMMA.addProduct({
    productId: "SDK",
    productName: "SDK",
    quantity: 1,
    price: 1,
    extras: {IONIC: "Working"}
});

EMMA.trackOrder();
```
In the case of cancelling a purchase that has already been made, use the `cancelOrder` method.

```tsx
EMMA.cancelOrder();
```
## In-app messaging
EMMA allows the use of various communication formats:

- Adball
- Banner
- Startview
- Strip
- Native Ad

> Banner format is only supported for Android.

#### NativeAd format

The NativeAd format returns a JSON object with the native ad information. It is necessary to implement the `inAppResponse` callback to correctly handle the response. Example of use:

```tsx
const nativeAdType = INAPP_TYPE.NATIVE_AD
const templateId = 'template1'

EMMA.inAppMessage({  
  type: nativeAdType,  
  templateId: templateId,  
  inAppResponse: (nativeAdResponse: EMMANativeAd[]) => {  
    console.log('NativeAd', nativeAdResponse);
    // Process NativeAd response
    setNativeAds(nativeAdResponse);
  },  
});
```

As an example, here is a possible data model to receive the object returned in the NativeAd response:

```tsx
interface Fields {
  Subtitle: string;
  CTA: string;
  "Main picture": string;
  Title: string;
}

interface EMMANativeAd {
  id: number;
  templateId: string;
  cta: string;
  times: number;
  tag: string;
  showOn: string;
  fields: Fields;
}
```

#### All other formats

Any format other than NativeAd does not expect an explicit result, as it is automatically injected into the view. Example of use:

```tsx
EMMA.inAppMessage({ type });
console.log(`InApp ${type} message`);
```

## In-app events

The `Adball`, `Banner`, `Startview` and `Strip` formats, when injected, send `Print` and `Click` events automatically when these actions are performed. In the case of NativeAd, as it is a format managed by the developer, these methods must be added to notify these actions:

```tsx
EMMA.sendInAppImpression({
    campaignId: nativeAdResponse[0].id,
    type: IN_APP_TYPE.NATIVE_AD,
});

EMMA.sendInAppClick({
    campaignId: nativeAdResponse[0].id,
    type: IN_APP_TYPE.NATIVE_AD,
});
```

The `openNativeAd` method allows to open a NativeAd according to its configuration. This method accepts three parameters, already obtained in the NativeAd response: `campaignId`, `cta` and `showOn`. If this method is used, there is no need to use the `sendInAppClick` as it is included internally. Example of use:

```tsx
EMMA.openNativeAd(
    nativeAdResponse[0].id, 
    nativeAdResponse[0].fields.CTA, 
    nativeAdResponse[0].showOn
);
```

## GDPR

In order to control the use of the data protection law EMMA provides several methods to activate/deactivate the tracking of users.

```tsx
// Enable user tracking
EMMA.enableUserTracking();

//Disable user tracking
EMMA.disableUserTracking(false);
```

The `disableUserTracking` method provides a flag to permanently remove the user in case tracking is disabled. Note that this action is irreversible and all data associated with the user will be lost.

To check if tracking is enabled or disabled use the following method:

```tsx
const isEnabled = EMMA.isUserTrackingEnabled();
```
## Sending the customerId

If the app does not use login/registration events to notify the customerId you can send it with the following method:

```tsx
EMMA.setCustomerId(customerId);
```


## Push token sending

In case the push methods of the EMMA SDK are not used, the push token can be sent to send push notifications. In case the SDK methods are added, this method is not necessary.

```tsx
EMMA.sendPushToken(token);
```
## Request IDFA (iOS only)

To request the IDFA identifier use the following method:
```tsx
EMMA.requestTrackingWithIdfa();
```


## Request permission for notifications (Android only)

Since Android 13 to receive notifications it is necessary to request permission from the user. As of version 1.3.0 the following method for requesting permission has been added:
```tsx
EMMA.requestNotificationPermission();
```
# Reference

- Example project: [https://github.com/EMMADevelopment/EMMAIonicExample](https://github.com/EMMADevelopment/EMMAIonicExample)
- Configuration example: [https://github.com/EMMADevelopment/EMMAIonicExample/blob/master/config.xml](https://github.com/EMMADevelopment/EMMAIonicExample/blob/master/config.xml)
- Initialisation example: [https://github.com/EMMADevelopment/EMMAIonicExample/blob/master/src/app/app.component.ts](https://github.com/EMMADevelopment/EMMAIonicExample/blob/master/src/app/app.component.ts)

