---
title: Guía de integración rápida
description: Guía de integración rápida del SDK de EMMA con React Native
published: true
date: 2025-02-04T12:59:33.222Z
tags: ios, react, react-native, android, quick-start, quick-wins
editor: markdown
dateCreated: 2024-05-27T11:24:56.090Z
---

# Referencias

## Documentación

- [📖 Integración completa *Detalle para integrar el SDK de EMMA para React Native*](https://developer.emma.io/es/react-native/integration)
{.links-list}

## Repositorios de ejemplo

- [⚛️ React Native *App de ejemplo en GitHub*](https://github.com/EMMADevelopment/emma-react-native-sdk)
{.links-list}

# Descarga de EMMA

- Yarn: `yarn add emma-react-native-sdk`
- NPM: `npm install emma-react-native-sdk`

Ejecuta `pod install` en la ruta `ios/` de la app

# Integración básica

- Integra EMMA en tu archivo:

```javascript
import EmmaSdk from 'emma-react-native-sdk';
```

- Comprueba que inicializa correctamente:

```javascript
EmmaSdk.startSession(
  {
    sessionKey: 'EXAMPLE_EMMA_SESSION_KEY',
  },
  () => {
    console.log('Got it!');
  },
  (error) => {
    console.error('Oh, oh!', error);
  }
);
```

> Consulta la integración completa en la [documentación](https://developer.emma.io/es/react-native/integration).
{.is-info}

# Integración en Android

Es importante que en `AndroidManifest.xml` la *Activity* `MainActivity` tenga como atributo `launchMode="singleTask"`:

```xml
<activity
    android:name=".MainActivity"
    android:label="@string/app_name"
    android:configChanges="keyboard|keyboardHidden|orientation|screenSize|uiMode"
    android:launchMode="singleTask"
    android:windowSoftInputMode="adjustResize">
```

## Push

- Añadir el servicio push de EMMA en el `AndroidManifest.xml` de la app de Android, dentro del tag `<application>`:

```xml
<service
    android:name="io.emma.android.push.EMMAFcmMessagingService"
    android:enabled="true"
    android:exported="false">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT"/>
    </intent-filter>
</service>
```

- Indicar el uso de `google-services` en `android/build.gradle`:

```xml
buildscript {
    ....
    repositories {
        ....
        google()
    }
    dependencies {
       ...
        classpath 'com.google.gms:google-services:4.3.15'
    }
}
```

- Añadir `google-services` en el archivo `android/app/build.gradle`:

```xml
dependencies {
    ....
    implementation 'com.google.firebase:firebase-messaging:21.0.1'
}

apply plugin: 'com.google.gms.google-services'
```

> Importante añadir `google-services.json` obtenido en la consola de Firebase en la ruta `android/app/`.
{.is-warning}

> Puedes encontrar más información sobre el Push [aquí](https://developer.emma.io/es/android/integracion-sdk#integraci%C3%B3n-notificaciones-push).
{.is-info}

## Powlink y Deeplink

- Añadir la siguiente *Activity* al `AndroidManifest.xml` de la app, dentro de `<application>`:

```xml
<activity
          android:name="io.emma.android.activities.EMMADeepLinkActivity"
          android:noHistory="true"
          android:exported="true"
          android:theme="@android:style/Theme.NoDisplay">

          <intent-filter>
              <action android:name="android.intent.action.VIEW"/>

              <category android:name="android.intent.category.DEFAULT"/>
              <category android:name="android.intent.category.BROWSABLE"/>

              <data android:scheme="${DEEPLINK_SCHEME}"/>
          </intent-filter>

          <intent-filter android:autoVerify="true">
              <action android:name="android.intent.action.VIEW"/>

              <category android:name="android.intent.category.DEFAULT"/>
              <category android:name="android.intent.category.BROWSABLE"/>

              <data
                  android:host="${CUSTOM_POWLINK_DOMAIN}.powlink.io"
                  android:scheme="https"/>

              <data
                  android:host="${CUSTOM_SHORT_POWLINK_DOMAIN}.pwlnk.io"
                  android:scheme="https"/>

          </intent-filter>
      </activity>
```

- Reemplazar `${DEEPLINK_SCHEME}` por el esquema de deeplink de la app, por ejemplo: emmarn (de emmarn://). El esquema básico de deeplink se utiliza para realizar acciones desde comunicación In-App, también para utilizar la *landing* de redirección de deeplinks en AppTracker.

- Reemplazar `${CUSTOM_POWLINK_DOMAIN}` y `${CUSTOM_SHORT_POWLINK_DOMAIN}` por los dominios configurados en el dashboard de EMMA en la sección de preferencias de la app.

- Debajo de la *Activity*, en el mismo `AndroidManifest.xml`, hay que añadir también el siguiente `<meta-data>`:

```xml
<meta-data
    android:name="io.emma.DEEPLINK_OPEN_ACTIVITY"
    android:value="${PACKAGE_NAME}.MainActivity"/>
```

- Reemplazar `${PACKAGE_NAME}` por la ruta del fichero `MainActivity`, por ejemplo (com.example). Este meta-data indica al SDK de EMMA qué *Activity* tiene que abrir al recibir un deeplink, en el caso de los SDKs híbridos solo suele haber una *Activity*.

# Integración en iOS

## Push

- Activar la `Capability > Push Notification`.
- Importar en el `AppDelegate` el *bridge* de EMMA para React Native:

```swift
// EMMA bridge import
import emma_react_native_sdk
```

- Añadir en el mismo archivo el método para notificar el delegado al bridge de EMMA y añadir los métodos delegados para recibir las notificaciones.

```swift
import UIKit
import UserNotifications

// EMMA bridge import
import emma_react_native_sdk

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, RCTBridgeDelegate, UNUserNotificationCenterDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    //RCT Bridge initialization

    // Pass push delegate to EMMA bridge
    if #available(iOS 10.0, *) {
      EmmaReactNative.setPushNotificationsDelegate(self)
    }

    return true
  }

  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    EmmaReactNative.registerToken(deviceToken)
  }

  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    NSLog("Error registering notifications " + error.localizedDescription);
  }

  @available(iOS 10.0, *)
  func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
    EmmaReactNative.willPresent(notification)
    completionHandler([.badge, .sound])
  }

  @available(iOS 10.0, *)
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
     EmmaReactNative.didReceive(response, withActionIdentifier: response.actionIdentifier)
     completionHandler()
  }
}
```

## Powlink y Deeplink

- Añadir en el `AppDelegate` el *bridge* de EMMA para RN y la librería Linking de RN. 

```swift
// EMMA bridge import
import emma_react_native_sdk
```

> Si estás usando Swift en la app, la librería Linking está implementada en Objective-C. Por ello, es necesario añadirla en el fichero `App-Bridging-Header.h` creado para hacer de *bridge* entre Objective-C y Swift.
{.is-info}

```swift
// Imports for EMMA
#import <React/RCTLinkingManager.h>
```

- Añadir los siguientes métodos al `AppDelegate`.

	- El método `openURL` recoge todos los deeplinks que abren la aplicación.
  - El método `continueUserActivity` recoge los Universal Links que abren la aplicación.

```swift
import UIKit

// EMMA bridge import
import emma_react_native_sdk


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, RCTBridgeDelegate, UNUserNotificationCenterDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    //RCT Bridge initialization

    return true
  }

  func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
    EmmaReactNative.handleLink(url)
    return RCTLinkingManager.application(UIApplication.shared, open: url, options: options)
  }

  func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {

    if let webUrl = userActivity.webpageURL {
      EmmaReactNative.handleLink(webUrl)
    }

    return RCTLinkingManager.application(application, continue: userActivity, restorationHandler: restorationHandler)
  }
```

Para añadir el Powlink (Universal Link) dirigete a la sección `Capabilities` dentro del *target* de la app en Xcode y añade `Associate domains`. Seguidamente, añadiremos el Powlink con el formato `applinks:customsubdomain.powlink.io`.  

> Nótese que `customdomain` es el subdominio configurado en el Dashboard de EMMA.
{.is-warning}

![powlink_adding_capabilities](/react-native/powlink_adding_capabilities.png)

![added_powlink_capabilities](/react-native/added_powlink_capabilities.png)

En el caso del deeplink básico, es necesario añadir el esquema en la sección `Info` dentro del *target* de la app en Xcode y en `URL Types` añade una nueva entrada dónde puedes definir el esquema de la app.

![added_urltype_info.png](/react-native/added_urltype_info.png)

Para más información sobre los dominios de powlink y cómo configurar los dominios en el Dashboard, dirígete [aquí](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu).

# Powlink

- Una vez abierta la aplicación añade los siguientes métodos para obtener el deeplink o powlink:

```javascript
// Listen to deeplink requests
Linking.addEventListener('url', ({ url }) => setDeeplink(url));

// Detect application launchings from a deeplink
const handleInitialDeeplink = () =>
Linking.getInitialURL()
  .then((url) => setDeeplink(url || null))
  .catch(() => setDeeplink(null));
```

# Notificaciones push

Añade el `startPush` en el archivo `App.tsx` siempre y cuando hayas realizado el `startSession`:

```javascript
const handleStartSession = async () => {
    console.log('Starting session...');
    try {
      await EmmaSdk.startSession(startSessionParams);
      EmmaSdk.startPush({
        classToOpen: '{PACKAGE_NAME}.MainActivity', //PACKAGE_NAME es el path absoluto de la activity
        iconResource: 'notification_icon',
     });
      console.log('Session started on', Platform.OS);
    } catch (err) {
      console.error('Session failed to start', err);
    }
};

useEffect(() => {
    handleStartSession();
}, []);
```

> `classToOpen` y `iconResource` son parámetros obligatorios.
{.is-info}

# Mensajes In-App

## NativeAd

Devuelve un objeto JSON que contiene el NativeAd.

```javascript
const type = INAPP_TYPE.NATIVE_AD
 const templateId = 'template1'
try {
   const result = await EmmaSdk.inAppMessage({ type, templateId });
   // Process Native Ad result
   setNativeAds(result);
   console.log(`InApp ```${type}``` message`, result);
 } catch (err) {
   console.error('InApp message error', err);
 }
```

## StartView

El StartView configurado se inyecta en la vista al realizar esta llamada.

```javascript
try {
  await EmmaSdk.inAppMessage({IN_APP_TYPE.STARTVIEW});
  console.log(`InApp ${type} message`);
} catch (err) {
  console.error('InApp message error', err);
}
```

## AdBall

El AdBall configurado se inyecta en la vista al realizar esta llamada.

```javascript
try {
  await EmmaSdk.inAppMessage({IN_APP_TYPE.ADBALL});
  console.log(`InApp ${type} message`);
} catch (err) {
  console.error('InApp message error', err);
}
```

## Banner

El Banner configurado se inyecta en la vista al realizar esta llamada.

```javascript
try {
  await EmmaSdk.inAppMessage({IN_APP_TYPE.BANNER});
  console.log(`InApp ${type} message`);
} catch (err) {
  console.error('InApp message error', err);
}
```

> El formato Banner solo está soportado para Android.
{.is-info}

## Strip

El Strip configurado se inyecta en la vista al realizar esta llamada.

```javascript
try {
  await EmmaSdk.inAppMessage({IN_APP_TYPE.STRIP});
  console.log(`InApp ${type} message`);
} catch (err) {
  console.error('InApp message error', err);
}
```

# Eventos

## Registro

```javascript
const userParams: LoginRegisterUserParams = {
  userId: 'user#12345',
};

const handleRegisterUser = () => {
    EmmaSdk.registerUser(userParams);
};
```

## Login

```javascript
const userParams: LoginRegisterUserParams = {
  userId: 'user#12345',
};

const handleLoginUser = () => {
    EmmaSdk.loginUser(userParams);
};
```

## Evento personalizado

```javascript
EmmaSdk.trackEvent({
    eventToken: '7b358954cf16bc2b7830bb5307f80f96',
    eventAttributes: { ReactNative: 'true' }, // optional
});
```

## Evento de compra

Consta de los métodos `startOrder`, `addProduct`, `trackOrder` y `cancelOrder`.

```javascript
EmmaSdk.startOrder({
  orderId: 'EMMA',
  totalPrice: 100,
  customerId: 'EMMA',
  currencyCode: 'EUR',
});

// Add products
EmmaSdk.addProduct({
  productId: 'SDK',
  productName: 'SDK',
  quantity: 1,
  price: 1,
  extras: { ReactNative: 'working' },
});

// Commit order
EmmaSdk.trackOrder();
//Cancel order
EmmaSdk.cancelOrder('EMMA');
```

# Envío de etiquetas de usuario

Envía un elemento Clave/Valor como etiqueta de usuario de cara a obtener mejor segmentación en el filtrado `Users with tag`.

```javascript
EmmaSdk.trackUserExtraInfo({ userTags: { TAG: 'EMMA_EXAMPLE' } });
```
