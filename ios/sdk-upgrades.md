---
title: Actualizaciones EMMA SDK iOS
description: 
published: true
date: 2025-02-10T08:19:21.181Z
tags: 
editor: markdown
dateCreated: 2020-11-11T15:16:02.695Z
---

# 4.15.2 - 10/02/2025
* **IMPROVE** Ahora el StartView se muestra por encima del Prisma y cualquier formato de comunicación.

# 4.15.1 - 26/12/2024

- **IMPROVE** Se ha implementado una solución para evitar que StartView, AdBall y Strip aparezcan simultáneamente.
- **IMPROVE** Visualización mejorada de AdBall cuando se ejecuta en UIKit con un Strip en pantalla.
- **IMPROVE** Añadido *padding* al AdBall en las esquinas inferiores para mejorar UI/UX.
- **IMPROVE** Se han ajustado las dimensiones de la *trashView* para resolver problemas con el arrastre del AdBall.
- **FIX** Se ha corregido un problema por el que las reglas no se ejecutaban cuando un Strip estaba activo.

# 4.15.0 - 26/09/2024

- **NEW** Añadido un nuevo callback de información de atribución de instalación en la sesión de inicio.
- **NEW** Cuando se abre un enlace con el parámetro `st_open_browser=true` en la WebView, la URL se lanza en el navegador.
- **IMPROVE** Las sesiones con tiempos inferiores a 10 segundos se descartan.

# 4.14.0 - 11/07/2024

- **NEW** Añadido nuevo método `sendDismissedClick` para enviar clics cuando la inapp es descartada.

# 4.13.1 - 21/03/2024

- **NEW** Añadida firma `PrivacyInfo.xcprivacy` y XCFramework.

# 4.13.0 - 05/02/2024

- **NEW** Se elimina el método `setCurrencyCode`. `currencyCode` se selecciona en el panel de control de EMMA. Todos los precios enviados en la orden no serán convertidos por EMMA a una moneda específica, serán interpretados como un valor unitario.
- **NEW** Añadidos desplazamientos de banner (*offsets* superior e inferior).
- **FIX** Corrección de errores menores.

# 4.12.3 - 17/01/2024

- **FIX** Arreglado que el Strip no se muestre en aplicaciones basadas en SwiftUI.

# 4.12.2 - 12/01/2024

- **FIX** Se ha corregido el problema de no enviar un `currencyCode` distinto de EUR en la solicitud `trackOrder`.

# 4.12.1 - 04/07/2023

- **IMPROVE** Añadido `updatePostbackConversionValue` con el parámetro `lockWindow`.

# 4.12.0 - 05/06/2023

- **CHANGE** Deshabilitado el soporte de bitcode y eliminadas las arquitecturas i386 y armv7. Apple deja obsoleto el código de bits con Xcode14: https://developer.apple.com/documentation/xcode-release-notes/xcode-14-release-notes
- **CHANGE** Se requiere XCode14.1+: https://developer.apple.com/news/upcoming-requirements/?id=04252023a
- **CHANGE** Objetivo de despliegue de iOS actualizado a iOS 11. Compatible con dispositivos con iOS 11+.
- **IMPROVE** El tamaño del SDK se ha reducido de ~14mb a ~4mb.
- **IMPROVE** Soporte del título en las notificaciones push.
- **FIX** Algunos deeplinks dentro de StartViews se ejecutan antes de que el StartView se cierre, causando superposición de ViewControllers.
- **NEW** Soporte para SKAdNetwork 4.

# 4.11.4 - 23/03/2023

- **FIX** A veces el Strip se oculta cuando se cierra el controlador de vista. Ahora el Strip se muestra en todos los ViewController.
- **NEW** También se ha añadido el método `closeStrip` para darle la libertad de cerrar el Strip cuando se quiera.

# 4.11.3 - 17/01/2023

- **REMOVE** Eliminado el soporte para Apple Search Ads `iAd.framework`. Sólo para iOS < 14.3, a partir de la versión 4.10.2 se utiliza `AdService.framework`.

# 4.11.2 - 15/11/2022

- **NEW** Cerrar WebView desde web con deeplink `://startview/close`.
- **FIX** Corregido error añadido en 4.11.1, StartView y AdBall con reglas no funcionan correctamente.

# 4.11.1 - 20/10/2022

- **FIX** Corregido StartView, algunas veces se bloquea cuando se inicia fuera del hilo principal.
- **FIX** Corregido fallo con fechas nulas en el proceso de atribución.

# 4.11.0 - 05/05/2022

- **NEW** Tipos de clic para las comunicaciones inapp Adball y StartView.

# 4.10.3 - 24/03/2022

- **FIX** Corregida la fecha de finalización del cupón.

# 4.10.2 - 03/03/2022

- **FIX** Corregido crash crítico producido en producción sólo con app con SDK iniciado y petición de atribución de ASA. Las versiones 4.10.0 y 4.10.1 han sido eliminadas por afección.

# 4.10.1 - 07/02/2022
###### Deleted version

- **FIX** Corregida la Atribución de Apple con `AdServices.framework`. En iOS 14.3 a 14.8.1 podría producirse un cuelgue.

# 4.10.0 - 20/01/2022
###### Deleted version

- **NEW** Notificaciones con botones de acción.

# 4.9.3 - 07/10/2021

- **FIX** Corregido el problema del banner al salir del `rootViewController` y errores menores.

# 4.9.2 - 27/04/2021

- **FIX** En las primeras versiones de iOS 14 a veces el IDFA no se obtenía correctamente.
- **FIX** Corregido error `customerId` en compras añadidas en la versión 4.9.0.

# 4.9.1 - 18/03/2021

- **FIX** Corregido `EMMAEventRequest` como opcional al instanciar.

# 4.9.0 - 13/03/2021

- **CHANGE** Cambiado el *deployment target* a iOS 9.
- **NEW** SDK parcialmente reescrito en Swift. Se han adaptado varias APIs para que resulten más cómodas en el lenguaje Swift.
- **NEW** Nuevo método para añadir plugins inapp.
- **REMOVE** Método eliminado `+(void)startSession:(NSString*)appKey withOptions:(NSDictionary*)launchOptions __attribute__((deprecated("Use startSession without options")));`.
- **REMOVE** Método eliminado `+(void) startPushSystem: (NSDictionary*) launchOptions __attribute__((deprecated("Use startPushSystem without parameters")));`.
- **REMOVE** Método eliminado `+(void)startOrder:(NSString*)orderId customerId:(NSString*)customerId totalPrice:(float)totalPrice coupon:(NSString*)coupon;`.
- **REMOVE** Método eliminado `+(void)registerUser:(NSString*)userId forMail: (NSString*)mail;` y sustituidos por opcionales.
- **REMOVE** Método eliminado `+(void)loginUser:(NSString*)userId forMail:(NSString*)mail;` y sustituidos por opcionales.
- **REMOVE** Método eliminado `+(void)addProduct:(NSString*)productId name:(NSString*)name qty:(float)qty price:(float)price;`.
- **REMOVE** Método eliminado `+(void)addRateAlertForAppStoreURL:(NSString*)appStoreURL;`.
- **REMOVE** Método eliminado `+(void) setRateAlertFreq: (int) hours;`.
- **REMOVE** Método eliminado `+(void) setRateAlertTitle: (NSString*) title;`.
- **REMOVE** Método eliminado `+(void) setRateAlertMessage: (NSString*) message;`.
- **REMOVE** Método eliminado `+(void) setRateAlertCancelButton: (NSString*)>cancelButtonText;`.
- **REMOVE** Método eliminado `+(void) setRateAlertRateItButton: (NSString*) rateItButtonText;`.
- **REMOVE** Método eliminado `+(void) setRateAlertLaterButton: (NSString*) laterButtonText;`.
- **REMOVE** Método eliminado `+(void) setRateAlertShowAfterUpdate:(BOOL) showAlert;`.
- **RENAME** Método renombrado `EMMA.add(inAppDelegate: self)` por `EMMA.addInAppDelegate(delegate: self)`.
- **RENAME** Método renombrado  `EMMA.trackEvent(eventRequest)` por `EMMA.trackEvent(request: eventRequest)`.
- **RENAME** Método renombrado `EMMA.handleLink(url)` por `EMMA.handleLink(url: url)`.
- **RENAME** Método renombrado `EMMA.handlePush(userInfo)` por `EMMA.handleLink(userInfo: userInfo)`.
- **RENAME** Método renombrado `EMMA.openNativeAd(nativeAdCampaignId: campaignId)` por `EMMA.opeNativeAd(campaignId: campaignId)`.

# 4.8.1 - 07/01/2021

- **FIX** Corrección del error "Collection was mutated while being enumerated" en Campaign Controller.
- **FIX** Corrección de errores menores.

# 4.8.0 - 12/11/2020

- **IMPROVE** Cambiada la gestión de los identificadores a nivel interno
- **NEW** Nuevo método para obtener el IDFA solicitando los permisos de tracking en iOS 14.
- **NEW** Nuevo método para actualizar el customer id sin necesidad de enviar el evento login o registro.
- **FIX** Bugs menores y mejoras.

```swift
 if #available(iOS 14.0, *) {
   EMMA.requestTrackingWithIdfa()
 }
        
 EMMA.setCustomerId("198284343")
```

# 4.6.7 - 11/09/2020
- **FIX** Arreglado un crash que se daba en auto events con carácteres extraños en el título.

# 4.6.6 - 23/07/2020
- **FIX** Arregla un crash introducido en la versión 4.6.5.

# 4.6.5 - 23/07/2020

- **IMPROVE** Se guardan los de callbacks del delegado en la instancia.

# 4.6.4 - 01/07/2020

- **NEW** Nuevo método para obtener el deviceId.

# 4.6.3 - 28/05/2020

- **FIX** Arreglados parámetros de la campaña en rules.

# 4.6.2 - 07/04/2020

- **NEW** Parámetros del click para en la instalación durante la atribución de campañas.
- **FIX** Bugs menores

En la versión 4.6.2 o superior del SDK se ha añadido el código de extensión dentro de un método SDK. Reemplaza todo el contenido del archivo con el código de abajo:

```swift
import UIKit
import UserNotifications
import EMMA_iOS

class NotificationService: UNNotificationServiceExtension {
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            EMMA.didReceive(request, with: bestAttemptContent) { (content) in
                contentHandler(bestAttemptContent)
            }
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
}
```
Para añadir el SDK a la extensión es necesario modificar el archivo Podfile añadiendo el pod eMMa al objetivo de la extensión:
```ruby
target 'ProjectName' do
  pod 'eMMa', '~> 4.6.2'
end

target 'EMMANotificationServiceExtension' do
  pod 'eMMa', '~> 4.6.2'
end
```

# Actualización a versión 4.6
Se ha eliminado del delegado EMMAPushDelegate los métodos pushMessage y pushTag y se ha añadido el método onPushOpen. A este método se le pasa la campaña de push que se ha abierto en ese momento. Esta campaña tiene atributos como tag o message, que reemplazarían los métodos eliminados.
```objc
 -(void)onPushOpen:(EMMAPush*)push
```

# Actualización a la versión 4.5
Las versiones previas que se actualicen a la 4.5 deberán realizar los siguientes cambios.

## Cambios en Eventos

Los siguiente métodos que quedaron deprecados en la versión 4.4 se han eliminado en la 4.5:

```objc
+(void) trackEvent:(NSString*)token 
+(void) trackEvent:(NSString *)token withAttributes: (NSDictionary*) attributtes
+(void) trackEventWithRequest:(EMMAEventRequest *) request
```

Ambos se remplazan por el siguiente método:

```objc
+(void) trackEvent:(EMMAEventRequest *) request
```
Ejemplo de uso:
```swift
let eventRequest = EMMAEventRequest.init(token: "<token>")
// Optional: You can add your custom event attributes
eventRequest?.attributes = ["test_attribute":"test_value"]
// Optional. You can capture emma requests with this delegate
eventRequest?.requestDelegate = self
// Optional. Append your request ID to capture it later
eventRequest?.customId = "MY_EVENT_REQUEST"
        
EMMA.trackEvent(eventRequest)
```

## Cambios en InApp

Los siguiente métodos que quedaron deprecados en la 4.4 se han eliminado en la 4.5:

```objc
+(void)inAppMessage:(InAppType)type andRequest:(EMMAInAppRequest*) request
+(void)inAppMessage:(InAppType)type andRequest:(EMMAInAppRequest*) request withDelegate:(id) delegate
```
Ambos se remplazan por los siguientes métodos:
```objc
+(void)inAppMessage:(EMMAInAppRequest*) request
+(void)inAppMessage:(EMMAInAppRequest*) request withDelegate (id) delegate
```
Ejemplo de uso:
```swift
import UIKit
import EMMA_iOS

class InAppExampleViewController: UIViewController, EMMAInAppMessageDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getStartView()
        // You can add as many request delegates as you want
        EMMA.add(inAppDelegate: self)
        
        //Check if any adball is showing
        if EMMA.isAdBallShowing() {
            print("There is an adball floating arround")
        }
    }
    
    // MARK: - EMMA InApp Messages Requests
    
    func getStartView() {
        let startViewinAppRequest = EMMAInAppRequest(type: .Startview)
        // Optional. You can filter by label
        startViewinAppRequest?.label = "<LABEL>"
        /*
         By default Startview presents on UIApplication.shared.delegate?.window?.rootViewController
         You can customize this behavior uncommenting following line
        */
        //EMMA.setRootViewController(UIViewController!)
        EMMA.inAppMessage(startViewinAppRequest)
    }
    
    func getBanner() {
        let bannerRequest = EMMAInAppRequest(type: .Banner)
        EMMA.inAppMessage(bannerRequest)
    }
    
    func getAdBall() {
        let adballRequest = EMMAInAppRequest(type: .Adball)
        EMMA.inAppMessage(adballRequest)
    }
    
    func getDynamicTabBar() {
        // You must define your UITabBarController
        // Uncomment following line!
        // EMMA.setPromoTabBarController(UITabBarController!)
        
        // Sets default promo tab index if not defined in EMMA Platform
        EMMA.setPromoTabBarIndex(5)
        
        // Sets a tab bar item to be shown if not defined in EMMA Platform
        // EMMA.setPromoTabBarItem(UITabBarItem!)
        
        let dynamicTabBarRequest = EMMAInAppRequest(type: .PromoTab)
        EMMA.inAppMessage(dynamicTabBarRequest)
    }
    
    func getStrip() {
        let stripRequest = EMMAInAppRequest(type: .Strip)
        EMMA.inAppMessage(stripRequest)
    }
    
    
    // MARK: - EMMA InApp Message Delegate
    
    func onShown(_ campaign: EMMACampaign!) {
        guard let c = campaign else {
            print("Error getting campaign info")
            return
        }
        print("Shown campaign \(c)")
    }
    
    func onHide(_ campaign: EMMACampaign!) {
        guard let c = campaign else {
            print("Error getting campaign info")
            return
        }
        print("Hide campaign \(c)")
    }
    
    func onClose(_ campaign: EMMACampaign!) {
        guard let c = campaign else {
            print("Error getting campaign info")
            return
        }
        print("Closed campaign \(c)")
    }
}
```

>Si el mensaje es para solicitar un Native Ad es obligatorio usar EMMANativeAdRequest (subclase de EMMAInAppRequest) para añadir el templateId y su delegado específico.
{.is-info}

## Cambios en Push

En esta nueva versión se han realizado mejoras en la gestión de notificaciones push.

Quedan deprecados los siguientes métodos:

```objc
+(void) startPushSystem: (NSDictionary*) launchOptions
+(void) startSession:(NSString*)appKey withOptions:(NSDictionary*)launchOptions
```
Debido a unas mejoras en el SDK simplemente añadiendo la disposición de los delegados de Push no hace falta notificar las `launchOptions` al SDK.