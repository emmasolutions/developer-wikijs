---
title: Manifiesto de privacidad
description: 
published: true
date: 2024-08-20T07:24:48.593Z
tags: 
editor: markdown
dateCreated: 2024-03-17T16:04:20.474Z
---

# Manifiesto de Privacidad

> En la WWDC23, Apple presentó las etiquetas nutricionales de privacidad, que ofrecen a los usuarios una descripción general completa de cómo una aplicación maneja los datos. Estas etiquetas brindan información valiosa sobre las prácticas de privacidad de las aplicaciones con las que interactúan los usuarios.
{.is-info}

Con los manifiestos de privacidad, un desarrollador de aplicaciones podrá crear un registro de las categorías de datos que recopila a través de su aplicación de forma independiente y mediante el uso de dependencias de terceros, como los SDK de terceros utilizados por la aplicación. Esta información ayuda a los desarrolladores de aplicaciones a representar con precisión las prácticas de privacidad en sus aplicaciones, incluso a través de las etiquetas nutricionales que proporcionan a sus usuarios.

En muchos casos, los desarrolladores de aplicaciones no saben qué datos recopilan los SDK de terceros o para qué los utiliza un SDK específico. El Manifiesto de Privacidad permite que los SDK de terceros pasen fácilmente esta información a los desarrolladores de aplicaciones, ayudándoles a crear etiquetas nutricionales precisas.

**Apple planea, a partir del 1 de mayo de 2024, poner en vigor esta funcionalidad a todas las apps que se creen nuevas o realicen una actualización**, EMMA ha actualizado el SDK de iOS en todas las plataformas, la nueva versión incluye el Manifiesto de Privacidad para facilitar el proceso de generación de etiquetas nutricionales de los desarrolladores de aplicaciones. Esta versión también incluye la firma recomendada por Apple para garantizar la integridad de los binarios. 

> Esta actualización del SDK no es obligatoria, si no deseas actualizar el SDK puedes tomar como referencia el ejemplo de Manifiesto de Privacidad que añadimos más abajo. Recuerda que enviar un Manifiesto de Privacidad para revelar los permisos del SDK de EMMA es opcional, pero si hay que añadir las APIs utilizadas en NSPrivacyAccessedAPITypes en el PrivacyInfo.xcprivacy de la app. Consulta la [lista de SDK de terceros de Apple que requieren un manifiesto de privacidad con permisos.](https://developer.apple.com/support/third-party-SDK-requirements/)
{.is-info}

A continuación se muestra un listado de la plataforma y versión de SDK la cual el Manifiesto de Privacidad y la firma están disponibles:


| Plataforma           | Versión |
|---------------|------------------------------------------|
| iOS Nativo    | >= 4.13.1                                |
| Cordova/IONIC | >= 1.7.1                                 |
| React Native  | >= 1.6.0                                 |
| Flutter       | >= 1.4.0                                 |

En el caso de no actualizar el SDK, añade en el fichero PrivacyInfo.xcprivacy de la app el siguiente código:

```xml
<dict>
    <key>NSPrivacyAccessedAPITypes</key>
    <array>
        <dict>
            <key>NSPrivacyAccessedAPIType</key>
            <string>NSPrivacyAccessedAPICategoryUserDefaults</string>
            <key>NSPrivacyAccessedAPITypeReasons</key>
            <array>
                <string>CA92.1</string>
            </array>
        </dict>
    </array>
</dict>
```

## Ejemplo de Manifiesto de Privacidad

Este es un ejemplo de Manifiesto de Privacidad del SDK de EMMA con la configuración por defecto del SDK, sin añadir métodos adicionales, simplemente iniciando el SDK con el método *startSession* como único método configurado. **Es posible que este Manifiesto difiera mucho de las necesidades de tu app por eso no debe tomarse al pie de la letra.**

![privacy-manifest-example.png](/ios/privacy-manifest-example.png)

Fragmento de código del **ejemplo** de Manifiesto de Privacidad:

```xml
<dict>
    <key>NSPrivacyCollectedDataTypes</key>
    <array>
        <dict>
            <key>NSPrivacyCollectedDataType</key>
            <string>NSPrivacyCollectedDataTypeOtherDataTypes</string>
            <key>NSPrivacyCollectedDataTypeLinked</key>
            <true/>
            <key>NSPrivacyCollectedDataTypeTracking</key>
            <true/>
            <key>NSPrivacyCollectedDataTypePurposes</key>
            <array>
                <string>NSPrivacyCollectedDataTypePurposeAnalytics</string>
                <string>NSPrivacyCollectedDataTypePurposeProductPersonalization</string>
            </array>
        </dict>
        <dict>
            <key>NSPrivacyCollectedDataType</key>
            <string>NSPrivacyCollectedDataTypeProductInteraction</string>
            <key>NSPrivacyCollectedDataTypeLinked</key>
            <true/>
            <key>NSPrivacyCollectedDataTypeTracking</key>
            <false/>
            <key>NSPrivacyCollectedDataTypePurposes</key>
            <array>
                <string>NSPrivacyCollectedDataTypePurposeAnalytics</string>
                <string>NSPrivacyCollectedDataTypePurposeProductPersonalization</string>
            </array>
        </dict>
        <dict>
            <key>NSPrivacyCollectedDataType</key>
            <string>NSPrivacyCollectedDataTypeDeviceID</string>
            <key>NSPrivacyCollectedDataTypeLinked</key>
            <true/>
            <key>NSPrivacyCollectedDataTypeTracking</key>
            <true/>
            <key>NSPrivacyCollectedDataTypePurposes</key>
            <array>
                <string>NSPrivacyCollectedDataTypePurposeDeveloperAdvertising</string>
                <string>NSPrivacyCollectedDataTypePurposeAppFunctionality</string>
                <string>NSPrivacyCollectedDataTypePurposeAnalytics</string>
                <string>NSPrivacyCollectedDataTypePurposeProductPersonalization</string>
            </array>
        </dict>
    </array>
    <key>NSPrivacyAccessedAPITypes</key>
    <array>
        <dict>
            <key>NSPrivacyAccessedAPIType</key>
            <string>NSPrivacyAccessedAPICategoryUserDefaults</string>
            <key>NSPrivacyAccessedAPITypeReasons</key>
            <array>
                <string>CA92.1</string>
            </array>
        </dict>
    </array>
</dict>
```

## Razones requeridas del uso de APIs

Apple solicita la necesidad de que los desarrolladores declaren el uso de APIs específicas. Para más información puedes consultar la documentación de Apple [aquí](https://developer.apple.com/documentation/bundleresources/privacy_manifest_files/describing_use_of_required_reason_api?language=objc).

Para aclarar el uso de las APIs, los desarrolladores de aplicaciones deberán declarar la categoría de API y especificar los motivos de su uso. A continuación se muestran las API de que son utilizadas por el SDK de EMMA en todas sus versiones:

| Categoría de API   | Tipo de API                              | Razón  | Observaciones                                                                                                                         |
|--------------------|------------------------------------------|--------|---------------------------------------------------------------------------------------------------------------------------------------|
| [User defaults APIs](https://developer.apple.com/documentation/bundleresources/privacy_manifest_files/describing_use_of_required_reason_api) | NSPrivacyAccessedAPICategoryUserDefaults | CA92.1 | El SDK utiliza esta API para guardar datos relacionados con la instalación, identificadores del usuario y el ciclo de vida de la app. |

## Datos vinculados al usuario

Los Datos personales que recopila EMMA están vinculados al dispositivo de un usuario.

EMMA puede brindarle la posibilidad de asociar los datos que recopila con un ID de publicidad u otros identificadores de dispositivo. Cualquier asociación que realice con los datos de EMMA a identificadores de usuario únicos, como ID de usuario o ID de cuenta, es exclusiva de su práctica y queda a su propia discreción.

## Fines de uso de los datos recogidos por EMMA

La siguiente tabla muestra los fines del uso de datos recogidos por el SDK de EMMA.
Utilice las siguientes cadenas como valores para la clave NSPrivacyCollectedDataTypePurposes en sus diccionarios NSPrivacyCollectedDataTypes:



| Fines de los datos                       | Descripción                                                                                                                                                                                                                                                                          | ¿EMMA usa este fin?                                                                                                                                                                                               | Valor en el Manifiesto de Privacidad                    |
|------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| Publicidad de terceros                   | Como mostrar anuncios de terceros en su aplicación o compartir datos con entidades que muestran anuncios de terceros.                                                                                                                                                                | Opcional - EMMA puede compartir datos con entidades que muestran anuncios de terceros (Google, Meta etc). Solo si se configure explícitamente en la plataforma, EMMA por defecto no envía ningún dato a terceros. | NSPrivacyCollectedDataTypePurposeThirdPartyAdvertising  |
| Publicidad o marketing del desarrollador | Como mostrar anuncios propios en su aplicación, enviar comunicaciones de marketing directamente a sus usuarios o compartir datos con entidades que mostrarán sus anuncios.                                                                                                           | Opcional - EMMA puede compartir datos con el cliente o incluso con otras entidades para mostrar anuncios propios en la app. EMMA solo realizará esta opción si así está configurada en la plataforma.             | NSPrivacyCollectedDataTypePurposeDeveloperAdvertising   |
| Analíticas                               | Usar datos para evaluar el comportamiento del usuario, incluso para comprender la efectividad de las funciones del producto existente, planificar nuevas funciones o medir el tamaño o las características de la audiencia.                                                          | Sí                                                                                                                                                                                                                | NSPrivacyCollectedDataTypePurposeAnalytics              |
| Personalización del producto             | Personalizar lo que ve el usuario, como una lista de productos recomendados, publicaciones o sugerencias.                                                                                                                                                                            | Opcional - EMMA puede recolectar ciertos datos para personalizar comunicaciones en la app (Pushes, Startview, Native Ad etc).                                                                                     | NSPrivacyCollectedDataTypePurposeProductPersonalization |
| Funcionalidad de la aplicación           | Por ejemplo, para autenticar al usuario, habilitar funciones, prevenir fraudes, implementar medidas de seguridad, garantizar el tiempo de actividad del servidor, minimizar las fallas de las aplicaciones, mejorar la escalabilidad y el rendimiento o brindar atención al cliente. | Sí                                                                                                                                                                                                                | NSPrivacyCollectedDataTypePurposeAppFunctionality       |
| Otros fines                              | Cualquier otro proposito no listado.                                                                                                                                                                                                                                                 | No                                                                                                                                                                                                                | NSPrivacyCollectedDataTypePurposeOther                  |


## Etiquetas nutricionales recogidas por EMMA

**Las etiquetas a añadir dependen de la app del cliente y como esté tratando y proporcinando los datos a EMMA**. La guia realizada es  una orientación de como se deben añadir estas etiquetas en la sección de "Uso de datos" del Manifiesto de Privacidad. Si ya tienes una app en AppleStore ya tienes completado el formulario de Uso de Datos de obligado complimiento, puedes tomar ese formulario como punto de partida para añadir los datos al Manifiesto de Privacidad. Para una correcta integración lo ideal es la colaboración con asesores legales para identificar cuales son las etiquetas que tu app utiliza. Para más información sobre las etiquetas nutricionales consulta la documentación de Apple [aquí](https://developer.apple.com/documentation/bundleresources/privacy_manifest_files/describing_data_use_in_privacy_manifests).

Dentro del Manifiesto de Privacidad utilice las siguientes cadenas como valores para la key NSPrivacyCollectedDataType en los diccionarios de NSPrivacyCollectedDataTypes:

**Información de contacto**

Cualquiera de los datos de esta tabla serían entidades vinculadas (Linked) al usuario. Ninguna se usaría con motivo de Tracking.


| Tipo de datos                | Description                                                 | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad?                             | Valor en el Manifiesto de Privacidad           |
|------------------------------|-------------------------------------------------------------|---------------------------------------|------------------------------------------------------------------------------|------------------------------------------------|
| Nombre                       | Nombre o apellidos.                                         | No                                    | Solo si se envía como etiqueta.                                              | NSPrivacyCollectedDataTypeName                 |
| Email                        | Cuando no se envia el email en formato hash.                | No                                    | Solo si se envía como etiqueta o en los métodos de loginUser o registerUser. | NSPrivacyCollectedDataTypeEmailAddress         |
| Número de teléfono           | Cuando no se envia el teléfono en formato hash.             | No                                    | Solo si se envía como etiqueta.                                              | NSPrivacyCollectedDataTypePhoneNumber          |
| Dirección física             | Dirección de casa, apartado de correo o similares. | No                                    | Solo si se envía como etiqueta.                                              | NSPrivacyCollectedDataTypePhysicalAddress      |
| Otra información de contacto | Cualquier otra información de contacto fuera de la app.     | No                                    | Solo si se envía como etiqueta.                                              | NSPrivacyCollectedDataTypeOtherUserContactInfo |


**Salud y fitness**

Cualquiera de los datos de esta tabla serían entidades vinculadas (Linked) al usuario. Ninguna se usaría con motivo de Tracking.


| Tipo de datos                | Descripción                                                                                                                                                                                                                                                                                              | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad? | Valor en el Manifiesto de Privacidad           |
|------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------|--------------------------------------------------|------------------------------------------------|
| Salud                        | Datos médicos y de salud, incluidos, entre otros, datos de la API de registros médicos clínicos, la API HealthKit, las API de trastornos del movimiento o investigaciones de sujetos humanos relacionadas con la salud o cualquier otro dato médico o de salud proporcionado por cualquier otro usuario. | No                                    | Solo si se envía como etiqueta.                  | NSPrivacyCollectedDataTypeHealth               |
| Fitness                      | Datos de actividad física y ejercicio, incluidos, entre otros, la API Motion and Fitness.                                                                                                                                                                                                                | No                                    | Solo si se envia como etiqueta.                  | NSPrivacyCollectedDataTypeFitness              |

**Información financiera**

Cualquiera de los datos de esta tabla serían entidades vinculadas (Linked) al usuario. Ninguna se usaría con motivo de Tracking.

| Tipo de datos                | Description                                                                                                                                                                                                                                                                                             | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad? | Valor en el Manifiesto de Privacidad           |
|------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------|--------------------------------------------------|------------------------------------------------|
| Información del pago         | Como forma de pago, número de tarjeta de pago o número de cuenta bancaria. Si su aplicación utiliza un servicio de pago, la información de pago se ingresa fuera de su aplicación y usted, como desarrollador, nunca tiene acceso a la información de pago, no se recopila y no es necesario revelarla. | No                                    | Solo si se envia como etiqueta.                  | NSPrivacyCollectedDataTypePaymentInfo          |
| Información de crédito       | Como la puntuación de crédito                                                                                                                                                                                                                                                                           | No                                    | Solo si se envia como etiqueta.                  | NSPrivacyCollectedDataTypeCreditInfo           |
| Otra información financiera  | Como salario, ingresos, activos, deudas o cualquier otra información financiera.                                                                                                                                                                                                                        | No                                    | Solo si se envía como etiqueta.                  | NSPrivacyCollectedDataTypeOtherFinancialInfo   |

**Localización**

Cualquiera de los datos de esta tabla serían entidades vinculadas (Linked) al usuario. Ninguna se usaría con motivo de Tracking.

| Tipo de datos                | Description                                                                                                                                                                              | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad?                            | Valor en el Manifiesto de Privacidad           |
|------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------|-----------------------------------------------------------------------------|------------------------------------------------|
| Localización precisa         | Información que describe la ubicación de un usuario o dispositivo con igual o mayor resolución que una latitud y longitud con tres o más decimales.                                      | No                                    | Solo si se usa el método del SDK .trackLocation()                           | NSPrivacyCollectedDataTypePreciseLocation      |
| Localización aproximada      | Información que describe la ubicación de un usuario o dispositivo con una resolución inferior a una latitud y longitud con tres o más decimales, como servicios de ubicación aproximada. | No                                    | No | NSPrivacyCollectedDataTypeCoarseLocation       |

**Información sensible**

Cualquiera de los datos de esta tabla serían entidades vinculadas (Linked) al usuario. Ninguna se usaría con motivo de Tracking.

| Tipo de datos                | Description                                                                                                                                                                                                               | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad?                            | Valor en el Manifiesto de Privacidad           |
|------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------|-----------------------------------------------------------------------------|------------------------------------------------|
| Información sensible         | Como datos raciales o étnicos, orientación sexual, información sobre embarazo o parto, discapacidad, creencias religiosas o filosóficas, afiliación sindical, opinión política, información genética o datos biométricos. | No                                    | Solo si se envía como etiqueta.                                             | NSPrivacyCollectedDataTypeSensitiveInfo        |

**Contactos**

| Tipo de datos                | Description                                                                                                                                                                              | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad?                            | Valor en el Manifiesto de Privacidad           |
|------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------|-----------------------------------------------------------------------------|------------------------------------------------|
| Contactos                    | Como una lista de contactos en el teléfono, la agenda de direcciones o el gráfico social del usuario.                                                                                    | No                                    | No                                             | NSPrivacyCollectedDataTypeContacts             |

**Contenido del usuario**

Cualquiera de los datos de esta tabla serían entidades vinculadas (Linked) al usuario. Ninguna se usaría con motivo de Tracking.

| Tipo de datos              | Descripción                                                                                                           | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad? | Valor en el Manifiesto de Privacidad           |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------|---------------------------------------|--------------------------------------------------|------------------------------------------------|
| Emails o mensajes de texto | Incluyendo línea de asunto, remitente, destinatarios y contenido del correo electrónico o mensaje.                    | No                                    | Solo si se envía como etiqueta.                  | NSPrivacyCollectedDataTypeEmailOrTextMessages  |
| Fotos videos               | Fotos o videos del usuario.                                                                                           | No                                    | No                                               | NSPrivacyCollectedDataTypePhotosorVideos       |
| Datos de audio             | Grabaciones de voz del usuario o sonidos.                                                                             | No                                    | No                                               | NSPrivacyCollectedDataTypeAudioData            |
| Contenido de juegos        | Como juegos guardados, coincidencias multijugador o lógica de juego, o contenido generado por el usuario en el juego. | No                                    | Solo si se envía como etiqueta.                  | NSPrivacyCollectedDataTypeGameplayContent      |
| Atención al cliente        | Datos generados por el usuario durante una solicitud de atención al cliente.                                          | No                                    | Solo si se envía como etiqueta.                  | NSPrivacyCollectedDataTypeOtherUserContactInfo |
| Otro contenido del usuario | Cualquier contenido generado del usuario.                                                                             | No                                    | Solo si se envía como etiqueta.                  | NSPrivacyCollectedDataTypeOtherUserContent     |

**Historial de navegación**

| Tipo de datos              | Descripción                                                                                                           | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad? | Valor en el Manifiesto de Privacidad           |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------|---------------------------------------|--------------------------------------------------|------------------------------------------------|
| Historial de navegación    | Información sobre el contenido que el usuario ha visto y que no forma parte de la aplicación, como sitios web.        | No                                    | No                                               | NSPrivacyCollectedDataTypeBrowsingHistory      |

**Historial de búsqueda**

| Tipo de datos              | Descripción                                                                                                           | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad? | Valor en el Manifiesto de Privacidad           |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------|---------------------------------------|--------------------------------------------------|------------------------------------------------|
| Historial de búsqueda      | Información sobre búsquedas realizadas en la aplicación.                                                              | No                                    | No                                               | NSPrivacyCollectedDataTypeSearchHistory        |

**Identificadores**

- El User ID sería una entidad vinculada (Linked) al usuario. No se usaría con motivo de Tracking.
- El Device ID sería una entidad vinculada (Linked) al usuario y se usaría como Tracking solo si así se configura en la plataforma activando redes de atribución. 



| Tipo de datos              | Descripción                                                                                                                                                                                                 | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad?                                                                                                                                   | Valor en el Manifiesto de Privacidad           |
|----------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------|
| User ID                    | Nombre de usuario, identificador, ID de cuenta, ID de usuario asignado, número de cliente u otro ID a nivel de usuario o cuenta que se puede utilizar para identificar a un usuario o cuenta en particular. | No                                    | Solo si se envía como customerId en los métodos loginUser, registerUser o setCustomerId.                                                                                           | NSPrivacyCollectedDataTypeUserID               |
| Device ID                  | Como el identificador de publicidad del dispositivo u otra identificación a nivel de dispositivo.                                                                                                           | Sí                                    | Sí, EMMA genera un identificador de dispositivo propio y recoge los identificadores de dispositivo IDFV e IDFA, en el caso de este último siempre que la app o el usuario haya dado permiso para ello. | NSPrivacyCollectedDataTypeDeviceID             |


**Historial de compras**

Cualquiera de los datos de esta tabla serían entidades vinculadas (Linked) al usuario. Ninguna se usaría con motivo de Tracking.

| Tipo de datos              | Descripción                                                                                                           | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad?                                                                                                                                   | Valor en el Manifiesto de Privacidad           |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------|---------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------|
| Historial de compras       | Las compras o tendencias de compra de una cuenta o individuo.                                                         | No                                    | Solo si se envía la compra desde la app con el startOrder, addProduct y el trackOrder.                                                                                             | NSPrivacyCollectedDataTypePurchaseHistory      |



**Uso de datos**

Cualquiera de los datos de esta tabla serían entidades vinculadas (Linked) al usuario. Ninguna se usaría con motivo de Tracking.

| Tipo de datos              | Descripción                                                                                                                                                                                                                                            | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad?                                                                                                                   | Valor en el Manifiesto de Privacidad           |
|----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------|
| Interacción con productos  | Como inicios de aplicaciones, toques, clics, información de desplazamiento, datos de escucha de música, visualizaciones de videos, lugar guardado en un juego, video o canción, u otra información sobre cómo el usuario interactúa con la aplicación. | Sí                                    | Sí, EMMA contabiliza las sesiones y el número de veces que el usuario interactua con una comunicación en el caso de usar la funcionalidad.                         | NSPrivacyCollectedDataTypeProductInteraction   |
| Datos publicitarios        | Como información sobre los anuncios que ha visto el usuario.                                                                                                                                                                                           | No                                    | No, EMMA no recoge datos de otros Advertisers                                                                                                                          | NSPrivacyCollectedDataTypeAdvertisingData      |
| Otros usos de datos        | Cualquier otro tipo de actividad del usuario en la app.                                                                                                                                                                                                | No                                    | Solo si envias algún otro evento que puede estar realizacionado con la actividad del usuario en la app. Como loginUser, registerUser o cualquier evento personalizado. | NSPrivacyCollectedDataTypeOtherUsageData       |

**Diagnóstico**

Cualquiera de los datos de esta tabla serían entidades vinculadas (Linked) al usuario. Ninguna se usaría con motivo de Tracking.

| Tipo de datos              | Descripción                                                                                                           | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad? | Valor en el Manifiesto de Privacidad           |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------|---------------------------------------|--------------------------------------------------|------------------------------------------------|
| Datos de crashes           | Crash logs                                                                                                            | No                                    | No                                               | NSPrivacyCollectedDataTypeCrashData            |
| Datos de rendimiento       | Como el tiempo de inicio, la tasa de bloqueos o el uso de energía.                                                    | No                                    | Solo si se envía como una etiqueta.              | NSPrivacyCollectedDataTypePerformanceData      |
| Otros datos de diagnóstico | Cualquier otro dato recopilado con el fin de medir diagnósticos técnicos relacionados con la aplicación.              | No                                    | Solo si se envía como una etiqueta.              | NSPrivacyCollectedDataTypeOtherDiagnosticData  |

**Entorno**

| Tipo de datos              | Descripción                                                                                                           | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad? | Valor en el Manifiesto de Privacidad           |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------|---------------------------------------|--------------------------------------------------|------------------------------------------------|
| Escaneo de entorno         | Como malla, planos, clasificación de escenas y/o detección de imágenes del entorno del usuario.                       | No                                    | No                                               | NSPrivacyCollectedDataTypeEnvironmentScanning  |

**Cuerpo**

| Tipo de datos              | Descripción                                                                                                           | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad? | Valor en el Manifiesto de Privacidad           |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------|---------------------------------------|--------------------------------------------------|------------------------------------------------|
| Manos                      | La estructura de la mano del usuario y los movimientos de la mano.                                                    | No                                    | No                                               | NSPrivacyCollectedDataTypeHands                |
| Cabeza                     | El movimiento de la cabeza del usuario.                                                                               | No                                    | No                                               | NSPrivacyCollectedDataTypeHead                 |

**Otro tipo de datos**

Estos datos NO estarían vinculados (Linked) al usuario y no se usuarían como Tracking.


| Tipo de datos              | Descripción                                                                                                           | ¿Recoge EMMA estos datos por defecto? | ¿Debes incluirlo en el Manifiesto de Privacidad?                         | Valor en el Manifiesto de Privacidad           |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------|---------------------------------------|--------------------------------------------------------------------------|------------------------------------------------|
| Otro tipo de datos         | Cualquier otro tipo de datos                                                                                          | Sí                                    | Sí, EMMA recolecta otros datos como el modelo del dispositivo, marca del dispositivo, tipo de conexión a internet y timezone. | NSPrivacyCollectedDataTypeOtherDataTypes       |
