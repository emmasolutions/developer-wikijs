---
title: Guía de integración rápida
description: Guía de integración rápida para el SDK de EMMA en la plataforma iOS
published: true
date: 2025-02-03T11:22:55.339Z
tags: ios, sdk, quick-start, quick-wins
editor: markdown
dateCreated: 2024-05-27T11:19:53.403Z
---

# Referencias

## Documentación

- [📖 Integración completa *Detalle para integrar el SDK de EMMA en la plataforma iOS*](https://developer.emma.io/es/ios/integracion-sdk)
{.links-list}

## Repositorios de ejemplo

- [🪽 Swift - SwiftUI *App de ejemplo en GitHub*](https://github.com/EMMADevelopment/emma-ios-app)
{.links-list}


# Descarga de EMMA

## Swift Package Manager (SPM)

- **Descarga y Configuración:**
    - Abrir Xcode y seleccionar `File > Add Package Dependencies...`.
    - Añadir la URL del SDK: [https://github.com/EMMADevelopment/eMMa-iOS-SDK](https://github.com/EMMADevelopment/eMMa-iOS-SDK)
    - Seleccionar la versión 4.12.0+ como versión mínima, por ejemplo.
    
> La versión más reciente del SDK es la **4.15.1**. Consulta esta [página](https://developer.emma.io/es/ios/sdk-upgrades) para detalles sobre las actualizaciones.
{.is-info}

## CocoaPods

- **Instalación:**
    - Instala CocoaPods usando `gem install cocoapods`.
    - Crea un archivo llamado `Podfile` en tu proyecto Xcode.
    - Añade la línea `pod 'eMMa'` al `Podfile`.
    - Ejecuta `pod install` en el directorio del proyecto Xcode.
- **Configuración:**
    - CocoaPods descargará e instalará la librería de EMMA creando un nuevo archivo `.xcworkspace`.
    - Abrir este archivo en Xcode después de la instalación.

## Manual

- **Descarga y configuración:**
    - Descarga la última versión de `EMMA_iOS.xcframework` en el siguiente [enlace](https://github.com/EMMADevelopment/eMMa-iOS-SDK).
    - Añade `EMMA_iOS.xcframework` a tu proyecto de Xcode en la sección `Frameworks, Libraries, and Embedded Content`.
    - Selecciona `Embed and Sign`.

# Integración Básica

## Inicialización

- Asegúrate de obtener el Session Key de EMMA ([EMMA Key](https://docs.emma.io/es/configuracion#general-emma-key-y-api-key)).
- Importa el módulo de EMMA: `import EMMA_iOS`.
- Inicializa la librería de EMMA en `application(_:didFinishLaunchingWithOptions:)`.

```swift
import UIKit
import EMMA_iOS

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let configuration = EMMAConfiguration()
        configuration.debugEnabled = true
        configuration.sessionKey = "MY_EMMA_SESSION_KEY"

        EMMA.startSession(with: configuration)
        return true
    }
}
```

## Desactivar Envío de Pantallas

- Para desactivar el envío de pantallas, configura `trackScreenEvents` en `false` en la configuración. Por defecto es `true`.

```swift
let configuration = EMMAConfiguration()
configuration.debugEnabled = true
configuration.sessionKey = "MY_EMMA_SESSION_KEY"
configuration.trackScreenEvents = false // disable screens
```

# Integración de Powlink

Para el correcto funcionamiento del powlink, EMMA debe conocer todos los links que se abren en la aplicación de forma externa, ya sea desde una URL externa, o desde un deeplink implementado en la aplicación.
    
Para ello debes implementar el método `application(_:continue:restorationHandler:)` en tu `AppDelegate` y ejecutar el método `EMMA.handleLink(url: URL)`.

Procesando los *paths* de los Powlinks recibidos podrás lanzar distintas partes de tu aplicación.
    
```swift
func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
     if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
         if let url = userActivity.webpageURL {
             EMMA.handleLink(url: url)
         }
     }
     return true
}
```

# Notificaciones push

Una vez generados los certificados de tu app, ya puedes empezar a integrar las Notificaciones Push. A continuación, tenemos un ejemplo de un `AppDelegate` completo con la integración del push:

```swift
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, EMMAPushDelegate {
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
         ...
      //Enable EMMA Push System
      EMMA.startPushSystem()
      EMMA.setPushSystemDelegate(self)
  }

  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      EMMA.registerToken(deviceToken)
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
      if #available(iOS 14.0, *) {
          completionHandler([.badge, .sound, .banner, .list])
      } else {
          completionHandler([.badge, .sound, .alert])
      }
  }

  @available(iOS 10.0, *)
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
      EMMA.handlePush(response.notification.request.content.userInfo)
      completionHandler()
    }
}
```

# Mensajes In-App

## NativeAd

```swift
import UIKit
import EMMA_iOS

class NativeAdExampleViewController: UIViewController, EMMAInAppMessageDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func getNativeAd(templateId: String) {
        let nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        EMMA.inAppMessage(nativeAdRequest, with: self)
    }

    func getBatchNativeAd(templateId: String) {
        let nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        nativeAdRequest.isBatch = true
        EMMA.inAppMessage(nativeAdRequest, with: self)
    }

    // MARK: - InAppMessage Delegate
    func onReceived(_ nativeAd: EMMANativeAd!) {
        let content = nativeAd.nativeAdContent as? [String:AnyObject]
        if let title = content?["Title"] as? String) {
            print("Received NativeAd with Title: \(title)")
            // Draw Native Ad and Send Impression
            EMMA.sendImpression(.campaignNativeAd, withId: String(nativeAd.idPromo))
        }
    }

    func onBatchNativeAdReceived(_ nativeAds: [EMMANativeAd]!) {
        nativeAds.forEach { (nativeAd) in
            if let tag = nativeAd.tag {
                print("Received batch nativead with tag: \(tag)")
            }
        }
    }

    func openNativeAd(nativeAd: EMMANativeAd) {
        // This method executes CTA Action and sends NativeAd Click
        EMMA.openNativeAd(String(nativeAd.idPromo))
    }

    func sendNativeAdClick(nativeAd: EMMANativeAd) {
        // Send manual click. Useful if we want to override CTA action
        EMMA.sendClick(.campaignNativeAd, withId: String(nativeAd.idPromo))
    }

    func onShown(_ campaign: EMMACampaign!) {

    }

    func onHide(_ campaign: EMMACampaign!) {

    }

    func onClose(_ campaign: EMMACampaign!) {

    }
}
```

`EMMANativeAd` contiene todos los campos configurados en EMMA para esta plantilla de NativeAd, para obtenerlos se usará el siguiente método:

```swift
let content = nativeAd.nativeAdContent as? [String:AnyObject]
let title = content?["Title"] as? String
let image = content?["Main picture"] as? String
let cta = content?["CTA"] as? String
printSide(title, image, cta)
```

Una vez obtenidos todos los campos requeridos, ya se puede crear la vista para pintar este NativeAd en pantalla en función del diseño que se le quiera aplicar. Una vez pintado el NativeAd en pantalla, es necesario llamar a este método para poder obtener las impresiones en el reporting:

```swift
EMMA.sendImpression(.campaignNativeAd, withId: String(nativeAd.idPromo))
```

## StartView

El StartView configurado se inyecta en la vista al realizar esta llamada.

```swift
func getStartView() {
    let startViewinAppRequest = EMMAInAppRequest(type: .Startview)
    EMMA.inAppMessage(startViewinAppRequest)
}
```

## AdBall

El AdBall configurado se inyecta en la vista al realizar esta llamada.

```swift
func getAdBall() {
    let adballRequest = EMMAInAppRequest(type: .Adball)
    EMMA.inAppMessage(adballRequest)
}
```

## Banner

El Banner configurado se inyecta en la vista al realizar esta llamada.

```swift
func getBanner() {
    let bannerRequest = EMMAInAppRequest(type: .Banner)
    EMMA.inAppMessage(bannerRequest)
}
```

## Strip

El Strip configurado se inyecta en la vista al realizar esta llamada.

```swift
func getStrip() {
    let stripRequest = EMMAInAppRequest(type: .Strip)
    EMMA.inAppMessage(stripRequest)
}
```

## Coupon

```swift
import UIKit
import EMMA_iOS

class CouponExampleViewController: UIViewController, EMMACouponDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Add coupon delegate
        EMMA.add(self)
    }

    func getAllCoupons() {
        let couponsRequest = EMMAInAppRequest(type: .Coupons)
        EMMA.inAppMessage(couponsRequest)
    }

    func sentCouponClick(_ coupon: EMMACoupon) {
        EMMA.sendClick(.campaignCoupon, withId: String(coupon.couponId))
    }

    func getSingleCoupon() {
        let couponsRequest = EMMAInAppRequest(type: .Coupons)
        couponsRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(couponsRequest)
    }

    func checkCouponRedeems() {
        let couponsRequest = EMMAInAppRequest(type: .CouponValidRedeems)
        // You must pass coupon id to check
        couponsRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(couponsRequest)
    }

    func redeemCoupon() {
        let redeemCouponRequest = EMMAInAppRequest(type: .RedeemCoupon)
        redeemCouponRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(redeemCouponRequest)
    }

    func cancelCoupon() {
        let cancelCouponRequest = EMMAInAppRequest(type: .CancelCoupon)
        cancelCouponRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(cancelCouponRequest)
    }

    // MARK: - EMMA Coupon Delegate methods
    func onCouponsReceived(_ coupons: [EMMACoupon]!) {
        guard let receivedCoupons = coupons else {
            print("Error retrieving coupons")
            return
        }
        // Now we can send coupon impressions
        receivedCoupons.forEach { (coupon) in
            EMMA.sendImpression(.campaignCoupon, withId: String(coupon.couponId))
        }
        print("Received coupons \(receivedCoupons)")
    }

    func onCouponsFailure() {
        print("Error retrieving coupons")
    }

    func onCouponValidRedeemsReceived(_ validRedeems: Int) {
        if validRedeems > 0 {
            print("Coupon have valid redeems pending \(validRedeems)")
        }
    }
}
```

## DynamicTab

```swift
func getDynamicTabBar() {
    // You must define your UITabBarController
    // Uncomment following line!
    // EMMA.setPromoTabBarController(UITabBarController!)

    // Sets default promo tab index if not defined in EMMA Platform
    EMMA.setPromoTabBarIndex(5)

    // Sets a tab bar item to be shown if not defined in EMMA Platform
    // EMMA.setPromoTabBarItem(UITabBarItem!)

    let dynamicTabBarRequest = EMMAInAppRequest(type: .PromoTab)
    EMMA.inAppMessage(dynamicTabBarRequest)
}
```

## Plugins personalizados

La clase principal del nuevo formato debe extender de la clase abstracta `EMMAInAppPlugin`.

```swift
import EMMA_iOS

public class CustomInAppPlugin: EMMAInAppPlugin {

    public func getId() -> String {
        return "emma-plugin-custom"
    }

    public func show(_ nativeAd: EMMANativeAd) {
       // Process data
    }

    public func dismiss() {
    }
}
```

Añade el plugin en el SDK después del inicio de sesión con `addInAppPlugin`.

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
     ....
     EMMA.startSession(with: configuration)
     EMMA.addInAppPlugin([CustomInAppPlugin()])
     return true
}
```

Llama al plugin en la parte de la app que quieras como si de un NativeAd se tratase.

```swift
let nativeAdRequest = EMMANativeAdRequest()
nativeAdRequest.templateId = "emma-plugin-custom"
EMMA.inAppMessage(request: nativeAdRequest)
```

# Eventos

## Registro

```swift
func register() {
  EMMA.registerUser("554234", forMail: "test@emma.io")
}
```

## Login

```swift
func login() {
    EMMA.loginUser("554234", forMail: "test@emma.io")
}
```

## Evento personalizado

Usa `EMMA.trackEvent(request:)` para contar el número de veces que ciertos eventos suceden durante una sesión en tu app.

Esto puede ser útil para medir cuántas veces los usuarios convierten en diferentes acciones, por ejemplo.

Puedes obtener los tokens de eventos creándolos en la plataforma de EMMA. Si un token no existente es enviado a EMMA, se devolverá un error.

Para más información sobre eventos, consulta la documentación [aquí](https://docs.emma.io/es/primeros-pasos/eventos).

```swift
let eventRequest = EMMAEventRequest.init(token: "<token>")
// Optional: You can add your custom event attributes
eventRequest.attributes = ["test_attribute":"test_value"]
// Optional. You can capture emma requests with this delegate
eventRequest.requestDelegate = self
// Optional. Append your request ID to capture it later
eventRequest.customId = "MY_EVENT_REQUEST"

EMMA.trackEvent(request: eventRequest)
```

## Evento de compra

Consta de los métodos `startOrder`, `addProduct`, `trackOrder` y `cancelOrder`.

```swift
func trackTransaction() {
    EMMA.startOrder("<ORDER_ID>", customerId: "<CUSTOMER_ID>", totalPrice: 10.0, coupon: "")
    EMMA.addProduct("<PRODUCT_ID>", name: "<PRODUCT_NAME>", qty: 1.0, price: 10.0)
    EMMA.trackOrder()
    EMMA.cancelOrder("<ORDER_ID>")
}
```

# Envío de etiquetas de usuario

El método `EMMA.trackExtraUserInfo(info:)` actualiza o añade parámetros extra de cara a obtener mejor segmentación en el filtrado `Users with tag`. Puede ser usado en el registro, en el login o en cualquier otra sección de la app donde se recoja información del usuario.

```swift
func trackUserInfo() {
    //In this example we set the tag "AGE" of our user to value "40"
    EMMA.trackExtraUserInfo(["AGE" : "40"])
}
```

# Seguimiento publicitario con IDFA

## Habilitar

Para cumplir con los requisitos de iOS 14 y solicitar el permiso de rastreo publicitario, implementa el siguiente método en tu app:

```swift
if #available(iOS 14.0, *) {
    EMMA.requestTrackingWithIdfa()
}
```

> Asegúrate de incluir la clave `NSUserTrackingUsageDescription` en el archivo `Info.plist`. Consulta la [documentación oficial de Apple](https://developer.apple.com/documentation/apptrackingtransparency) para más detalles.
{.is-info}

## Deshabilitar

Para cumplir con la normativa RGPD y permitir a los usuarios desactivar el seguimiento, usa el siguiente método:

```swift
public func disableUserTracking(deleteUserData: Bool) {
    EMMA.disableUserTracking(deleteUserData)
}
```

> Si `deleteUserData` es `true`, los datos del usuario se eliminarán de los servidores de EMMA de forma irreversible.
{.is-warning}

## Rehabilitar

Si el usuario cambia de opinión y desea habilitar nuevamente el seguimiento, implementa:

```swift
public func enableUserTracking() {
    EMMA.enableUserTracking()
}
```
