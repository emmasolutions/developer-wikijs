---
title: Obtener Push Auth Key
description: 
published: true
date: 2020-11-11T16:59:50.933Z
tags: ios, sdk, apple, push
editor: markdown
dateCreated: 2020-11-11T16:55:26.280Z
---

# Obtener y configurar Push Auth Key en Apple

Para obtener el certificado hay que visitar la cuenta de [Apple Developers](https://developer.apple.com/account/)

Le damos click en la sección de Certificados, IDs y Perfiles.

![captura_de_pantalla_2018-11-12_a_las_0_02_25.png](/captura_de_pantalla_2018-11-12_a_las_0_02_25.png)

Una vez en Apple le damos click en la sección Keys y añadimos una key.

![captura_de_pantalla_2018-11-12_a_las_0_02_52.png](/captura_de_pantalla_2018-11-12_a_las_0_02_52.png)

Añadimos el nombre, seleccionamos el check de APNs y le damos a continuar.

![captura_de_pantalla_2018-11-12_a_las_0_13_18.png](/captura_de_pantalla_2018-11-12_a_las_0_13_18.png)

Confirmamos y descargamos el archivo .p8 que genera.

![captura_de_pantalla_2018-11-12_a_las_0_05_43.png](/captura_de_pantalla_2018-11-12_a_las_0_05_43.png)

![captura_de_pantalla_2018-11-12_a_las_0_13_56.png](/captura_de_pantalla_2018-11-12_a_las_0_13_56.png)

# Configurar Push Auth Key en EMMA

Añadimos el archivo(.p8), el team ID y el bundle ID en EMMA -> Preferencias App

![captura_de_pantalla_2020-11-10_a_las_10.32.39.png](/captura_de_pantalla_2020-11-10_a_las_10.32.39.png)

A la hora de añadir el certificado es necesario introducir el team ID para hacer la validación con Apple del mismo. Añadiremos el bundle ID y el team ID para utilizarse en el envío de las notificaciones.

Para obtener el bundle ID y team ID se puede hacer de la siguiente forma. El ID es equivalente al bundle ID y el prefix es equivalente al team ID.

![captura_de_pantalla_2018-11-12_a_las_10_30_59.png](/captura_de_pantalla_2018-11-12_a_las_10_30_59.png)

