---
title: Integración SDK EMMA iOS
description: 
published: true
date: 2025-02-20T10:24:15.340Z
tags: ios, sdk
editor: markdown
dateCreated: 2020-11-11T15:15:57.155Z
---

# Descarga e integración básica

## Descarga EMMA iOS SDK

### Swift Package Manager (SPM)
Puedes añadir el SDK de EMMA con SPM a partir de la versión `4.9.0+` y requiere de Xcode 12 o superior.

Pasos de ejemplo para la integración:
1. Abrir Xcode y seleccionar `File > Add Package Dependencies...`
2. Añadir la URL del SDK `https://github.com/EMMADevelopment/eMMa-iOS-SDK`.
3. En la configuración de versiones:
	 - Selecciona la opción "Up to Next Major Version".
   - Especifica la versión mínima como `4.12.0`. Esto garantiza que siempre utilices las versiones más recientes dentro del rango `4.x`.
4. Verifica que el SDK se ha integrado correctamente en tu proyecto.

> La versión más reciente del SDK es la **4.15.2**. Consulta esta [página](https://developer.emma.io/es/ios/sdk-upgrades) para detalles sobre las actualizaciones.
{.is-info}

### Cocoapods

La última versión de EMMA está disponible a través de CocoaPods

1. Instala CocoaPods usando `gem install cocoapods`.
2. Si es la primera vez que usas CocoaPods, puedes opcionalmente ejecutar `pod setup` para bajarte el repositorio con todos los “specs" y consultarlos en local, esto directamente creará una copia del repositorio de GitHub.
3. Crea un archivo en tu proyecto Xcode llamado `Podfile`.
4. A continuación añade la linea: `pod 'eMMa'` 
5. Finalmente, ejecuta en el directorio del proyecto Xcode lo siguiente: `pod install`.

Una vez hecho, CocoaPods descargará e instalará la librería de EMMA creando un nuevo archivo `.xcworkspace`. Por último, abrir este archivo en Xcode.

### Descarga manual

Para realizar la descarga manual sigue los siguientes pasos:

1. Descarga la última versión de `EMMA_iOS.xcframework` en el siguiente [enlace](https://github.com/EMMADevelopment/eMMa-iOS-SDK).
2. Añade `EMMA_iOS.xcframework` a tu proyecto de Xcode en la sección `Frameworks, Libraries,and Embedded Content`.
3. Selecciona `Embed and Sign`.
4. Si usas una extensión para el servicio de notificaciones push añade `EMMA_iOS.xcframework` en `Framework and Libraries` y selecciona `Do Not Embed`.
5. Ejecuta el proyecto.

## Integración básica

***NOTA: Este es el requerimiento mínimo para comenzar a medir instalaciones de tu App.***

### Dependencias
- **Obtener el `Session Key` de EMMA**.
	Consulta la documentación para ver donde puedes encontrar tu `EMMA Key` en la [página de documentación de EMMA](https://docs.emma.io/es/configuracion#general-emma-key-y-api-key).

### Inicializar la librería

El lugar donde tiene más sentido inicializar la librería de EMMA es en el método  [application(_:didFinishLaunchingWithOptions:)](https://developer.apple.com/documentation/uikit/uiapplicationdelegate/application(_:didfinishlaunchingwithoptions:)).

Primero deberemos importar el módulo de EMMA `import EMMA_iOS` y llamar al método `EMMA.startSession(with:)` con los datos de tu `Session Key`.

Ejemplo de integración básica:
#### Tabs {.tabset}
##### Swift
```swift
import UIKit
// Import EMMA SDK
import EMMA_iOS

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let configuration = EMMAConfiguration()
        configuration.debugEnabled = true
        configuration.sessionKey = "MY_EMMA_SESSION_KEY"
        
        EMMA.startSession(with: configuration)
        return true
    }
}
```
##### SwiftUI
```swift
import SwiftUI
import EMMA_iOS

class AppDelegate: NSObject, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        // EMMA uses window to display some InApp Messages
        self.window = UIWindow(frame:UIScreen.main.bounds)
        
        let configuration = EMMAConfiguration()
        configuration.sessionKey = "MY_EMMA_SESSION_KEY"
        EMMA.startSession(with: configuration)
        return true
    }
}

@main
struct EMMASwiftUiApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
```
##### Objective-C
```objc
#import "AppDelegate.h"
// Include EMMA SDK Header
#import "EMMA_iOS/EMMA_iOS.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    EMMAConfiguration *configuration = [EMMAConfiguration new];
    configuration.sessionKey = @"MY_EMMA_SESSION_KEY";
    configuration.debugEnabled = true;
    
    [EMMALegacy startSessionWithConfiguration:configuration];
    return YES;
}

@end
```

### Desactivar envío de pantallas

El envío de pantallas está activo por defecto en el SDK de EMMA. Para desactivarlo usa la siguiente configuración:

#### Tabs {.tabset}
##### Swift
```swift
import UIKit
// Import EMMA SDK
import EMMA_iOS

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let configuration = EMMAConfiguration()
        configuration.debugEnabled = true
        configuration.sessionKey = "MY_EMMA_SESSION_KEY"
        configuration.trackScreenEvents = false // disable screens
        
        EMMA.startSession(with: configuration)
        return true
    }
}
```
##### Objective-C
```objc
#import "AppDelegate.h"
// Include EMMA SDK Header
#import "EMMA_iOS/EMMA_iOS.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    EMMAConfiguration *configuration = [EMMAConfiguration new];
    configuration.sessionKey = @"MY_EMMA_SESSION_KEY";
    configuration.debugEnabled = true;
    configuration.trackScreenEvents = false; // disable screens
    
    [EMMALegacy startSessionWithConfiguration:configuration];
    return YES;
}

@end
```
## Actualizaciones de versiones anteriores

Si se está actualizando de una versión anterior del SDK, consulta antes esta [página](/es/ios/sdk-upgrades) para ver posibles cambios de implementación.

# Integración Acquisition

Documentación para integrar la capacidad de gestionar las campañas de captación:

## Integración de SKAdNetwork 4.0

SKAdNetwork (SKAN) es una herramienta lanzada por Apple que permite a los anunciantes medir el rendimiento de la campaña sin vulnerar la privacidad del usuario. Esto quiere decir que no se utilizan identificadores con IDFA o IDFV para hacer un seguimiento del usuario, tampoco requiere de consentimiento del usuario en el ATT.

> Para implementar esta funcionalidad es necesario integrar la versión del SDK 4.12.0 o superior.
{.is-warning}

Desde iOS 15 Apple permite enviar estas copias de *postbacks*, no solo a los anunciantes como venía haciendo hasta ahora, sino a cualquier *endpoint* añadido previamente en la configuración de la app.

![skadnetwork_graph.png](/ios/skadnetwork_graph.png)

**¿Cómo funciona el SKAdNetwork?** Primero, el anunciante muestra un anuncio de una app (*Advertiser app*) en su propia aplicación o en una aplicación de terceros (*Publisher app*). El usuario visualiza el anuncio, le da clic y se descarga la app anunciada en la AppStore. Seguidamente, el usuario abre la app, el SDK de EMMA notifica a SKAdNetwork la apertura de la app en el método `EMMA.startSession(with:)` y Apple envia una copia del *postback* a nuestro dominio añadido en el `info.plist`.

Para SKAdNetwork 3.0 e inferiores, el *postback* se envía después de la ventana de atribución, que es de 0 a 24 horas. No es posible saber con exactitud cuándo se envía ya que Apple lo hace de forma aleatoria en las 24 o 48 horas sucesivas a que termine la ventana de 24 horas.

Para SKAdNetwork 4.0, se envían hasta 3 *postbacks* después de cada ventana de atribución. El envío se realiza 24h-48h después en el primer *postback* y 24h-144h para el segundo y tercer *postback*. En este caso, tampoco es posible predecir el tiempo de envío ya que lo hace de forma aleatoria dentro de las ventanas. En la siguiente imagen se puede ver la ventana de cada *postback* y su correspondiente retraso.

![skad4_windows.png](/ios/skad4_windows.png)

Para registrar el dominio de EMMA en el `info.plist` de la app sigue los siguientes pasos:

1. Añade la key `NSAdvertisingAttributionReportEndpoint` al `info.plist` de la app (para más detalles visita la [documentación de Apple](https://developer.apple.com/documentation/bundleresources/information_property_list/nsadvertisingattributionreportendpoint)).

2. Añade como valor de la key `https://emma-skadnetwork.com`. Es importante que el valor tenga exactamente este formato.


Una vez añadido el dominio al `info.plist` hay que asegurarse que la app contenga el `StoreKit.framework`. El `StoreKit.framework` es el framework que se encarga de la comunicación con SKAdNetwork a partir de iOS 14+. Para asegurarse del correcto funcionamiento de la app añade `StoreKit.framework` a la app como Opcional.

**¿Cómo puedo probar la integración?** Es cierto que SKAdNetwork no puede probarse de forma real en entornos de prueba o pre-producción (Testflight), pero sí que el SDK de EMMA nos puede dar en estos entornos pistas de su correcta implementación. Para asegurarnos que esta funcionalidad está bien integrada, sigue los siguiente pasos:

1. Lanza la app en un entorno de pruebas con el SDK de EMMA 4.12.0 o superior.
2. Asegurate que, después de ejecutar el método `startSession` en el `AppDelegate`, visualizas el siguiente log: `EMMALogger: 4 - Registering app for SKAdNetwork`.
3. Después de este log verás el resultado del registro de SKAdNetwork:`SKAdNetwork: Error while updating conversion value: Error Domain=SKANErrorDomain Code=10 "(null)"`. Este error es normal en entornos de prueba al no funcionar el SKAdNetwork, pero nos da una pista de que la integración es correcta y el `StoreKit.framework` está funcionando. Cualquier otro error o log de que la API de SKAdNetwork no está disponible es síntoma de un problema de integración con `StoreKit.framework`.

## Integración del POWLINK

### Dependencias
- **Configurar sub-dominio para el powlink**.

	Powlink para iOS utiliza la tecnología de Apple para Universal Links. Para poder tener aperturas directas de la aplicación mediante universal links, deberemos configurar un dominio o subdominio único. Consulta la [guía de soporte para configurar Powlink](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu) en el dashboard de EMMA.

- **Añadir soporte para Universal Links a tu aplicación**.
	Consulta la [guía de Apple para añadir los Universal Links](https://developer.apple.com/documentation/xcode/allowing_apps_and_websites_to_link_to_your_content?language=objc) a la aplicación .

### Integrar EMMA Handle Link

Para el correcto funcionamiento del powlink, EMMA debe conocer todos los links que se abren en la aplicación de forma externa, ya sea desde una URL externa, o desde un deeplink implementado en la aplicación.

Para ello debes implementar el método [application(_:continue:restorationHandler:)](https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623072-application) en tu `AppDelegate` y ejecutar el método `EMMA.handleLink(url: URL)`. 

Procesando los paths de los POWLINKS recibidos podrás lanzar distintas partes de tu aplicación.

Ejemplo de implementación del método `EMMA.handleLink(url: URL)`:
#### Tabs {.tabset}
##### Swift
```swift
   func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                EMMA.handleLink(url: url)
            }
        }
        return true
   }
```
##### Objective-C
```objc
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler {
    if (userActivity.activityType == NSUserActivityTypeBrowsingWeb) {
        [EMMALegacy handleLink:userActivity.webpageURL];
    }
    return YES;
}
```

## POWLINK con dominio propio

Si estás usando un tracker con un dominio que no es el de EMMA (.powlink.io o .pwlnk.io), es necesario añadir el dominio al iniciar la librería para que se reconozca este dominio como un dominio capaz de atribuir campañas.

```swift
        /*
         You can configure custom powlink domains. It can be full domains,
         or short link domains
         EMMA will treat this domains as their own
         */
        configuration.customPowlinkDomains = ["mycustomdomain.com"]
        configuration.shortPowlinkDomains = ["pow.link"]
```

### Ejemplo de contenido para associated domains

- applinks:mysubdomain.powlink.io
- applinks:shortsubdomain.pwlnk.io

![captura_de_pantalla_2018-11-09_a_las_14.58.25.png](/captura_de_pantalla_2018-11-09_a_las_14.58.25.png)

# Integración Behavior

Con EMMA puedes realizar una integración completa del SDK que te permita conocer la localización de tus usuarios, cómo se registran en tu App, cuántas transacciones realizan y hasta sus características propias. Es decir, toda la información de tus usuarios que obtendrás en la sección de Behavior.

## Medición de eventos

La plataforma de EMMA hace la diferenciación entre dos tipos de eventos. Los que la plataforma incluye por defecto y los eventos Custom que quieras integrar según la estructura de tu aplicación.

### Eventos por defecto

Puedes consultar más información sobre los eventos por defecto [aquí](https://docs.emma.io/es/primeros-pasos/eventos#eventos-por-defecto).

Sigue los siguientes pasos para su integración:

#### Sign Up/Registros/Leads

El método `EMMA.registerUser(userId:forMail:andExtras:)` permite enviar información sobre los registros en la aplicación.

*Ejemplo:*
#### Tabs {.tabset}
##### Swift
```swift
  func register() {
    EMMA.registerUser("554234", forMail: "test@emma.io")
  }
```
##### Objective-C
```objc
- (void) registerUser {
    [EMMALegacy registerUser:@"554234" forMail:@"test@emma.io"];
}
```

#### Login

El método `EMMA.loginUser(userId:forMail:andExtras:)` permite enviar información sobre los eventos `login`.

Si tenemos un evento `login` sucesivo con los mismos datos, podemos utilizar el método `EMMA.loginDefault()`. Este método sería útil en el caso de un "Auto-Login", por ejemplo.

#### Tabs {.tabset}
##### Swift
```swift
func login() {
	EMMA.loginUser("554234", forMail: "test@emma.io")
}
```
##### Objective-C
```objc
- (void) registerUser {
    [EMMALegacy loginUser:@"554234" forMail:@"test@emma.io"];
}
```

### Eventos personalizados

Usa `EMMA.trackEvent(request:)` para contar el número de veces que ciertos eventos suceden durante una sesión en tu app.

Esto puede ser útil para medir cuántas veces los usuarios convierten en diferentes acciones, por ejemplo. 

Puedes obtener los tokens de eventos creándolos en la plataforma de EMMA. Si un token no existente es enviado a EMMA, se devolverá un error.

Para más información sobre eventos, consulta la documentación [aquí](https://docs.emma.io/es/primeros-pasos/eventos).


#### Tabs {.tabset}
##### Swift
```swift
let eventRequest = EMMAEventRequest.init(token: "<token>")
// Optional: You can add your custom event attributes
eventRequest.attributes = ["test_attribute":"test_value"]
// Optional. You can capture emma requests with this delegate
eventRequest.requestDelegate = self
// Optional. Append your request ID to capture it later
eventRequest.customId = "MY_EVENT_REQUEST"
        
EMMA.trackEvent(request: eventRequest)
```
##### Objective-C
```objc
    EMMAEventRequest * request = [[EMMAEventRequest alloc] initWithToken:@"<token>"];
    // Optional: You can add your custom event attributes
    request.attributes = @{@"test_attribute": @"test_value"};
    // Optional. You can capture emma requests with this delegate
    request.requestDelegate = self;
    // Optional. Append your request ID to capture it later
    request.customId = @"MY_EVENT_REQUEST";
    
    [EMMALegacy trackEvent:request];
```

#### Control de flujo en las peticiones de eventos

Para la *request* de eventos se puede añadir un delegado que controle el estado de la petición y si esta devuelve datos (p.e. una *rule*) o no.

*Ejemplo:*

#### Tabs {.tabset}
##### Swift
```swift
class EventExample: NSObject, EMMARequestDelegate {
    
    func sendAdvancedEvent() {
        let eventRequest = EMMAEventRequest.init(token: "<token>")
        // Optional: You can add your custom event attributes
        eventRequest?.attributes = ["test_attribute":"test_value"]
        // Optional. You can capture emma requests with this delegate
        eventRequest?.requestDelegate = self
        // Optional. Append your request ID to capture it later
        eventRequest?.customId = "MY_EVENT_REQUEST"
        
        EMMA.trackEvent(eventRequest)
    }
    
    /****** EMMARequestDelegate Protocol ********/
    func onStarted(_ id: String!) {
        if id == "MY_EVENT_REQUEST" {
            print("Request for MY_EVENT_REQUEST started")
        }
    }
    
    func onSuccess(_ id: String!, containsData data: Bool) {
        if id == "MY_EVENT_REQUEST" {
            print("Request for MY_EVENT_REQUEST succeed")
        }
    }
    
    func onFailed(_ id: String!) {
        if id == "MY_EVENT_REQUEST" {
            print("Request for MY_EVENT_REQUEST failed")
        }
    }
}
```
##### Objective-C
```objc
// ViewController.h
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

@interface ViewController : UIViewController<EMMARequestDelegate>
@end

// ViewController.m
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) sendAdvancedEvent {
    EMMAEventRequest * request = [[EMMAEventRequest alloc] initWithToken:@"<token>"];
    // Optional: You can add your custom event attributes
    request.attributes = @{@"test_attribute": @"test_value"};
    // Optional. You can capture emma requests with this delegate
    request.requestDelegate = self;
    // Optional. Append your request ID to capture it later
    request.customId = @"MY_EVENT_REQUEST";
    
    [EMMALegacy trackEvent:request];
}

- (void)onFailed:(NSString *)requestId {
    if ([requestId isEqualToString:@"MY_EVENT_REQUEST"]) {
        NSLog(@"Request for MY_EVENT_REQUEST failed");
    }
}

- (void)onStarted:(NSString *)requestId {
    if ([requestId isEqualToString:@"MY_EVENT_REQUEST"]) {
        NSLog(@"Request for MY_EVENT_REQUEST started");
    }
}

- (void)onSuccess:(NSString *)requestId containsData:(BOOL)data {
    if ([requestId isEqualToString:@"MY_EVENT_REQUEST"]) {
        NSLog(@"Request for MY_EVENT_REQUEST success");
    }
}

@end
```

### Medir transacciones
EMMA permite medir cualquier transacción o compra que se realice en tu app. Este es el ejemplo para medir una transacción:
#### Tabs {.tabset}
##### Swift
```swift
    func trackTransaction() {
        EMMA.startOrder("<ORDER_ID>", customerId: "<CUSTOMER_ID>", totalPrice: 10.0, coupon: "")
        EMMA.addProduct("<PRODUCT_ID>", name: "<PRODUCT_NAME>", qty: 1.0, price: 10.0)
        EMMA.trackOrder()
    }
```
##### Objective-C
```objc
- (void) trackTransaction {
    [EMMALegacy startOrder:@"<ORDER_ID>" customerId:@"<CUSTOMER_ID>" totalPrice:10.0 coupon:@""];
    [EMMALegacy addProduct:@"<PRODUCT_ID>"name:@"<PRODUCT_NAME>" qty:1.0 price:10.0];
    [EMMALegacy trackOrder];
}
```

#### Iniciar transacción

El método para iniciar la transacción es `EMMA.startOrder(orderId:andCustomer:withTotalPrice:withExtras:assignCoupon:)`.

#### Tabs {.tabset}
##### Swift
```swift
EMMA.startOrder("<ORDER_ID>", customerId: "<CUSTOMER_ID>", totalPrice: 10.0, coupon: "")
```
##### Objective-C
```objc
[EMMALegacy startOrder:@"<ORDER_ID>" customerId:@"<CUSTOMER_ID>" totalPrice:10.0 coupon:@""];
```


#### Añadir Productos a la transacción

Una vez iniciada la transacción hay que añadir los productos a la misma. Para ello usaremos el método `EMMA.addProduct(productId:andName:withQty:andPrice:withExtras:)`.

#### Tabs {.tabset}
##### Swift
```swift
EMMA.addProduct("<PRODUCT_ID>", name: "<PRODUCT_NAME>", qty: 1.0, price: 10.0)
```
##### Objective-C
```objc
[EMMALegacy addProduct:@"<PRODUCT_ID>"name:@"<PRODUCT_NAME>" qty:1.0 price:10.0];
```

#### Medición de la transacción

Una vez tenemos todos los productos añadidos, ejecutamos la medición de la transacción con el método `EMMA.trackOrder()`.

#### Tabs {.tabset}
##### Swift
```swift
EMMA.trackOrder()
```
##### Objective-C
```objc
[EMMALegacy trackOrder];
```

#### Cancelar una transacción

En el caso de que se necesite cancelar el tracking de una transacción usaremos el método `EMMA.cancelOrder(orderId:)`.

#### Tabs {.tabset}
##### Swift
```swift
    func cancelTransaction() {
        EMMA.cancelOrder("<ORDER_ID>")
    }
```
##### Objective-C
```objc
-(void) cancelTransaction {
    [EMMALegacy cancelOrder:@"<ORDER_ID>"];
}
```

## Propiedades del usuario (TAGS)

El método `EMMA.trackExtraUserInfo(info:)` actualiza o añade parámetros extra de cara a obtener mejor segmentación en el filtrado `Users with tag`. Puede ser usado en el registro, en el login o en cualquier otra sección de la app donde se recoja información del usuario.

> Si quieres usar la EMMA RULE `On his Birthday`, envía la fecha de cumpleaños con el método para el TAG en el siguiente formato (ISO):
**Nombre:** `BIRTHDAY` **Valor:** `YYYY-MM-DD`
{.is-info}

> Recuerda revisar los logs del SDK para consultar el listado de TAGS que no pueden ser usados por estar reservados para el sistema de EMMA.
{.is-info}

#### Tabs {.tabset}
##### Swift
```swift
    func trackUserInfo() {
        //In this example we set the tag "AGE" of our user to value "40"
        EMMA.trackExtraUserInfo(["AGE" : "40"])
    }
```
##### Objective-C
```objc
-(void) trackUserInfo {
    [EMMALegacy trackExtraUserInfo:@{@"AGE": @"40"}];
}
```


## Registro de la localización del usuario

EMMA puede adjuntar la localización actual del usuario, si la aplicación dispone de permisos con el método `EMMA.trackLocation()`.

## Información del usuario

### Identificador de EMMA

Podemos recuperar el ID de usuario de EMMA con el método `EMMA.getUserId(resultBlock:)`.

Este método devuelve el EMMA ID como un *String*. Este ID es único para cada usuario y puede ser utilizado para filtrar cuando se envían comunicaciones.

#### Tabs {.tabset}
##### Swift
```swift
    func getUserID() {
        /* This method gets EMMA user id */
        EMMA.getUserId { (user_id) in
            guard let uid = user_id else {
                print("Error getting user id")
                return
            }
            print("Our EMMA USER ID is \(uid)")
        }
    }
```
##### Objective-C
```objc
-(void) getUserId {
    [EMMALegacy getUserId:^(NSString *userId) {
        if  (userId) {
            NSLog(@"User id is %@", userId);
        }
    }]
}
```

### Identificador de dispositivo

El formato del identificador es de tipo UUID V4. Para obtener el identificador del dispositivo usa el siguiente método:

#### Tabs {.tabset}
##### Swift
```swift
    EMMA.deviceId()
```
##### Objective-C
```objc
	[EMMALegacy deviceId];
```

### Identificador de usuario del cliente (Customer ID)

Para enviar el customer ID independientemente del login/registro usa el siguiente método:

#### Tabs {.tabset}
##### Swift
```swift
    EMMA.setCustomerId(customerId: "<Customer ID>")
```
##### Objective-C
```objc
	[EMMALegacy setCustomerId:@"<Customer ID>"];
```


### Obtener información de usuario

El método `EMMA.getUserInfo(resultBlock:)` recupera el perfil del usuario que tenemos registrado en EMMA.

> Los datos que devuelve la llamada se obtienen de la propia información que recopila el SDK. En un primer arranque puede tardar unos segundos en recopilar la información, si la llamada se realiza justo después del `startSession` podría devolver nulo.
{.is-info}

[Referencia de los campos del perfil de usuario](/es/reference/user-info-fields)

#### Tabs {.tabset}
##### Swift
```swift
    func getUserInfo() {
        /* This method retrieves a json representation
            of emma's user profile */
        EMMA.getUserInfo { (user_profile) in
            guard let profile = user_profile else {
                print("Error getting user profile")
                return
            }
            print("Retrieved user profile \(profile)")
        }
    }
```
##### Objective-C
```objc
-(void) getUserInfo {
    [EMMALegacy getUserInfo:^(NSDictionary *userInfo) {
        if (userInfo) {
            NSLog(@"Retrieved user profile %@", userInfo);
        }
    }]
}
```

## Información de la atribución de instalación

A través del método `EMMA.installAttributionInfo(attributionDelegate:)` podremos obtener los datos de la atribución de la instalación de cada usuario.

Consulta la [descripción de los campos de atribución](/es/reference/user-info-fields#descripción-de-los-campos-de-atribución-del-usuario) para ver la información disponible.

A partir de la versión 4.6.2 la campaña recibe los parámetros del click a través del método `clickParams`.

Para poder obtener información sobre la atribución de la instalación, se puede seguir el siguiente ejemplo:


#### Tabs {.tabset}
##### Swift
```swift
import UIKit
import EMMA_iOS

class AttributionInfoExampleViewController: UIViewController, EMMAInstallAttributionDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        //We can get EMMA Attribution Info
        EMMA.installAttributionInfo(self)
    }
    
    // MARK: - EMMA Attribution Delegate
    func onAttributionReceived(_ attribution: EMMAInstallAttribution!) {
        guard let info = attribution else {
            print("Error getting attribution info")
            return
        }
        print("Received attribution info \(info)")
    }
}
```

##### Objective-C
```objc
// AttributionInfoExampleViewController.h
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

@interface AttributionInfoExampleViewController : UIViewController<EMMAInstallAttributionDelegate>

@end

// AttributionInfoExampleViewController.m
#import "AttributionInfoExampleViewController.h"

@interface AttributionInfoExampleViewController ()

@end

@implementation AttributionInfoExampleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [EMMALegacy installAttributionInfo:self];
}


- (void)onAttributionReceived:(EMMAInstallAttribution *)attribution {
    if (attribution) {
        NSLog(@"Received attribution info %@", attribution);
    }
}

@end
```

# Integración Notificaciones Push

> Para diferenciar una notificación de nuestro sistema de push respecto otros sistemas, el payload enviado por EMMA contiene un flag denominado "eMMa".
{.is-warning}

EMMA te permite añadir un potente sistema de Notificaciones Push fácil de integrar. La plataforma también permite enviar info a través de las notificaciones y generar cualquier acción dentro de tu app gracias a ellas.

## Push Auth Key

El APNs Auth Key de Apple es un tipo de certificado (.p8) que remplaza a sus antecesores: el certificado APNs de producción y el certificado APNs de desarrollo. Desde EMMA recomendamos el uso de este certificado por los siguiente motivos:

- Unifica el certificado de desarrollo y producción en un solo certificado.
- Es un certificado que no se tiene que renovar cada año.
- Un solo certificado sirve para todas las apps de la cuenta de Apple.

Para el uso de este certificado es necesario:

- El certificado .p8 descargado del panel de Apple Developers.
- El bundle ID de la aplicación que tiene que recibir los Push.
- El team ID de la cuenta de Apple.

[Como obtener el Push Auth Key](/es/ios/push-auth-key) en Apple.

## Usando EMMA Notificaciones Push

Una vez generados los certificados de tu app, ya puedes empezar a integrar las Notificaciones Push. A continuación tenemos un ejemplo de un AppDelegate completo con la integración del push:


#### Tabs {.tabset}
##### Swift
```swift
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, EMMAPushDelegate {
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
 				...    
      configuration.pushNotificationsDelegate = self
      //Enable EMMA Push System
      EMMA.startPushSystem()
  }
    
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      EMMA.registerToken(deviceToken)
	}
   
	@available(iOS 10.0, *)
	func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
      if #available(iOS 14.0, *) {
          completionHandler([.badge, .sound, .banner, .list])
      } else {
          completionHandler([.badge, .sound, .alert])
      }
  }
	
  @available(iOS 10.0, *)
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
      EMMA.handlePush(response.notification.request.content.userInfo)
      completionHandler()
    }
}
```
##### Objective-C
```objc
// AppDelegate.h
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

@interface AppDelegate : UIResponder <UNUserNotificationCenterDelegate, EMMAPushDelegate> 
@end

// AppDelegate.m
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

@interface AppDelegate : UIResponder <UNUserNotificationCenterDelegate, EMMAPushDelegate> 

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    EMMAConfiguration *configuration = [EMMAConfiguration new];
    configuration.sessionKey = @"MY_EMMA_SESSION_KEY";
    configuration.debugEnabled = true;
    configuration.trackScreenEvents = false; // disable screens
    configuration.pushNotificationsDelegate = self;
    
    [EMMALegacy startSessionWithConfiguration:configuration];
    [EMMALegacy startPushSystem];
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    [EMMALegacy registerToken:deviceToken];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    if (@available(iOS 14, *)) {
        completionHandler(UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionList | UNNotificationPresentationOptionBanner);
    } else {
        completionHandler(UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert);
    }
}

-(void) userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    [EMMALegacy handlePush:response.notification.request.content.userInfo];
    completionHandler();
}
@end

- (void)onPushOpen:(EMMAPush *)push {
    if (push) {
    	NSLog(@"Obtained push campaign %@", push);
    }
}
```
####

Para mantener los valores del badge entre la extensión y la aplicación, es necesario añadir a las Capabilities de la app un nuevo AppGroup con el siguiente formato: group.YOUR_BUNDLE_ID.emma

### Activar Push Capabilities

Para registrar el token es necesario activar las Notificaciones Push en Capabilities.

![emma_14.png](/emma_14.png)

### Resetear badge

Para resetear el badge que cuenta el número de notificaciones recibidas, existen dos formas:

- Si no estás usando la extensión, simplemente con el método que proporciona Apple es suficiente:

#### Tabs {.tabset}
##### Swift
```swift
UIApplication.sharedApplication().applicationIconBadgeNumber = 0
```
##### Objective-C
```objc
[UIApplication sharedApplication].applicationIconBadgeNumber = 0
```
####
- Si usas la extensión, el valor del badge se va almacenando e incrementando al recibir notificaciones, para resetearlo:
#### Tabs {.tabset}
##### Swift
```swift
// Clear with push system start
 let options = EMMAPushOptions()
 options.badgeClearing = true
 EMMA.startPushSystem(with: options)

// Clear anywhere in the app
 EMMAPushBadgeController.clearBadge(fromOpen: true)
```
##### Objective-C
```objc
// Clear with push system start
  EMMAPushOptions * pushOptions = [EMMAPushOptions new];
  pushOptions.badgeClearing = true
  [EMMALegacy startPushSystemWithOptions:pushOptions];
  
// Clear anywhere in the app
  [EMMAPushBadgeController clearBadgeFromOpen:true];
```
####

El badge se guarda en preferencias del usuario. Para sincronizar esas preferencias entre la app y la extensión es necesario configurar un AppGroup en "Capabilities", tanto en el target de la App como en la extensión. El formato del bundle tiene que ser "group.{bundleId}.emma", donde bundleId es el Bundle ID del target App.

![app_groups_badge.png](/ios/app_groups_badge.png)

## Rich Push

Las Rich Push Notifications se habilitan mediante una Notification Service Extension, un binario aparte dentro del paquete de tu aplicación. Antes de mostrar una nueva notificación push, el sistema llamará a tu Notification Service Extension permitiéndote modificar el contenido y los adjuntos que serán mostrados.

### Creando la Notification Service Extension

Para crear la Notification Service Extension en tu proyecto, en Xcode eliges `File -> New -> Target` y eliges la plantilla de la Notification Service Extension.

![emma_16.png](/emma_16.png)

![emma_17.png](/emma_17.png)

Puedes nombrar la extensión a tu gusto, la llamaremos RichPushExtension para esta guía. Asegúrate de incluir la extensión en tu aplicación.

![emma_18.png](/emma_18.png)

Al pulsar en *Finish* se te preguntará para activar la extensión, haz click en *Activate* para finalizar.

![emma_19.png](/emma_19.png)

Al acabar estos pasos, tres nuevos archivos han sido añadidos al directorio con el nombre de la extensión que hemos creado (RichPushExtension en nuestro caso): `NotificationService.h`, `NotificationService.m` and `Info.plist`.

![emma_20.png](/emma_20.png)

Por último, asegúrate de que están habilitadas las notificaciones push para la extensión que hemos creado. Selecciona tu nueva extension (RichPushExtension), elige Capabilities y activa Push Notifications.

![emma_21.png](/emma_21.png)

### NotificationService

#### Tabs {.tabset}
##### Swift
```swift
import UIKit
import UserNotifications
import EMMA_iOS

class NotificationService: UNNotificationServiceExtension {
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            EMMA.didReceiveNotificationRequest(request: request, withNotificationContent: bestAttemptContent) { (content) in
                contentHandler(bestAttemptContent)
            }
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
}
```
##### Objective-C
```objc 
#import "NotificationService.h"
#import "EMMA_iOS/EMMA_iOS.h"

@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    
    [EMMALegacy didReceiveNotificationRequest:request withNotificationContent:_bestAttemptContent AndCompletionHandler:^(UNNotificationContent *) {
        contentHandler(self.bestAttemptContent);
    }];
}

- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    self.contentHandler(self.bestAttemptContent);
}

@end
```

### Añadir dependencia al Pod de la extensión

Para añadir el SDK a la extensión es necesario modificar el archivo Podfile añadiendo el pod eMMa al objetivo de la extensión:

```ruby
target 'ProjectName' do
  pod 'eMMa', '~> 4.10.1'
end

target 'EMMANotificationServiceExtension' do
  pod 'eMMa', '~> 4.10.1'
end
```

### Rich Push, sonidos personalizados

Para usar sonidos personalizados en las notificaciones que envíes con EMMA, tienes que añadir los archivos de sonido .caf que quieras a tu aplicación, en la raíz del paquete de tu aplicación o en Library/Sounds/. Recuerda que debes emplear para los sonidos los mismos nombres de archivo en iOS y Android.

### Rich Push, abrir URL's y deeplinks

Puedes redireccionar las aperturas de las notificaciones push a una sección en tu app. Para ello debes usar una estructura como esta:

*scheme://host/page1/page2/page3*

### Acciones en las notificaciones

A partir de la versión del SDK 4.10.x, se ha añadido la funcionalidad de botones con acciones en las notificaciones. Para poder traquear la acción desde la cual se abre la notificación, hay que reemplazar el `handlePush` que estamos usando hasta ahora por este:


#### Tabs {.tabset}
##### Swift
```swift
	@available(iOS 10.0, *)
	func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        EMMA.handlePush(userInfo: response.notification.request.content.userInfo,
                        actionIdentifier: response.actionIdentifier
        )
        completionHandler()
	}
```
##### Objective-C

```objc 
-(void) userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    [EMMALegacy handlePush:response.notification.request.content.userInfo withActionIdentifier:response.actionIdentifier];
    completionHandler();
}
```
####

Para asegurar el correcto funcionamiento hay que comprobar que el SDK que contiene el servicio de notificaciones tiene la versión 4.10.x y está alineado con la versión de la app.

Consulta [aquí](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#contenido) para configurar un push con estás acciones.

# Integración In-App Messaging

EMMA incluye 8 formatos comunicativos diferentes que puedes integrar para impactar a tus usuarios en iOS:

- **NativeAd** - Formato que integra tus comunicaciones respetando siempre el formato y estilo de los contenidos de la App.
- **StartView** - Formato que se despliega en toda la pantalla mostrando contenido web.
- **AdBall** - Formato comunicativo con forma de burbuja, cuyo click provoca el despliegue de un contenido web.
- **Banner** - Formato básico para tus comunicaciones que puedes utilizar para comunicar mensajes específicos tanto arriba como abajo de la pantalla.
- **Strip** - Formato comunicativo que ocupa la barra de notificaciones del dispositivo y que muestra un mensaje de derecha a izquierda.
- **Coupon** - Formato que te da la opción de canjear cupones si tienes un lugar físico controlando el número de redenciones, el código promocional y la fecha de activación y finalización.
- **DynamicTab** - Formato comunicativo que se incluye como una nueva pestaña dentro de tu TabBar. Sólo disponible para iOS.
- **Plugins personalizados** - Formato de comunicación propio a través de la tecnología de NativeAd.
{.grid-list .body-2}

## NativeAd

EMMA NativeAd te permite obtener la información de un NativeAd correspondiente a una plantilla que se haya definido y configurado en la plataforma de EMMA.

Obtendremos toda la información del NativeAd disponible para el usuario referente al `templateId`, según las condiciones que se hayan configurado en la plataforma de EMMA. El objeto `EMMANativeAd` contiene todos los campos configurados en EMMA para esta plantilla de NativeAd, para obtenerlos se usará el siguiente método:

#### Tabs {.tabset}
##### Swift
```swift
import UIKit
import EMMA_iOS

class NativeAdExampleViewController: UIViewController, EMMAInAppMessageDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func getNativeAd(templateId: String) {
        let nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        EMMA.inAppMessage(nativeAdRequest, with: self)
    }
    
    func getBatchNativeAd(templateId: String) {
        let nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        nativeAdRequest.isBatch = true
        EMMA.inAppMessage(nativeAdRequest, with: self)
    }
    
    // MARK: - InAppMessage Delegate
    func onReceived(_ nativeAd: EMMANativeAd!) {
    		let content = nativeAd.nativeAdContent as? [String:AnyObject]
        if let title = content?["Title"] as? String) {
            print("Received NativeAd with Title: \(title)")
            // Draw Native Ad and Send Impression
            EMMA.sendImpression(.campaignNativeAd, withId: String(nativeAd.idPromo))
        }
    }
    
    func onBatchNativeAdReceived(_ nativeAds: [EMMANativeAd]!) {
        nativeAds.forEach { (nativeAd) in
            if let tag = nativeAd.tag {
                print("Received batch nativead with tag: \(tag)")
            }
        }
    }
    
    func openNativeAd(nativeAd: EMMANativeAd) {
        // This method executes CTA Action and sends NativeAd Click
        EMMA.openNativeAd(String(nativeAd.idPromo))
    }
    
    func sendNativeAdClick(nativeAd: EMMANativeAd) {
        // Send manual click. Useful if we want to override CTA action
        EMMA.sendClick(.campaignNativeAd, withId: String(nativeAd.idPromo))
    }
    
    func onShown(_ campaign: EMMACampaign!) {
        
    }
    
    func onHide(_ campaign: EMMACampaign!) {
        
    }
    
    func onClose(_ campaign: EMMACampaign!) {
        
    }
}
```
##### Objective-C
```objc
// NativeAdExampleViewController.h
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

@interface NativeAdExampleViewController : UIViewController<EMMAInAppMessageDelegate>
@end

// NativeAdExampleViewController.m
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

#import "NativeAdExampleViewController.h"

@interface NativeAdExampleViewController ()

@end

@implementation NativeAdExampleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) getNativeAd: (NSString*) templateId {
    EMMANativeAdRequest * nativeAdRequest = [EMMANativeAdRequest new];
    nativeAdRequest.templateId = templateId;
    [EMMALegacy inAppMessage: nativeAdRequest withDelegate:self];
}

- (void) getBatchNativeAd: (NSString *) templateId {
    EMMANativeAdRequest * nativeAdRequest = [EMMANativeAdRequest new];
    nativeAdRequest.templateId = templateId;
    nativeAdRequest.isBatch = YES;
    [EMMALegacy inAppMessage: nativeAdRequest withDelegate:self];
}

- (void) onReceived:(EMMANativeAd *)nativeAd {
    NSDictionary * nativeAdContent = [nativeAd nativeAdContent];
    NSString * title = [nativeAdContent objectForKey:@"Title"];
    if (title) {
        NSLog(@"Received NativeAd with title %@", title);
        // Show Native Ad
        // Send impression
        [EMMALegacy sendImpression: kCampaignNativeAd withId:[@(nativeAd.idPromo) stringValue]];
    }
}

- (void) openNativeAd: (EMMANativeAd *) nativeAd {
    // This method executes CTA Action and sends NativeAd Click
    [EMMALegacy openNativeAd: [@(nativeAd.idPromo) stringValue]];
}

- (void) sendNativeAdClick: (EMMANativeAd *) nativeAd {
		// Send manual click. Useful if we want to override CTA action
    [EMMALegacy sendClick:kCampaignNativeAd withId:[@(nativeAd.idPromo) stringValue]];
}

- (void)onClose:(EMMACampaign *)campaign {
   
}

- (void)onHide:(EMMACampaign *)campaign {
    
}

- (void)onShown:(EMMACampaign *)campaign {
    
}
@end
```
####

Se llama a **onReceived** en el caso de que exista un NativeAd correspondiente a la plantilla con identificador “templateId”.

Se llama a **onBatchReceived** cuando se realiza la llamada de NativeAd con el parámetro batch a true y existen uno o más NativeAds correspondientes a la plantilla con identificador "templateId".

`EMMANativeAd` contiene todos los campos configurados en EMMA para esta plantilla de NativeAd, para obtenerlos se usará el siguiente método:

```swift
		let content = nativeAd.nativeAdContent as? [String:AnyObject]
    let title = content?["Title"] as? String
    let image = content?["Main picture"] as? String
    let cta = content?["CTA"] as? String
    printSide(title, image, cta)
````

En el caso de configurar una plantilla contenedor, se pueden obtener los valores del NativeAd de la siguiente forma:

```swift
   let content = nativeAd.nativeAdContent
   if let container = content?["container"] as? Array<[String: String]> {
      container.forEach { (containerFields: [String : String]) in
        let title = containerFields?["Title"]
        let image = containerFields?["Main picture"]
        let cta = containerFields?["CTA"]
        printSide(title, image, cta)
      }
   }
````

Una vez obtenidos todos los campos requeridos, ya se puede crear la vista para pintar este NativeAd en pantalla en función del diseño que se le quiera aplicar. Una vez pintado el NativeAd en pantalla, es necesario llamar a este método para poder obtener las impresiones en el reporting:

#### Tabs {.tabset}
##### Swift
```swift
EMMA.sendImpression(.campaignNativeAd, withId: String(nativeAd.idPromo))
```
##### Objective-C
```objc 
- (void) sendNativeAdClick: (EMMANativeAd *) nativeAd {
	[EMMALegacy sendClick:kCampaignNativeAd withId:[@(nativeAd.idPromo) stringValue]];
}
````

### Abrir un NativeAd
#### Tabs {.tabset}
##### Swift
```swift
    func openNativeAd(nativeAd: EMMANativeAd) {
        // This method executes CTA Action and sends NativeAd Click
        EMMA.openNativeAd(String(nativeAd.idPromo))
    }
```
##### Objective-C
```objc 
- (void) openNativeAd: (EMMANativeAd *) nativeAd {
		// This method executes CTA Action and sends NativeAd Click
    [EMMALegacy openNativeAd: [@(nativeAd.idPromo) stringValue]];
}
```
####


Con esta llamada, se mostrará el contenido del link configurado en el NativeAd desde la plataforma de EMMA. El método `openNativeAd` internamente envía el *event click* a EMMA.

Alternativamente, si no se usa este método, se puede enviar el click llamando al método:

#### Tabs {.tabset}
##### Swift
```swift
    func sendNativeAdClick(nativeAd: EMMANativeAd) {
        // Send manual click. Useful if we want to override CTA action
        EMMA.sendClick(.campaignNativeAd, withId: String(nativeAd.idPromo))
    }
```
##### Objective-C
```objc
- (void) sendNativeAdClick: (EMMANativeAd *) nativeAd {
    // Send manual click. Useful if we want to override CTA action
    [EMMALegacy sendClick:kCampaignNativeAd withId:[@(nativeAd.idPromo) stringValue]];
}
```
### Native Ad Múltiple (Batch)

Para NativeAd múltiple se utilizará una clase de `EMMAInAppRequest` o una subclase de `EMMANativeAdRequests`. Hasta ahora el parámetro `templateId` iba en la clase `EMMAInAppRequest`, ahora quedará *deprecated* y pasará a ser parámetro de la subclase `EMMANativeAdRequest`. Se podrá usar de ambas formas, pero la forma correcta será en la subclase.

#### Tabs {.tabset}
##### Swift
```swift
    func getBatchNativeAd(templateId: String) {
        let nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        nativeAdRequest.isBatch = true
        EMMA.inAppMessage(nativeAdRequest, with: self)
    }
```
##### Objective-C
```objc
- (void) getBatchNativeAd: (NSString *) templateId {
    EMMANativeAdRequest * nativeAdRequest = [EMMANativeAdRequest new];
    nativeAdRequest.templateId = templateId;
    nativeAdRequest.isBatch = YES;
    [EMMALegacy inAppMessage: nativeAdRequest withDelegate:self];
}
```

### NativeAd único
#### Tabs {.tabset}
##### Swift
```swift
    func getNativeAd(templateId: String) {
        let nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        EMMA.inAppMessage(nativeAdRequest, with: self)
    }
```

##### Objective-C
```objc
- (void) getNativeAd: (NSString*) templateId {
    EMMANativeAdRequest * nativeAdRequest = [EMMANativeAdRequest new];
    nativeAdRequest.templateId = templateId;
    [EMMALegacy inAppMessage: nativeAdRequest withDelegate:self];
}
```

## StartView

La StartView de EMMA te permite mostrar información en HTML al inicio de tu como una WebView.
Para permitir el uso de StartViews debes integrar:

#### Tabs {.tabset}
##### Swift
```swift
    func getStartView() {
        let startViewinAppRequest = EMMAInAppRequest(type: .Startview)
        // Optional. You can filter by label
        startViewinAppRequest?.label = "<LABEL>"
        /*
         By default Startview presents on UIApplication.shared.delegate?.window?.rootViewController
         You can customize this behavior uncommenting following line
        */
        //EMMA.setRootViewController(UIViewController!)
        EMMA.inAppMessage(startViewinAppRequest)
    }
```
##### Objective-C
```objc 
-(void) getStartView {
    /* By default Startview presents on UIApplication.shared.delegate?.window?.rootViewController
    You can customize this behavior uncommenting following line */
    //[EMMALegacy setRootViewController:(UIViewController *)]
    EMMAInAppRequest *startViewRequest = [[EMMAInAppRequest alloc] initWithType: Startview];
    [EMMALegacy inAppMessage:startViewRequest];
}
```

## AdBall

EMMA AdBall te permite mostrar un pequeño gráfico circular en tu aplicación, que el usuario puede desplazar libremente por la pantalla de inicio de la app y también cerrarla. Si se presiona sobre este, aparece un pop-up con contenido HTML.

#### Tabs {.tabset}
##### Swift
```swift
    func getAdBall() {
        let adballRequest = EMMAInAppRequest(type: .Adball)
        EMMA.inAppMessage(adballRequest)
    }
```
##### Objective-C
```objc 
-(void) getStartView {
    /* By default Startview presents on UIApplication.shared.delegate?.window?.rootViewController
    You can customize this behavior uncommenting following line */
    //[EMMALegacy setRootViewController:(UIViewController *)]
    EMMAInAppRequest *startViewRequest = [[EMMAInAppRequest alloc] initWithType: Startview];
    [EMMALegacy inAppMessage:startViewRequest];
}
```

### Comprobar si se está mostrando el AdBall
Utilizar para comprobar si el AdBall se está mostrando en la pantalla del dispositivo. Devuelve `BOOL` `true` si se está mostrando en la pantalla.
#### Tabs {.tabset}
##### Swift
```swift
        //Check if any adball is showing
        if EMMA.isAdBallShowing() {
            print("There is an adball floating arround")
        }
```
##### Objective-C
```objc 
		//Check if any adball is showing
    if ([EMMALegacy isAdBallShowing]) {
        NSLog(@"There is an adball floating arround");
    }
```

## Banner

Los Banners de EMMA te permiten mostrar un banner en tu app con información promocional customizada. El banner será mostrado en función de la configuración que establezcas en el dashboard de EMMA. Es una comunicación que permite mostrarle al usuario una WebView externa con contenido HTML o redireccionar a otra pestaña de la aplicación (vía deeplink).

Para mostrar los Banners en tu app necesitas al menos integrar el primer método descrito a continuación:

>Se recomienda hacer la llamada dentro de la función `viewDidLayoutSubviews`.
{.is-warning}

#### Tabs {.tabset}
##### Swift
```swift
    func getBanner() {
        let bannerRequest = EMMAInAppRequest(type: .Banner)
        EMMA.inAppMessage(bannerRequest)
    }
```
##### Objective-C
```objc 
-(void) getBanner {
    EMMAInAppRequest *bannerRequest = [[EMMAInAppRequest alloc] initWithType:Banner];
    [EMMALegacy inAppMessage:bannerRequest];
}
```

## Strip

El EMMA Strip te permite mostrar un banner de texto sobre donde se sitúa habitualmente el Status Bar con un mensaje para sus usuarios. Este puede ser desplegado en cualquier pantalla de la app.

#### Tabs {.tabset}
##### Swift
```swift
    func getStrip() {
        let stripRequest = EMMAInAppRequest(type: .Strip)
        EMMA.inAppMessage(stripRequest)
    }
```
##### Objective-C
```objc 
-(void) getStrip {
    EMMAInAppRequest * stripRequest = [[EMMAInAppRequest alloc] initWithType: Strip];
    [EMMALegacy inAppMessage:stripRequest];
}
```

## Coupon

EMMA Coupons te permite obtener, verificar y canjear cupones que se hayan definido y configurado en la plataforma de EMMA.

Se devolverá un listado de los cupones existentes, listándose primero los cupones automáticos ordenados de más reciente a más antiguos y después se listarán los cupones clásicos ordenados también de más reciente a más antiguo.

En el block response, obtendremos un diccionario con la información relativa a cada cupón disponible para el usuario. Para cada cupón se dispone de la siguiente información: id (identificador interno de EMMA), código, número máximo de canjeos, número de veces canjeado, título, descripción, imagen, etc.

### Ejemplo completo de integración de Coupons

#### Tabs {.tabset}
##### Swift
```swift
import UIKit
import EMMA_iOS

class CouponExampleViewController: UIViewController, EMMACouponDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add coupon delegate
        EMMA.add(self)
    }
    
    func getAllCoupons() {
        let couponsRequest = EMMAInAppRequest(type: .Coupons)
        EMMA.inAppMessage(couponsRequest)
    }
    
    func sentCouponClick(_ coupon: EMMACoupon) {
        EMMA.sendClick(.campaignCoupon, withId: String(coupon.couponId))
    }
    
    func getSingleCoupon() {
        let couponsRequest = EMMAInAppRequest(type: .Coupons)
        couponsRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(couponsRequest)
    }
    
    func checkCouponRedeems() {
        let couponsRequest = EMMAInAppRequest(type: .CouponValidRedeems)
        // You must pass coupon id to check
        couponsRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(couponsRequest)
    }
    
    func redeemCoupon() {
        let redeemCouponRequest = EMMAInAppRequest(type: .RedeemCoupon)
        redeemCouponRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(redeemCouponRequest)
    }
    
    func cancelCoupon() {
        let cancelCouponRequest = EMMAInAppRequest(type: .CancelCoupon)
        cancelCouponRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(cancelCouponRequest)
    }
    
    // MARK: - EMMA Coupon Delegate methods
    func onCouponsReceived(_ coupons: [EMMACoupon]!) {
        guard let receivedCoupons = coupons else {
            print("Error retrieving coupons")
            return
        }
        // Now we can send coupon impressions
        receivedCoupons.forEach { (coupon) in
            EMMA.sendImpression(.campaignCoupon, withId: String(coupon.couponId))
        }
        print("Received coupons \(receivedCoupons)")
    }
    
    func onCouponsFailure() {
        print("Error retrieving coupons")
    }
    
    func onCouponValidRedeemsReceived(_ validRedeems: Int) {
        if validRedeems > 0 {
            print("Coupon have valid redeems pending \(validRedeems)")
        }
    }
}
```
##### Objective-C
```objc
// CouponExampleViewController.h
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

@interface CouponExampleViewController : UIViewController<EMMACouponDelegate>
@end

// CouponExampleViewController.m
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

#import "CouponExampleViewController.h"

@interface CouponExampleViewController ()

@end

@implementation CouponExampleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [EMMALegacy addCouponDelegate:self];
}

-(void) getAllCoupons {
    EMMAInAppRequest * couponsRequest = [[EMMAInAppRequest alloc] initWithType:Coupons];
    [EMMALegacy inAppMessage:couponsRequest];
}

-(void) sendCouponClick:(EMMACoupon*) coupon {
    [EMMALegacy sendClick:kCampaignCoupon withId:[@(coupon.couponId) stringValue]];
}

-(void) getSingleCoupon {
    EMMAInAppRequest * couponRequest = [[EMMAInAppRequest alloc] initWithType:Coupons];
    couponRequest.inAppMessageId = @"<COUPON_ID>";
    [EMMALegacy inAppMessage:couponRequest];
}

-(void) checkCouponsRedeem {
    EMMAInAppRequest *couponsRequest = [[EMMAInAppRequest alloc] initWithType:CouponValidRedeems];
    couponsRequest.inAppMessageId = @"<COUPON_ID>";
    [EMMALegacy inAppMessage:couponsRequest];
}

-(void) redeemCoupon {
    EMMAInAppRequest *redeemRequest = [[EMMAInAppRequest alloc] initWithType:RedeemCoupon];
    redeemRequest.inAppMessageId = @"<COUPON_ID>";
    [EMMALegacy inAppMessage:redeemRequest];
}

-(void) cancelCoupon {
    EMMAInAppRequest * cancelCouponRequest = [[EMMAInAppRequest alloc] initWithType: CancelCoupon];
    cancelCouponRequest.inAppMessageId = @"<COUPON_ID>";
    [EMMALegacy inAppMessage:cancelCouponRequest];
}

-(void)onCouponsFailure {
    NSLog(@"Error retrieving coupons");
}

-(void)onCouponsReceived:(NSArray<EMMACoupon *> *)coupons {
    for (EMMACoupon * coupon in coupons) {
        NSLog(@"Coupon %lo received", coupon.couponId);
        // Show coupon
        [EMMALegacy sendImpression: kCampaignCoupon withId:[@(coupon.couponId) stringValue]];
    }
}

-(void) onCouponValidRedeemsReceived:(int)validRedeems {
    if (validRedeems > 0) {
        NSLog(@"Coupon have valid redeems pending %d", validRedeems);
    }
}
@end

```

### Detalles de un cupón
Con esta llamada, obtendremos la información relativa a un cupón concreto.

El parámetro `couponId` debe ser el identificador interno de EMMA de un cupón, identificador que se puede obtener de una llamada a Coupons hecha con anterioridad.

En el block response, obtendremos un diccionario con la información relativa al cupón consultado: id (identificador interno de EMMA), código, número máximo de canjeos, número de veces canjeado, título, descripción, imagen, etc.

#### Tabs {.tabset}
##### Swift
```swift
    func getSingleCoupon() {
        let couponsRequest = EMMAInAppRequest(type: .Coupons)
        couponsRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(couponsRequest)
    }
    
    func onCouponsReceived(_ coupons: [EMMACoupon]!) {
        guard let receivedCoupons = coupons else {
            print("Error retrieving coupons")
            return
        }
        // Now we can send coupon impressions
        receivedCoupons.forEach { (coupon) in
            EMMA.sendImpression(.campaignCoupon, withId: String(coupon.couponId))
        }
        print("Received coupons \(receivedCoupons)")
    }
```
##### Objective-C
``` objc
-(void) getSingleCoupon {
    EMMAInAppRequest * couponRequest = [[EMMAInAppRequest alloc] initWithType:Coupons];
    couponRequest.inAppMessageId = @"<COUPON_ID>";
    [EMMALegacy inAppMessage:couponRequest];
}

-(void)onCouponsReceived:(NSArray<EMMACoupon *> *)coupons {
    for (EMMACoupon * coupon in coupons) {
        NSLog(@"Coupon %lo received", coupon.couponId);
        // Show coupon
        [EMMALegacy sendImpression: kCampaignCoupon withId:[@(coupon.couponId) stringValue]];
    }
}
```
### Revisar la validez de un cupón

Con esta llamada podemos comprobar si el usuario puede canjear el cupón indicado.

El parámetro `couponId` debe ser el identificador interno de EMMA de un coupon, identificador que se puede obtener de una llamada a `checkForCoupons` hecha con anterioridad.

En el block response, nos indicará el número de veces que el usuario todavía puede canjear el cupón.

#### Tabs {.tabset}
##### Swift
```swift
    func checkCouponRedeems() {
        let couponsRequest = EMMAInAppRequest(type: .CouponValidRedeems)
        // You must pass coupon id to check
        couponsRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(couponsRequest)
    }
    
    func onCouponValidRedeemsReceived(_ validRedeems: Int) {
        if validRedeems > 0 {
            print("Coupon have valid redeems pending \(validRedeems)")
        }
    }
```
##### Objective-C

```objc 
-(void) checkCouponsRedeem {
    EMMAInAppRequest *couponsRequest = [[EMMAInAppRequest alloc] initWithType:CouponValidRedeems];
    couponsRequest.inAppMessageId = @"<COUPON_ID>";
    [EMMALegacy inAppMessage:couponsRequest];
}

-(void) onCouponValidRedeemsReceived:(int)validRedeems {
    if (validRedeems > 0) {
        NSLog(@"Coupon have valid redeems pending %d", validRedeems);
    }
}
```

### Canjear un cupón

Con esta llamada, el usuario canjea el cupón indicado.

El parámetro `couponId` debe ser el identificador interno de EMMA de un cupón, identificador que se puede obtener de una llamada a Coupons hecha con anterioridad.

#### Tabs {.tabset}
##### Swift
```swift
    func redeemCoupon() {
    	let redeemCouponRequest = EMMAInAppRequest(type: .RedeemCoupon)
      redeemCouponRequest?.inAppMessageId = "<COUPON_ID>"
      EMMA.inAppMessage(redeemCouponRequest)
    }
```
##### Objective-C

```objc 
-(void) redeemCoupon {
     EMMAInAppRequest *couponsRequest = [[EMMAInAppRequest alloc] initWithType:CouponValidRedeems];
    couponsRequest.inAppMessageId = @"<COUPON_ID>";
    [EMMALegacy inAppMessage:couponsRequest];
}
```
### Cancelar un cupón

Con esta llamada se puede cancelar el canjeo de un cupón realizado con anterioridad.

El parámetro `couponId` debe ser el identificador interno de EMMA de un cupón identificador que se puede obtener en la llamada de tipo Coupons.

Opcionalmente se puede indicar un parámetro `count` si se quiere cancelar más de un canjeo realizado con anterioridad. Si no se indica, se cancelará una vez el cupón.

> El error 511 informa de que el *redeem* no se ha podido completar por alguna razón. Algunas posibles causas podrían ser la interrupción de la conexión con la BBDD o múltiples redenciones afectadas por un *capping*.  Recomendamos, en estos casos, tratar este error y avisar al usuario final.
{.is-info}

#### Tabs {.tabset}
##### Swift
```swift
    func cancelCoupon() {
        let cancelCouponRequest = EMMAInAppRequest(type: .CancelCoupon)
        cancelCouponRequest?.inAppMessageId = "<COUPON_ID>"
        EMMA.inAppMessage(cancelCouponRequest)
    }
```
##### Objective-C

```objc 
-(void) cancelCoupon {
    EMMAInAppRequest * cancelCouponRequest = [[EMMAInAppRequest alloc] initWithType: CancelCoupon];
    cancelCouponRequest.inAppMessageId = @"<COUPON_ID>";
    [EMMALegacy inAppMessage:cancelCouponRequest];
}
```

## DynamicTab

El EMMA TabBar View (DynamicTab) te permite mostrar información en HTML en una nueva sección de su aplicación (sólo en el caso de que ya utilice un TabBar en su app). 

El nuevo TabBar Item puede ser creado localmente o en la plataforma de EMMA. Si se realiza de ambas formas, prevalecerá el TabBar Item dinámico creado directamente en EMMA .

Para habilitar esta posibilidad en su app, necesitará implementar al menos el primer método. Se puede configurar siguiendo estas indicaciones:

> El índex del ítem del TabBar y el ítem propiamente dicho puede ser especificado en la plataforma de EMMA. Como se indicaba anteriormente, la configuración en EMMA prevalecerá siempre sobre la configuración local.
{.is-warning}

#### Tabs {.tabset}
##### Swift
```swift
    func getDynamicTabBar() {
        // You must define your UITabBarController
        // Uncomment following line!
        // EMMA.setPromoTabBarController(UITabBarController!)
        
        // Sets default promo tab index if not defined in EMMA Platform
        EMMA.setPromoTabBarIndex(5)
        
        // Sets a tab bar item to be shown if not defined in EMMA Platform
        // EMMA.setPromoTabBarItem(UITabBarItem!)
        
        let dynamicTabBarRequest = EMMAInAppRequest(type: .PromoTab)
        EMMA.inAppMessage(dynamicTabBarRequest)
    }
```
##### Objective-C
```objc 
-(void) getDynamicTarBar {
    // You must define your UITabBarController
    // Uncomment following line!
    //[EMMALegacy setPromoTabBarController:<#(UITabBarController *)#>];
    
    // Sets default promo tab index if not defined in EMMA Platform
    [EMMALegacy setPromoTabBarIndex:5];
    
    // Sets a tab bar item to be shown if not defined in EMMA Platform
    //[EMMALegacy setPromoTabBarItem:<#(UITabBarItem *)#>];
    
    EMMAInAppRequest * dynamicRequest = [[EMMAInAppRequest alloc] initWithType:PromoTab];
    [EMMALegacy inAppMessage:dynamicRequest];
}
```

## Plugins personalizados
### Crear plugin
A partir de la versión 4.9.0 se ha añadido la posibilidad de poder añadir plugins in-app al SDK. Los plugins in-app funcionan a través de la tecnología de NativeAd.

Puedes crear tu propio formato de comunicación y convertirlo en un plugin in-app, para ello es necesario que la clase principal del nuevo formato extienda de la clase abstracta `EMMAInAppPlugin`, esta clase obliga a sobrescribir dos métodos:

#### Tabs {.tabset}
##### Swift
```swift
import EMMA_iOS

public class CustomInAppPlugin: EMMAInAppPlugin {
    
    public func getId() -> String {
        return "emma-plugin-custom"
    }
    
    public func show(_ nativeAd: EMMANativeAd) {
       // Process data
    }
    
    public func dismiss() {

    }
}
```
##### Objective-C
```objc
// CustomInAppPlugin.h
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

@interface CustomInAppPlugin:  NSObject<EMMAInAppPluginProtocol>
@end

// CustomInAppPlugin.m
#import <Foundation/Foundation.h>

#import "CustomInAppPlugin.h"

@implementation CustomInAppPlugin

- (void)dismiss {
    
}

- (NSString * _Nonnull)getId {
    return @"emma-plugin-custom";
}

- (void)show:(EMMANativeAd * _Nonnull)nativeAd {
    // Process data
}

@end
```
###

El método `show()` es el método principal ya que es el que lanza el SDK cuando recibe el NativeAd correspondiente al plugin, el SDK pasa al plugin el NativeAd con el contenido acorde a la plantilla marcada como plugin en el Dashboard. Con estos parámetros se puede crear cualquier formato de comunicación adaptado al contenido del NativeAd.

El método `dismiss()` es para ocultar el plugin automáticamente. De momento, el SDK no tiene incorporado esta funcionalidad, deja el control de ocultado al propio plugin.

El método `getId()` devuelve el identificador del plugin que corresponde con el `templateId` generado en la plantilla.

La clase `EMMAInAppPlugin` contiene varios métodos estáticos como `sendInAppImpression` y `sendInAppClick`. Al igual que en NativeAd, puedes enviar estas acciones. También puedes invocar al/los `inappMessageListener` con los métodos `invokeShownListeners`, `invokeCloseListeners` y `invokeHideListeners`.

Puedes consultar el ejemplo de plugin [aquí](https://github.com/EMMADevelopment/inapp-plugin-prism-ios).

### Integrar plugin

Para integrar un plugin es necesario añadirlo en el SDK después del inicio de sesión, para ello es necesario utilizar el método `addInAppPlugin`.

#### Tabs {.tabset}
##### Swift
```swift
 func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
      ....
      EMMA.startSession(with: configuration)
      EMMA.addInAppPlugin([CustomInAppPlugin()])
      return true
 }
```
##### Objective-C
```objc
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    [EMMALegacy startSessionWithConfiguration:configuration];
    ....
    [EMMALegacy addInAppPlugins:@[[CustomInAppPlugin new]]];
    
    return YES;
}
```
####

Una vez el SDK tenga el plugin simplemente hay que llamarlo en la parte de la app donde se requiera igual que si de un NativeAd se tratase.


#### Tabs {.tabset}
##### Swift
```swift
import UIKit

class HomeViewController: UIViewController {
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
	 let nativeAdRequest = EMMANativeAdRequest()
	 nativeAdRequest.templateId = "emma-plugin-custom"
	 EMMA.inAppMessage(request: nativeAdRequest)
   }
}
```
##### Objective-C
```objc
@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    EMMANativeAdRequest * nativeAdRequest = [EMMANativeAdRequest new];
    nativeAdRequest.templateId = @"emma-plugin-custom";
    [EMMALegacy inAppMessage: nativeAdRequest withDelegate:self];
}
@end
```

## Cómo integrar las campañas In-App

A continuación, tenemos un ejemplo completo de la integración de campañas In-App con EMMA:

#### Tabs {.tabset}
##### Swift
```swift
import UIKit
import EMMA_iOS

class InAppExampleViewController: UIViewController, EMMAInAppMessageDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getStartView()
        // You can add as many request delegates as you want
        EMMA.add(inAppDelegate: self)
        
        //Check if any adball is showing
        if EMMA.isAdBallShowing() {
            print("There is an adball floating arround")
        }
    }
    
    // MARK: - EMMA InApp Messages Requests
    
    func getStartView() {
        let startViewinAppRequest = EMMAInAppRequest(type: .Startview)
        // Optional. You can filter by label
        startViewinAppRequest?.label = "<LABEL>"
        /*
         By default Startview presents on UIApplication.shared.delegate?.window?.rootViewController
         You can customize this behavior uncommenting following line
        */
        //EMMA.setRootViewController(UIViewController!)
        EMMA.inAppMessage(startViewinAppRequest)
    }
    
    func getBanner() {
        let bannerRequest = EMMAInAppRequest(type: .Banner)
        EMMA.inAppMessage(bannerRequest)
    }
    
    func getAdBall() {
        let adballRequest = EMMAInAppRequest(type: .Adball)
        EMMA.inAppMessage(adballRequest)
    }
    
    func getDynamicTabBar() {
        // You must define your UITabBarController
        // Uncomment following line!
        // EMMA.setPromoTabBarController(UITabBarController!)
        
        // Sets default promo tab index if not defined in EMMA Platform
        EMMA.setPromoTabBarIndex(5)
        
        // Sets a tab bar item to be shown if not defined in EMMA Platform
        // EMMA.setPromoTabBarItem(UITabBarItem!)
        
        let dynamicTabBarRequest = EMMAInAppRequest(type: .PromoTab)
        EMMA.inAppMessage(dynamicTabBarRequest)
    }
    
    func getStrip() {
        let stripRequest = EMMAInAppRequest(type: .Strip)
        EMMA.inAppMessage(stripRequest)
    }
    
    
    // MARK: - EMMA InApp Message Delegate
    
    func onShown(_ campaign: EMMACampaign!) {
        guard let c = campaign else {
            print("Error getting campaign info")
            return
        }
        print("Shown campaign \(c)")
    }
    
    func onHide(_ campaign: EMMACampaign!) {
        guard let c = campaign else {
            print("Error getting campaign info")
            return
        }
        print("Hide campaign \(c)")
    }
    
    func onClose(_ campaign: EMMACampaign!) {
        guard let c = campaign else {
            print("Error getting campaign info")
            return
        }
        print("Closed campaign \(c)")
    }
}
```
##### Objective-C
```objc 
// InAppExampleViewController.h
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

@interface InAppExampleViewController : UIViewController<EMMAInAppMessageDelegate>
@end

// InAppExampleViewController.m
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "EMMA_iOS/EMMA_iOS.h"

#import "InAppExampleViewController.h"

@interface InAppExampleViewController ()

@end

@implementation InAppExampleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getStartView];
    [EMMALegacy addInAppDelegate:self];
    
    if ([EMMALegacy isAdBallShowing]) {
        NSLog(@"There is an adball floating arround");
    }
}


-(void) getStartView {
    /* By default Startview presents on UIApplication.shared.delegate?.window?.rootViewController
    You can customize this behavior uncommenting following line */
    //[EMMALegacy setRootViewController:(UIViewController *)]
    EMMAInAppRequest *startViewRequest = [[EMMAInAppRequest alloc] initWithType: Startview];
    [EMMALegacy inAppMessage:startViewRequest];
}

-(void) getBanner {
    EMMAInAppRequest *bannerRequest = [[EMMAInAppRequest alloc] initWithType:Banner];
    [EMMALegacy inAppMessage:bannerRequest];
}

-(void) getAdBall {
    EMMAInAppRequest * adballRequest = [[EMMAInAppRequest alloc] initWithType: Adball];
    [EMMALegacy inAppMessage:adballRequest];
}

-(void) getDynamicTarBar {
    // You must define your UITabBarController
    // Uncomment following line!
    //[EMMALegacy setPromoTabBarController:<#(UITabBarController *)#>];
    
    // Sets default promo tab index if not defined in EMMA Platform
    [EMMALegacy setPromoTabBarIndex:5];
    
    // Sets a tab bar item to be shown if not defined in EMMA Platform
    //[EMMALegacy setPromoTabBarItem:<#(UITabBarItem *)#>];
    
    EMMAInAppRequest * dynamicRequest = [[EMMAInAppRequest alloc] initWithType:PromoTab];
    [EMMALegacy inAppMessage:dynamicRequest];
}

-(void) getStrip {
    EMMAInAppRequest * stripRequest = [[EMMAInAppRequest alloc] initWithType: Strip];
    [EMMALegacy inAppMessage:stripRequest];
}

- (void)onClose:(EMMACampaign *)campaign {
    if (campaign) {
        NSLog(@"Closed campaign %@", campaign);
    }
}

- (void)onHide:(EMMACampaign *)campaign {
    if (campaign) {
        NSLog(@"Hide campaign %@", campaign);
    }
}

- (void)onShown:(EMMACampaign *)campaign {
    if (campaign) {
        NSLog(@"Shown campaign %@", campaign);
    }
}

@end
```

## Otras opciones de In-App Messaging

### EMMA Whitelist

Con esta funcionalidad podemos limitar las URLs que abrirá el SDK de EMMA, de modo que en las comunicaciones In-App, sólo se mostrarán en un Webview aquel contenido que empiece por alguna de las URLs que hayamos indicado en la whitelist.

Si no indicamos ninguna URL en la whitelist, cualquier URL está permitida.

Esta funcionalidad afectaría a los Push (Rich URL), Banners, StartViews, AdBalls y DynamicTabs.

Las comunicaciones (Banners, StartViews, AdBalls y DynamicTabs)  pueden cargar contenido externo a la app mediante un Webview y para los Banners, AdBalls y DynamicTabs podrían cargar imágenes externas que también serían controladas por la whitelist.

Para un Push con Rich URL, si ésta no cumple, no se abriría la Webview correspondiente, pero el push sí que llega a la aplicación, hay que tener en cuenta que si en vez de una URL se emplea un deeplink, el scheme del deeplink debe ser añadido a la whitelist para poder abrirlo.

#### Cómo se usa

Debemos llamar a este método después del `startSession` y antes de llamar a cualquier método relativo a las comunicaciones In-App.

#### Tabs {.tabset}
##### Swift
```swift
    func enableWhitelist() {
        // If set the SDK only allows communication with following domains
        EMMA.setWhitelist(["https://www.emma.io", "https://www.emmasolutions.net"])
    }
```
##### Objective-C
```objc
- (void) enableWhitelist {
   // If set the SDK only allows communication with following domains
   [EMMALegacy setWhitelist(@[@"https://www.emma.io", @"https://www.emmasolutions.net"]];
}
```

#### Ejemplos

Si mi whitelist es “http://mydomain.com”.

1. Subimos al dashboard de EMMA un Banner con Target URL https://mydomain.com.
	El Banner no se mostrará, deberíamos añadir a la whitelist https://mydomain.com.

2. Configuramos en el dashboard de EMMA una StartView con StartView URL http://www.mydomain.com.
	La StartView no se mostrará, deberíamos añadir a la whitelist http://www.mydomain.com.

3. Configuramos en el dashboard de EMMA un Banner con Target URL http://mydomain.com/my/url y SmartPhone Banner URL http://subdomain.mydomain.com/my/image.
	El Banner no se mostrará, la URL de la imagen no cumple con la whitelist, deberíamos añadir a la whitelist http://subdomain.mydomain.com.

4. Subimos al dashboard de EMMA un Banner con Target URL http://mydomain.com/my/url/.
	El Banner se mostrará porque la URL introducida en el campo Target URL empieza por el mismo protocolo y dominio que la URL de la whitelist.

5. Configuramos en el dashboard de EMMA una StartView con StartView URL http://mydomain.com/mypage.html&param=value.
	La StartView se mostrará porque la URL introducida en el campo StartView URL empieza por el mismo protocolo y dominio que la URL de la whitelist.

### Filtrar campañas por etiqueta

Si lo deseas, puedes pasar un String personalizado que etiquete la Campaña en caso de que utilices más de una Campaña del mismo tipo en tu app y necesites distinguirlas.

```swift
        let startViewinAppRequest = EMMAInAppRequest(type: .Startview)
        // Optional. You can filter by label
        startViewinAppRequest?.label = "<LABEL>"
```

### Recuperar parámetros de campaña

Antes de continuar, es necesario añadir los parámetros a la comunicación en el *dashboard* de EMMA para que el SDK los recoja y los ponga a disposición de la aplicación. [Para saber más](https://docs.emma.io/es/mensajes-inapp#propiedades).

#### Resto de formatos

Para los formatos StartView, AdBall, Strip y Banner, la obtención de los parámetros de campaña se realiza a través de los métodos proporcionados por el delegado `EMMAInAppMessageDelegate`: `onShown`, `onHide` y `onClose`.

Cada uno de estos métodos recibe como parámetro una instancia de `EMMACampaign`, a través de la cual es posible acceder a los parámetros de la campaña mediante la propiedad `campaign.params`.

Ejemplo de uso:

```swift
// Implementación de EMMAInAppMessageDelegate
class HomeViewController: UIViewController, EMMAInAppMessageDelegate {
	// ...

	func onShown(_ campaign: EMMACampaign!) {
	    if (campaign != nil && campaign.params != nil && campaign.params.count > 0) {
	        print(campaign.params)
	    }
	}
	
	func onClose(_ campaign: EMMACampaign!) {
        // Acceso a campaign.params si es necesario
    }
    
    func onHide(_ campaign: EMMACampaign!) {
        // Acceso a campaign.params si es necesario
    }
}
```

# Habilitar seguimiento publicitario con IDFA

Apple anunció que con iOS 14 se necesitaría el permiso explícito de los usuarios para rastrearlos o acceder al Identificador de anunciantes (IDFA). Eso se tradujo en que para vincular los datos del usuario o dispositivo recopilados deberás solicitar permiso a través del marco de AppTrackingTransparency. Para solicitar el permiso en la aplicación usa el siguiente método:

#### Tabs {.tabset}
##### Swift
```swift
if #available(iOS 14.0, *) {
	EMMA.requestTrackingWithIdfa()
}
```
##### Objective-C
```objc
if (@available(iOS 14, *)) {
	[EMMALegacy requestTrackingWithIdfa];
}
```
####

> Recuerda incluir una clave `NSUserTrackingUsageDescription` en el archivo `Info.plist`. Este mensaje debe explicar claramente por qué tu aplicación solicita permiso para rastrear la actividad del usuario, ya que es un requisito para cumplir con las políticas de privacidad de Apple. Para más información consulta la [documentación oficial de Apple](https://developer.apple.com/documentation/bundleresources/information-property-list/nsusertrackingusagedescription). 
{.is-warning}

# Deshabilitar seguimiento publicitario

EMMA permite inhabilitar el trackeo o seguimiento para aquellos usuarios que manifiesten este deseo.

Este método es la mejor forma de adaptarse a la nueva RGPD (Reglamento General de la Protección de Datos).

La comunicación del usuario para expresar esta opción de no seguimiento debe ser tratada por la app haciendo una llamada al siguiente método:

#### Tabs {.tabset}
##### Swift
```swift
public func disableUserTracking(deleteUserData: Bool) {
    /*
    EMMA.disableUserTracking shuts down all communication with EMMA servers
    If deleteUserData is true; EMMA will remove this device data from
    their servers. This is a unrecoverable action.
    */
    EMMA.disableUserTracking(deleteUserData)
}
```

##### Objective-C
```objc
- (void) disableUserTracking: (BOOL) deleteUserData {
    /*
    EMMA.disableUserTracking shuts down all communication with EMMA servers
    If deleteUserData is true; EMMA will remove this device data from
    their servers. This is a unrecoverable action.
    */
    [EMMALegacy disableUserTracking: deleteUserData];
}

```
####

En el caso de que se quiera volver a activar las comunicaciones del usuario, se puede utilizar este otro método:
#### Tabs {.tabset}
##### Swift
```swift
    public func enableUserTracking() {
        EMMA.enableUserTracking()
    }
```
##### Objective-C
```objc
- (void) enableUserTracking {
	[EMMALegacy enableUserTracking];
}
```
