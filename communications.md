---
title: Actualización Google Advertising ID
description: 
published: true
date: 2022-02-21T12:59:34.784Z
tags: 
editor: markdown
dateCreated: 2022-02-01T09:19:44.744Z
---

# Comunicado oficial de Google

Como parte de la actualización de los servicios de Google Play a finales de 2021, la identificación de publicidad se eliminará cuando un usuario opte por no personalizar mediante la identificación de publicidad en la configuración de Android. Cualquier intento de acceder al identificador recibirá una cadena de ceros en lugar del identificador. 

Esta implementación de Google Play afectará a las aplicaciones que se ejecutan en Android 12 a partir de finales de 2021 y se expandirá a las aplicaciones que se ejecutan en todos los dispositivos compatibles con Google Play a partir del 1 de abril de 2022. 

Además, las aplicaciones que actualicen su nivel de API de destino a 31 (Android 12) deberán declarar un permiso normal de servicios de Google Play en el archivo de manifiesto de la siguiente manera: 

```html
<uses-permission android:name="com.google.android.gms.permission.AD_ID"/>
```

Si quieres implementar ID de publicidad en tu app o necesitas saber más sobre los requisitos que cumplan con las políticas de Google Play, puedes obtener más información [aquí](https://).

# Actualización de EMMA SDK

> La actualización del SDK es importante, ya que EMMA se basa en este identificador para realizar un perfil de los dispositivos.
{.is-warning}

Pasos a seguir para actualizar el SDK y que esto no afecte a las nuevas instalaciones:

1. Actualizar el SDK a la versión 4.9.6 o 4.10.2. 
2. Comprobar que la librería play-services-identifier está en la versión 18.0.1 entre las dependencias de la app y la dependencia de EMMA en la versión 4.9.6 o 4.10.2.

![captura_de_pantalla_2022-02-01_a_las_9.35.41.png](/communication/captura_de_pantalla_2022-02-01_a_las_9.35.41.png)

Para las versiones que usan SDKs híbridos como: IONIC, Cordova, Flutter, React Native, etc la dependencia de EMMA está puesta a 4.9.+, simplemente hay que recompilar el proyecto para que vuelva a descargar Gradle la nueva versión de la dependencia y asegurarse de que las librerías estén en el proyecto.

# FAQ
**¿Que pasa si mi aplicación tiene el target 30 (Android 11) como método de compilación?**

En este caso el requisito del permiso no es obligatorio, ya que solo afecta a aquellas aplicaciones que tengan Android 31 como target de la aplicación.

Pero al igual que con cualquier versión de la aplicación, a partir del 1 de Abril de 2022,  si un usuario desactiva el compartir el Google Avertising ID desde la sección de Ajustes del dispositivo el identificador obtenido es 00000000-0000-0000-0000-000000000000. Por lo tanto, para evitar perfilados erróneos es recomendable actualizar la versión del SDK.

**¿Tengo que pedir este permiso al arrancar la app como pasa con iOS y el IDFA?**

No, es un permiso de instalación, al instalar la app el usuario ya acepta el permiso. No es necesario avisar al usuario dentro de la app.

**Tengo la aplicación con target 31 (Android 12) desde hace un tiempo ¿que pasa con los usuarios que no actualicen o tarden en hacerlo?**

A nivel de EMMA recogemos el GAID al instalar por lo tanto ya tenemos el identificador de estos usuarios, la única afectación que pueden tener es si borran la caché del dispositivo, en ese caso intentaría obtener el identificador de publicidad de nuevo y devolvería una cadena con ceros. Una vez instalen la nueva versión de la app con el SDK se obtendrá el perfil correcto del usuario.

**¿Hay alguna estimación de cuantos usuarios van a desactivar el GAID desde Ajustes?**

No hay una cifra concreta en este aspecto, pero si nos basamos en cuantos dispositivos han limitado el tracking del GAID, opción que lleva unos años activa, solo el 2-3% de los usuarios con un Android han seleccionado esta opción.

# Documentación oficial
- Comunicado oficial soporte [aquí](https://support.google.com/googleplay/android-developer/answer/6048248?hl=en).
- Documentación oficial técnica [aquí](https://developers.google.com/android/reference/com/google/android/gms/ads/identifier/AdvertisingIdClient.Info).
