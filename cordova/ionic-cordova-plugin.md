---
title: EMMA Cordova SDK
description: SDK de EMMA para IONIC/Cordova
published: true
date: 2024-12-12T11:29:29.070Z
tags: 
editor: markdown
dateCreated: 2024-04-24T11:42:38.055Z
---

# Instalación

Para añadir el plugin de EMMA al proyecto ejecuta este comando en la consola donde esté situado el proyecto:
```tsx
ionic cordova plugin add cordova-plugin-emma-sdk --variable ADD_PUSH=1
```
Una vez se haya completado la instalación, en el `src/app/app.component.ts` añade el siguiente código:

```tsx
import { Component } from "@angular/core";
import { AlertController, Platform } from "@ionic/angular";

declare var window: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private alertCtrl: AlertController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.initEMMA();
    });
  }

  initEMMA() {
    const EMMA = window.plugins.EMMA;

    const configuration= {
      sessionKey: '<SESSIONKEY>',
      debug: true
    };

    EMMA.startSession(configuration);
  } 
}
```

# Integración Android

## Push (Opcional)

- Crear el proyecto en `Firebase` para poder usar **FCM**. [https://firebase.google.com/docs/android/setup](https://firebase.google.com/docs/android/setup)
- Añadir el Sender Id y Server Key que nos proporciona Firebase en la configuración de la app dentro de EMMA. [https://docs.emma.io/es/configuracion#preferencias-de-android](https://docs.emma.io/es/configuracion#preferencias-de-android)

Para que la notificación recibida en Android tenga imagen tenemos que añadir en el config.xml cual será el

icono de la notificación usado

*config.xml*

```xml
<resource-file src="resources/android/notification/drawable-mdpi-notification.png" target="app/src/main/res/drawable-mdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-hdpi-notification.png" target="app/src/main/res/drawable-hdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xhdpi-notification.png" target="app/src/main/res/drawable-xhdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xxhdpi-notification.png" target="app/src/main/res/drawable-xxhdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xxxhdpi-notification.png" target="app/src/main/res/drawable-xxxhdpi/notification.png" />

```

Para activar el push es necesario añadir el fichero de configuración `google-services.json` del proyecto firebase al fichero `config.xml`

*config.xml*

```xml
<!-- Google services JSON from Firebase -->
<resource-file src="google-services.json" target="app/google-services.json" />
```

Para recibir el push es necesario añadir el siguiente servicio dentro del AndroidManifest de la aplicación para ello tenemos que añadir lo siguiente en el `config.xml`

*config.xml*

```xml
<config-file parent="/manifest/application" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
    <service android:enabled="true" android:exported="false" android:name="io.emma.android.push.EMMAFcmMessagingService">
        <intent-filter>
            <action android:name="com.google.firebase.MESSAGING_EVENT" />
        </intent-filter>
    </service>
</config-file>
```

Para inicializar el push en la App es necesario llamar al método startPush una vez recibido el evento deviceReady dentro del `app.component.ts`

## Atribución DeepLink y AppLink (Opcional)

Para integrar la atribución es necesario configurar el DeepLink y el AppLink en la app.


> {custom_scheme} debe remplazarse por el nombre de schema escogido. Normalmente el nombre escogido tiene que ver con el nombre de la app.

> {basic_deeplink_scheme} reemplazar por el schema básico de deeplink p.e emmaionic.

> {config_id} debe reemplazarse por el id del tag widget dentro del fichero config.xml p.e my.company.com.

Para que la app se abra cuando se ejecuta un DeepLink o AppLink es necesario añadir lo siguiente al config.xml:

- Añade la preferencia de apertura de la app en modo singleTask al config.xml

```xml
<preference name="AndroidLaunchMode" value="singleTask" />
```

- Añade la EMMADeepLinkActivity al config.xml

```xml
<config-file parent="/manifest/application" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
   <activity android:exported="true" android:name="io.emma.android.activities.EMMADeepLinkActivity" android:noHistory="true" android:theme="@android:style/Theme.NoDisplay">
     <intent-filter autoVerify="true">
       <action android:name="android.intent.action.VIEW" />
       <category android:name="android.intent.category.DEFAULT" />
       <category android:name="android.intent.category.BROWSABLE" />
       <data android:host="{custom_scheme}.powlink.io" android:scheme="https" />
     </intent-filter>
     <intent-filter>
       <action android:name="android.intent.action.VIEW" />
       <category android:name="android.intent.category.DEFAULT" />
       <category android:name="android.intent.category.BROWSABLE" />
       <data android:scheme="{basic_deeplink_scheme}" />
     </intent-filter>
   </activity>
	 <meta-data android:name="io.emma.DEEPLINK_OPEN_ACTIVITY" android:value="{config_id}.MainActivity" />
</config-file>
```

Una vez añadido esto en la configuración es necesario añadir el evento en el componente principal. En este método es donde se añade la lógica para procesar el link.

```tsx
this.platform.ready().then(() => {
      document.addEventListener('onDeepLink', (event) => {
      //process deeplink and applink
      });
...
```

**En el caso de que estés usando tu propio callback de deeplink**, ignora la parte anterior y añade los intent-filters de apertura a tu activity de configuración:

```xml
     <intent-filter autoVerify="true">
       <action android:name="android.intent.action.VIEW" />
       <category android:name="android.intent.category.DEFAULT" />
       <category android:name="android.intent.category.BROWSABLE" />
       <data android:host="{custom_scheme}.powlink.io" android:scheme="https" />
     </intent-filter>
     <intent-filter>
       <action android:name="android.intent.action.VIEW" />
       <category android:name="android.intent.category.DEFAULT" />
       <category android:name="android.intent.category.BROWSABLE" />
       <data android:scheme="{basic_deeplink_scheme}" />
     </intent-filter>
```

Añade el método EMMA.handleLink para procesar el link y enviar el click a EMMA.

```tsx
    document.addEventListener('myCustomCallback', (event) => {
      const url = event.url;
      EMMA.handleLink(url);
    });
```

## Localización (Opcional)

Para activar la localización es necesario añadir los siguientes permisos al config.xml

```xml
<config-file parent="/manifest" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
</config-file>
```

Una vez añadidos la localización se puede activar con el método `EMMA.trackLocation()`.

# Integración iOS

## Push (Opcional)

- Activar las capabilities de push en el proyecto
[https://developer.emma.io/es/ios/integracion-sdk#activar-push-capabilities](https://developer.emma.io/es/ios/integracion-sdk#activar-push-capabilities)
- Configurar el Push Auth Key en EMMA
[https://developer.emma.io/es/ios/integracion-sdk#push-auth-key](https://developer.emma.io/es/ios/integracion-sdk#push-auth-key)
- Configuración Rich Push
[https://developer.emma.io/es/ios/integracion-sdk#rich-push](https://developer.emma.io/es/ios/integracion-sdk#rich-push)

Para activar la funcionalidad de Push solo tendrás que llamar al método `EMMA.startPush()` en `onDeviceReady`.

## Atribución, Deeplink y Universal Link

Para configurar correctamente esta funcionalidad usaremos las mismas dependencias que la integración del Powlink en el SDK nativo: [https://developer.emma.io/es/ios/integracion-sdk#dependencias-1](https://developer.emma.io/es/ios/integracion-sdk#dependencias-1)

Una vez configuradas las dependencias es necesario añadir el evento en el componente principal.

```tsx
    document.addEventListener('onDeepLink', (event) => {
      const url = event.url;
    });
```

## SKAdNetwork 4.0

Para integrar SKAdNetwork consulta la documentación nativa [aquí](https://developer.emma.io/es/ios/integracion-sdk#integraci%C3%B3n-de-skadnetwork-40)

## Localización


Para usar la localización es necesario añadir los siguientes permisos en el archivo `Info.plist` de la App:

```xml
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
<key>NSLocationAlwaysUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
<key>NSLocationWhenInUseUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>

```

## Solicitar el IDFA

Para solicitar el IDFA es necesario añadir el siguiente permiso al `Info.plist`:

```xml
<key>NSUserTrackingUsageDescription</key>
	<string>Uso del indentificador para redes de terceros</string>

```

# Integración TS

## Inicio de sesión

Iniciamos la sesión utilizando el método startSession

```tsx
EMMA.startSession(configuration);
```

Los parámetros de configuración para el inicio de sesión:

| Parámetro | Tipo | Descripción |
| --- | --- | --- |
| sessionKey | string | Key para identificación en el inicio de sesión. |
| apiUrl | string (Optional) | Añade la url si se usa un proxy. |
| queueTime | int (Optional) | Tiempo para vaciar la cola de operaciones. |
| isDebug | boolean (Optional) | Activa/desactiva los logs en el SDK. |
| customPowlinkDomains | string[] (Optional) | Añade los dominios si la app usa dominios custom en EMMA (los subdominios que se configuran en preferencias de la app no son dominios custom). |
| customShortPowlinkDomains | string[] (Optional) | Añade los dominios cortos si la app usa dominios custom en EMMA (los subdominios que se configuran en preferencias de la app no son dominios custom.) |
| trackScreenEvents | boolean (Optional) | Activa/desactiva el seguimiento de pantallas automáticas. |

## Push

Añade el método startPush después del startSession

```tsx
    const pushOptions = {
      classToOpen: 'io.emma.cordova.exampleionic.MainActivity',
      iconResource: 'notification'
    };

    this.EMMA.startPush(pushOptions);
```

Parámetros de startPush:

| Parámetro | Tipo | Descripción |
| --- | --- | --- |
| classToOpen | string | Indica la activity de apertura en el caso de hacer click en la notificación, en RN lo normal es tener una solo activity principal por lo tanto añadimos está, para ello siempre la indicamos con el path absoluto {packageName}.MainActivity. |
| iconResource | string | El icono que contendrá la notificación en la status bar y en el menú de notificaciones. El SDK busca ese icono de la carpeta drawable o drawable-xxx, por lo tanto es importante que el nombre que se incluya en este parámetro coincida con el png de drawable. |
| color | string (Optional) | Color en formato hexadecimal. |
| channelId | string (Optional) | Si la aplicación usa un canal de notificaciones ya existente, sino creará uno nuevo. |
| channelName | string (Optional) | Añade el nombre del nuevo canal. |

> El uso del startPush es válido para Android e iOS, aunque los parámetros solo sean usados en Android ya que en iOS coge los valores por defecto de la App.
> 

## Eventos

EMMA permite el envio de eventos personalizados y de varios eventos por defecto, el evento por defecto `Apertura` se envia al iniciar sesión en el SDK, pero el evento login y registro son eventos que hay que añadir el código de la aplicación.

#### Eventos personalizados

Para realizar el envío de eventos uso el siguiente método:

```tsx
eventToken : EventParams ={
    eventRequest: "7b358954cf16bc2b7830bb5307f80f96",
    eventAttributes: {IONIC: "working"}

trackEvent(){
    this.EMMA.trackEvent(this.eventToken);
  }
```

#### Registro y login

Para realizar el registro y/o login es necesario enviar el id del usuario en la app (Customer ID):

```tsx
EMMA.registerUser({
      userId:"emma",
	    email:"email.prueba@emma.io"
    });
EMMA.loginUser({
      userId:"emma",
	    email:"email@emma.io"
    });
```

## Propiedades del usuario

EMMA permite el envío de propiedades clave/valor asociadas al dispositivo:

```tsx
trackUserExtraInfo(){
    EMMA.trackUserExtraInfo({TAG_1 : "EMMA_EXAMPLE_TAG_1", TAG_2: "EMMA_EXAMPLE_TAG_2"});
}
```

## Localización

Para medir la localización del usuario añade el siguiente método:

```tsx
EMMA.trackLocation();
```

La localización del usuario solo se recogerá una vez al iniciar sesión.

## Compras

El proceso de compra consta de varios métodos: `startOrder`, `addProduct` y `trackOrder`.

```tsx
EMMA.startOrder({
    orderId: "EMMA",
    totalPrice: 100,
    customerId: "EMMA", 
    //opcionales
    coupon: "coupon",
    extras: {IONIC: "Working"}
});

EMMA.addProduct({
    productId: "SDK",
    productName: "SDK",
    quantity: 1,
    price: 1,
    extras: {IONIC: "Working"}
});

EMMA.trackOrder();
```

En el caso de cancel una compra que ya se ha realizado usa el método `cancelOrder`.

```tsx
EMMA.cancelOrder();
```

## Mensajes in-app

EMMA permite el uso de varios formatos de comunicación:

- Adball
- Banner
- Startview
- Strip
- Native Ad

> El formato Banner solo está soportado para Android.

#### Formato NativeAd

El formato NativeAd devuelve un objeto JSON con la información del anuncio nativo. Es necesario implementar el callback `inAppResponse` para manejar correctamente la respuesta. Ejemplo de uso:

```tsx
const nativeAdType = INAPP_TYPE.NATIVE_AD
const templateId = 'template1'

EMMA.inAppMessage({  
  type: nativeAdType,  
  templateId: templateId,  
  inAppResponse: (nativeAdResponse: EMMANativeAd[]) => {  
    console.log('NativeAd', nativeAdResponse);
    // Process NativeAd response
    setNativeAds(nativeAdResponse);
  },  
});
```

Como ejemplo, se deja un posible modelo de datos para recibir el objeto devuelto en la respuesta del NativeAd:

```tsx
interface Fields {
  Subtitle: string;
  CTA: string;
  "Main picture": string;
  Title: string;
}

interface EMMANativeAd {
  id: number;
  templateId: string;
  cta: string;
  times: number;
  tag: string;
  showOn: string;
  fields: Fields;
}
```

#### Resto de formatos

Cualquier formato distinto al NativeAd no espera un resultado explícito, ya que se inyecta automáticamente en la vista. Ejemplo de uso:

```tsx
EMMA.inAppMessage({ type });
console.log(`InApp ${type} message`);
```

## Eventos in-app

Los formatos `Adball`, `Banner`, `Startview` y `Strip` al ser inyectados envían los eventos `Impresión` y `Clic` de forma automática cuando se realizan dichas acciones. En el caso del NativeAd al ser un formato gestionado por el desarrollador para notificar estas acciones hay que añadir estos métodos:

```tsx
EMMA.sendInAppImpression({
    campaignId: nativeAdResponse[0].id,
    type: IN_APP_TYPE.NATIVE_AD,
});

EMMA.sendInAppClick({
    campaignId: nativeAdResponse[0].id,
    type: IN_APP_TYPE.NATIVE_AD,
});
```

El método `openNativeAd` permite abrir un NativeAd según la configuración de este. Este método acepta tres parámetros, ya obtenidos en la respuesta del NativeAd: `campaignId`, `cta` y `showOn`. Si se usa este método no hace falta usar el `sendInAppClick` ya que está incluido internamente. Ejemplo de uso:

```tsx
EMMA.openNativeAd(
    nativeAdResponse[0].id, 
    nativeAdResponse[0].fields.CTA, 
    nativeAdResponse[0].showOn
);
```

## GDPR

Para controlar el uso de la ley de protección de datos EMMA pone ha disposición varios métodos para activar/desactivar el seguimiento de los usuarios.

```tsx
// Enable user tracking
EMMA.enableUserTracking();

//Disable user tracking
EMMA.disableUserTracking(false);
```

El método `disableUserTracking` pone a disposición un flag para eliminar el usuario permanente en el caso de que se deshabilite el seguimiento. Hay que tener en cuenta que al realizar está acción es irreversible y todos los datos asociados al usuario se perderán.

Para comprobar si el seguimiento está activado o desactivado usa el siguiente método:

```tsx
const isEnabled = EMMA.isUserTrackingEnabled();
```

## Envío del identificador de cliente

Si en la app no se usan los eventos login/registro para notificar el customerId puedes enviarlo con el siguiente método:

```tsx
EMMA.setCustomerId(customerId);
```

## Envío del token de push

En el caso de no usar los métodos de push del SDK de EMMA se puede enviar el token de push para realizar envios de notificaciones push, en el caso de que se añadan los métodos del SDK no hace falta usar este método.

```tsx
EMMA.sendPushToken(token);
```

## Solicitar el IDFA (solo iOS)

Para solicitar el identificador IDFA usa el siguiente método:

```tsx
EMMA.requestTrackingWithIdfa();
```

## Solicitar permiso de notificaciones (solo Android)

Desde Android 13 para recibir notificationes es necesario solicitar un permiso al usuario. A partir de la versión 1.3.0 se ha añadido el siguiente método para solicitar el permiso:

```tsx
EMMA.requestNotificationPermission();
```

# Referencia

- Proyecto de ejemplo: [https://github.com/EMMADevelopment/EMMAIonicExample](https://github.com/EMMADevelopment/EMMAIonicExample)
- Ejemplo de configuración: [https://github.com/EMMADevelopment/EMMAIonicExample/blob/master/config.xml](https://github.com/EMMADevelopment/EMMAIonicExample/blob/master/config.xml)
- Ejemplo de inicialización: [https://github.com/EMMADevelopment/EMMAIonicExample/blob/master/src/app/app.component.ts](https://github.com/EMMADevelopment/EMMAIonicExample/blob/master/src/app/app.component.ts)