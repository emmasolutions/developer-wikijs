---
title: Guía de integración rápida
description: Guía de integración rápida del SDK de EMMA con el plugin Cordova/IONIC
published: true
date: 2025-02-04T12:59:50.282Z
tags: ios, sdk, android, ionic, cordova, quick-start, quick-wins
editor: markdown
dateCreated: 2024-05-27T11:21:39.123Z
---

# Referencias

## Documentación

- [📖 Integración completa *Detalle para integrar el SDK de EMMA para IONIC/Cordova*](https://developer.emma.io/es/cordova/ionic-cordova-plugin)
{.links-list}

## Repositorios de ejemplo

- [💠 IONIC - Cordova *App de ejemplo en GitHub*](https://github.com/EMMADevelopment/EMMAIonicExample)
{.links-list}

# Descarga de EMMA

- Ejecuta el comando en la terminal del proyecto:

```
ionic cordova plugin add cordova-plugin-emma-sdk --variable ADD_PUSH=1
```

# Integración básica

- Crea el entorno de desarrollo para Android e iOS.

```bash
ionic cordova prepare android
```

```bash
ionic cordova prepare ios
```

- Asegúrate de obtener el Session Key de EMMA ([EMMA Key](https://docs.emma.io/es/configuracion#general-emma-key-y-api-key)).
- Para inicializar EMMA, podemos añadir en `src/app/app.component.ts`:

```javascript
initEMMA() {
   const EMMA = window.plugins.EMMA;

   const configuration= {
     sessionKey: '<SESSIONKEY>',
     debug: true
   };

   EMMA.startSession(configuration);
}
```

# Integración en Android

## Push

- Crear el proyecto en Firebase para poder usar [FCM](https://firebase.google.com/docs/android/setup).
- Añadir el Sender Id y Server Key que nos proporciona Firebase en la configuración de la app dentro de [EMMA](https://docs.emma.io/es/configuracion#preferencias-de-android).
- Añadir en `config.xml` cuál será el icono de la notificación:

```xml
<resource-file src="resources/android/notification/drawable-mdpi-notification.png" target="app/src/main/res/drawable-mdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-hdpi-notification.png" target="app/src/main/res/drawable-hdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xhdpi-notification.png" target="app/src/main/res/drawable-xhdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xxhdpi-notification.png" target="app/src/main/res/drawable-xxhdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xxxhdpi-notification.png" target="app/src/main/res/drawable-xxxhdpi/notification.png" />
```

- Añadir `google-services.json` del proyecto Firebase al fichero `config.xml`:

```xml
<!-- Google services JSON from Firebase -->
<resource-file src="google-services.json" target="app/google-services.json" />
```

- Por último, añadir lo siguiente en el `config.xml`:

```xml
<config-file parent="/manifest/application" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
    <service android:enabled="true" android:exported="false" android:name="io.emma.android.push.EMMAFcmMessagingService">
        <intent-filter>
            <action android:name="com.google.firebase.MESSAGING_EVENT" />
        </intent-filter>
    </service>
</config-file>
```

## Powlink y Deeplink

- `{custom_scheme}` debe remplazarse por el nombre de esquema escogido. Normalmente el nombre escogido tiene que ver con el nombre de la app.
- Reemplazar`{basic_deeplink_scheme}` por el schema básico de deeplink, p.e emmaionic.
- `{config_id}` debe reemplazarse por el id del tag widget dentro del fichero `config.xml`, p.e my.company.com.

Para que la app se abra cuando se ejecuta un DeepLink es necesario añadir lo siguiente al `config.xml`:

- La preferencia de apertura de la app en modo `singleTask` al `config.xml`.

```xml
<preference name="AndroidLaunchMode" value="singleTask" />
```

- `EMMADeepLinkActivity` al `config.xml`.

```xml
<config-file parent="/manifest/application" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
   <activity android:exported="true" android:name="io.emma.android.activities.EMMADeepLinkActivity" android:noHistory="true" android:theme="@android:style/Theme.NoDisplay">
     <intent-filter autoVerify="true">
       <action android:name="android.intent.action.VIEW" />
       <category android:name="android.intent.category.DEFAULT" />
       <category android:name="android.intent.category.BROWSABLE" />
       <data android:host="{custom_scheme}.powlink.io" android:scheme="https" />
     </intent-filter>
     <intent-filter>
       <action android:name="android.intent.action.VIEW" />
       <category android:name="android.intent.category.DEFAULT" />
       <category android:name="android.intent.category.BROWSABLE" />
       <data android:scheme="{basic_deeplink_scheme}" />
     </intent-filter>
   </activity>
     <meta-data android:name="io.emma.DEEPLINK_OPEN_ACTIVITY" android:value="{config_id}.MainActivity" />
</config-file>
```

# Integración en iOS

## Push

Podemos realizar su configuración a través de los siguientes enlaces:

- [Capabilities *Activar las capabilities de push en el proyecto*](https://developer.emma.io/es/ios/integracion-sdk#activar-push-capabilities)
- [Push Auth Key *Configurar el Push Auth Key en EMMA*](https://developer.emma.io/es/ios/integracion-sdk#push-auth-key)
- [Rich Push *Configuración Rich Push*](https://developer.emma.io/es/ios/integracion-sdk#rich-push)
{.links-list}

## Powlink y Deeplink

Para configurar correctamente esta funcionalidad usaremos las mismas [dependencias](https://developer.emma.io/es/ios/integracion-sdk#dependencias-1) que la integración del Powlink en el SDK nativo.

# Powlink

- Añade el evento en el componente principal:

```javascript
this.platform.ready().then(() => {
      document.addEventListener('onDeepLink', (event) => {
      //process deeplink and applink
      });
// ...
}
```

- Finalmente, añade el método `EMMA.handleLink` para procesar el link y enviar el click a EMMA:

```javascript
document.addEventListener('myCustomCallback', (event) => {
  const url = event.url;
  EMMA.handleLink(url);
});
```

# Notificaciones push

Para activar la funcionalidad de Push solo tendrás que llamar al método `EMMA.startPush()` en `onDeviceReady` y una vez que se haya hecho `startSession`.

```javascript
const pushOptions = {
  classToOpen: 'io.emma.cordova.exampleionic.MainActivity',
  iconResource: 'notification'
};

this.EMMA.startPush(pushOptions);
```

> `classToOpen` y `iconResource` son parámetros obligatorios.
{.is-info}

# Mensajes In-App

## NativeAd

Es necesario implementar el *callback* `inAppResponse` para manejar correctamente la respuesta.

```javascript
const nativeAdType = INAPP_TYPE.NATIVE_AD
const templateId = 'template1'

EMMA.inAppMessage({
  type: nativeAdType,
  templateId: templateId,
  inAppResponse: (nativeAdResponse: EMMANativeAd[]) => {
    console.log('NativeAd', nativeAdResponse);
    // Process NativeAd response
    setNativeAds(nativeAdResponse);
  },
});
```

## Startview

El StartView configurado se inyecta en la vista al realizar esta llamada.

```javascript
try {
  EMMA.inAppMessage({type:IN_APP_TYPE.STARTVIEW});
  console.log(`InApp ${type} message`);
} catch (err) {
  console.error('InApp message error', err);
}
```

## AdBall

El AdBall configurado se inyecta en la vista al realizar esta llamada.

```javascript
try {
  EMMA.inAppMessage({type:IN_APP_TYPE.ADBALL});
  console.log(`InApp ${type} message`);
} catch (err) {
  console.error('InApp message error', err);
}
```

## Banner

El Banner configurado se inyecta en la vista al realizar esta llamada.

```javascript
try {
  EMMA.inAppMessage({type:IN_APP_TYPE.BANNER});
  console.log(`InApp ${type} message`);
} catch (err) {
  console.error('InApp message error', err);
}
```

> El formato Banner solo está soportado para Android.
{.is-info}

## Strip

El Strip configurado se inyecta en la vista al realizar esta llamada.

```javascript
try {
  EMMA.inAppMessage({type:IN_APP_TYPE.STRIP});
  console.log(`InApp ${type} message`);
} catch (err) {
  console.error('InApp message error', err);
}
```

# Eventos

## Registro

```javascript
EMMA.registerUser({
    userId:"emma",
    email:"email.prueba@emma.io"
});
```

## Login

```javascript
EMMA.loginUser({
    userId:"emma",
    email:"email@emma.io"
});
```

## Evento personalizado

```javascript
eventToken : EventParams = {
    eventRequest: "7b358954cf16bc2b7830bb5307f80f96",
    eventAttributes: {IONIC: "working"}
}

trackEvent() {
    this.EMMA.trackEvent(this.eventToken);
}
```

## Evento de compra

Consta de los métodos `startOrder`, `addProduct`, `trackOrder` y `cancelOrder`.

```javascript
EMMA.startOrder({
    orderId: "EMMA",
    totalPrice: 100,
    customerId: "EMMA",
    //opcionales
    coupon: "coupon",
    extras: {IONIC: "Working"}
});

EMMA.addProduct({
    productId: "SDK",
    productName: "SDK",
    quantity: 1,
    price: 1,
    extras: {IONIC: "Working"}
});

EMMA.trackOrder();
EMMA.cancelOrder();
```

# Envío de etiquetas de usuario

Envía un elemento Clave/Valor como etiqueta de usuario de cara a obtener mejor segmentación en el filtrado `Users with tag`.

```javascript
trackUserExtraInfo(){
    this.EMMA.trackUserExtraInfo({TAG : "EMMA_EXAMPLE"});
}
```
