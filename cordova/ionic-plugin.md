---
title: EMMA Ionic Plugin
description: 
published: true
date: 2023-07-10T06:53:25.312Z
tags: 
editor: markdown
dateCreated: 2021-08-12T09:14:12.704Z
---

# Instalación
Para añadir el plugin al proyecto es necesario ejecutar el siguiente comando:
```bash
$ ionic cordova plugin add cordova-plugin-emma-sdk --variable ADD_PUSH=1
```
En el `src/app/app.component.ts` añade el siguiente código:

```typescript
declare var window: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private alertCtrl: AlertController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.initEMMA();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  initEMMA() {
    document.addEventListener('onDeepLink', (event: any) => {
      const url = event.url;
      EMMA.handleLink(url);
    });

    document.addEventListener('onDeviceId', (event) => {
      //event.deviceId
    });

    const EMMA = window.plugins.EMMA;

    const configuration = {
      sessionKey: '<emma key>',
      debug: true
    };

    EMMA.startSession(configuration);

    // Optional to track location
    // EMMA.trackLocation();

    // Enable push system
    /*
    const pushOptions = {
      classToOpen: 'io.emma.cordova.exampleionic.MainActivity',
      iconResource: 'notification'
    };

    EMMA.startPush(pushOptions);
    */
  }
}

```

Consulta la documentación para ver donde puedes contrar tu `EMMA Key` en la [página de documentación de EMMA](https://docs.emma.io/es/configuracion#general-emma-key)

# Integración específica de Android

## Push (Opcional)

### Dependencias
- Crear el proyecto en `Firebase` para poder usar **FCM**. https://firebase.google.com/docs/android/setup
- Añadir el Sender Id y Server Key que nos proporciona Firebase en la configuración de la app dentro de EMMA. https://docs.emma.io/es/configuracion#preferencias-de-android
---

Para que la notificación recibida en Android tenga imagen tenemos que añadir en el config.xml cual será el
icono de la notificación usado

*config.xml*
```xml
<resource-file src="resources/android/notification/drawable-mdpi-notification.png" target="app/src/main/res/drawable-mdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-hdpi-notification.png" target="app/src/main/res/drawable-hdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xhdpi-notification.png" target="app/src/main/res/drawable-xhdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xxhdpi-notification.png" target="app/src/main/res/drawable-xxhdpi/notification.png" />
<resource-file src="resources/android/notification/drawable-xxxhdpi-notification.png" target="app/src/main/res/drawable-xxxhdpi/notification.png" />

```

Para activar el push es necesario añadir el fichero de configuración `google-services.json` del proyecto firebase al fichero `config.xml`

*config.xml*

```xml
<!-- Google services JSON from Firebase -->
<resource-file src="google-services.json" target="app/google-services.json" />
```

Para recibir el push es necesario añadir el siguiente servicio dentro del AndroidManifest de la aplicación para ello tenemos que añadir lo siguiente en el `config.xml`

*config.xml*

```xml
        <config-file parent="/manifest/application" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
            <service android:enabled="true" android:exported="false" android:name="io.emma.android.push.EMMAFcmMessagingService">
                <intent-filter>
                    <action android:name="com.google.firebase.MESSAGING_EVENT" />
                </intent-filter>
            </service>
        </config-file>
```

Para inicializar el push en la App es necesario llamar al método startPush una vez recibido el evento
deviceReady dentro del `app.component.ts`.

## Atribución, DeepLink y AppLink (Opcional)

Para integrar la atribución es necesario configurar el DeepLink y el AppLink en la app.
Para que la app se abra cuando se ejecuta un DeepLink o AppLink es necesario añadir lo siguiente al `config.xml`

*config.xml*
```xml
				<config-file parent="./application/activity" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
            <intent-filter>
                <action android:name="android.intent.action.VIEW" />
                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.BROWSABLE" />
                <data android:scheme="${custom_scheme}" />
            </intent-filter>
            <intent-filter android:autoVerify="true">
                <action android:name="android.intent.action.VIEW" />
                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.BROWSABLE" />
                <data android:host="${custom_scheme}.powlink.io" android:scheme="https" />
            </intent-filter>
        </config-file>
```

> ${custom_scheme} debe remplazarse por el nombre de schema escogido. Normalmente el nombre
escogido tiene que ver con el nombre de la app.
{.is-warning}

Una vez añadido esto en la configuración es necesario añadir el evento en el componente principal. En este
método es donde se añade la lógica para procesar el link.

```typescript
	this.platform.ready().then(() => {
		document.addEventListener('onDeepLink', (event) => {
		//process deeplink and applink
		});
  ...
```

## Localización

Para activar la localización es necesario añadir los siguientes permisos al config.xml

```xml
        <config-file parent="/manifest" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
            <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
            <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
        </config-file>
```

Una vez añadidos la localización se puede activar con el método `EMMA.trackLocation()`.

# Integración iOS

## Push (Opcional)

###  Dependencias
- Activar las capabilities de push en el proyecto 
https://developer.emma.io/es/ios/integracion-sdk#activar-push-capabilities
- Configurar el Push Auth Key en EMMA 
https://developer.emma.io/es/ios/integracion-sdk#push-auth-key
---

Para activar la funcionalidad de Push solo tendrás que llamar al método `EMMA.startPush()` en `onDeviceReady`.

## Atribución, Deeplink y Universal Link

## SKAdNetwork 4.0

Para integrar SKAdNetwork consulta la documentación nativa [aquí](https://developer.emma.io/es/ios/integracion-sdk#integraci%C3%B3n-de-skadnetwork-40)

### Dependencias

Para configurar correctamente esta funcionalidad usaremos las mismas dependencias que la integración del Powlink en el SDK nativo: https://developer.emma.io/es/ios/integracion-sdk#dependencias-1

--- 

Una vez configuradas las dependencias es necesario añadir el evento en el componente principal. 

```typescript
    document.addEventListener('onDeepLink', (event: any) => {
      const url = event.url;
      EMMA.handleLink(url);
    });
```

# Referencia

- Proyecto de ejemplo: https://github.com/EMMADevelopment/EMMAIonicExample
- Ejemplo de configuración: https://github.com/EMMADevelopment/EMMAIonicExample/blob/master/config.xml
- Ejemplo de inicialización: https://github.com/EMMADevelopment/EMMAIonicExample/blob/master/src/app/app.component.ts
