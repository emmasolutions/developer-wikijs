---
title: Guía de inicio rápido
description: Guía de inicio rápido del SDK de EMMA con Flutter
published: true
date: 2025-02-16T21:41:33.139Z
tags: ios, android, quick-start, quick-wins, flutter
editor: markdown
dateCreated: 2024-05-27T11:23:18.540Z
---

# Referencias

## Documentación

- [📖 Integración completa *Detalle para integrar el SDK de EMMA con Flutter*](https://developer.emma.io/es/flutter/flutter_sdk)
{.links-list}

## Repositorios de ejemplo

- [📱 Flutter *App de ejemplo en GitHub*](https://github.com/EMMADevelopment/emma_flutter_sdk/tree/master/example)
{.links-list}

# Descarga de EMMA

- En el archivo `pubspec.yaml` añade la siguiente línea:

```dart
dependencies:
  emma_flutter_sdk: 1.6.0
```

- Ejecuta `flutter packages get` para instalar el SDK.

- Importa la librería en tu archivo Dart:

```dart
import 'package:emma_flutter_sdk/emma_flutter_sdk.dart';
```

# Integración básica

- Asegúrate de obtener el Session Key de EMMA ([EMMA Key](https://docs.emma.io/es/configuracion#general-emma-key-y-api-key)).
- Añade el siguiente código de ejemplo para inicializar el SDK de EMMA:

```dart
 @override
  void initState() {
    super.initState();
    initPlatformState();
    initEMMA()
  }
  
  final startSessionParams = StartSession(
    sessionKey: "<EMMAKey>",
    queueTime: 10, // Optional
    isDebug: true, // Optional
    customShortPowlinkDomains: ["emma.shortLink.mycompany.com"], // Optional
    customPowlinkDomains: ["emma.link.mycompany.com"], // Optional
    trackScreenEvents: false, // Optional
    familiesPolicyTreatment: (Platform.isAndroid) ? true : false, // Optional
  );

  Future<void> initEMMA() async {
    await EmmaFlutterSdk.shared
        .startSession(startSessionParams);
  }
```

# Integración en Android

Es importante que en `AndroidManifest.xml` la *Activity* `MainActivity` tenga como atributo `launchMode="singleTask"`:

```xml
<activity
    android:name=".MainActivity"
    android:label="@string/app_name"
    android:configChanges="keyboard|keyboardHidden|orientation|screenSize|uiMode"
    android:launchMode="singleTask"
    android:windowSoftInputMode="adjustResize">
```

## Push

- Añade el icono de notificación dentro de la carpeta `app/res/drawable`.

- Añade el servicio push de EMMA en el `AndroidManifest.xml` de la app de Android, dentro del tag `<application>`. Este servicio es el que monta y muestra la notificacion push:
  
```xml
<service
    android:name="io.emma.android.push.EMMAFcmMessagingService"
    android:enabled="true"
    android:exported="false">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT"/>
    </intent-filter>
</service>
```

- Indica el uso del plugin de `google-services` en `android/build.gradle`:

```groovy
buildscript {
    ....
    repositories {
        ....
        google()
    }
    dependencies {
       ...
        classpath 'com.google.gms:google-services:4.3.15'
    }
}
```

- Una vez el servicio esté incluido, añadimos las dependencias de push y aplicamos el plugin de `google-services` en el archivo `android/app/build.gradle`:

```groovy
dependencies {
    ....
    implementation 'com.google.firebase:firebase-messaging:23.2.1'
}
apply plugin: 'com.google.gms.google-services'
```

> No olvidar añadir el `google-services.json` obtenido en la consola de Firebase.{.is-warning}

> Puedes encontrar más información sobre el Push [aquí](https://developer.emma.io/es/android/integracion-sdk#integraci%C3%B3n-notificaciones-push).
{.is-info}


## Powlink y Deeplink

- Añadir la siguiente *Activity* al `AndroidManifest.xml` de la app, dentro de `<application>`:

```xml
<activity
          android:name="io.emma.android.activities.EMMADeepLinkActivity"
          android:noHistory="true"
          android:exported="true"
          android:theme="@android:style/Theme.NoDisplay">

          <intent-filter>
              <action android:name="android.intent.action.VIEW"/>

              <category android:name="android.intent.category.DEFAULT"/>
              <category android:name="android.intent.category.BROWSABLE"/>

              <data android:scheme="${DEEPLINK_SCHEME}"/>
          </intent-filter>

          <intent-filter android:autoVerify="true">
              <action android:name="android.intent.action.VIEW"/>

              <category android:name="android.intent.category.DEFAULT"/>
              <category android:name="android.intent.category.BROWSABLE"/>

              <data
                  android:host="${CUSTOM_POWLINK_DOMAIN}.powlink.io"
                  android:scheme="https"/>

              <data
                  android:host="${CUSTOM_SHORT_POWLINK_DOMAIN}.pwlnk.io"
                  android:scheme="https"/>

          </intent-filter>
      </activity>
```

- Reemplazar `${DEEPLINK_SCHEME}` por el esquema de deeplink de la app, por ejemplo: emmarn (de emmarn://). El esquema básico de deeplink se utiliza para realizar acciones desde comunicación In-App, también para utilizar la *landing* de redirección de deeplinks en AppTracker.

- Reemplazar `${CUSTOM_POWLINK_DOMAIN}` y `${CUSTOM_SHORT_POWLINK_DOMAIN}` por los dominios configurados en el dashboard de EMMA en la sección de preferencias de la app.

- Debajo de la *Activity*, en el mismo `AndroidManifest.xml`, hay que añadir también el siguiente `<meta-data>`:

```xml
<meta-data
    android:name="io.emma.DEEPLINK_OPEN_ACTIVITY"
    android:value="${PACKAGE_NAME}.MainActivity"/>
```

- Reemplazar `${PACKAGE_NAME}` por la ruta del fichero `MainActivity`, por ejemplo (com.example). Este meta-data indica al SDK de EMMA qué *Activity* tiene que abrir al recibir un deeplink, en el caso de los SDKs híbridos solo suele haber una *Activity*.

# Integración en iOS

## Push

Para integrar las notificaciones push en iOS hay que activar la `Capability > Push Notification`.

Para poder enriquecer las notificaciones con contenido multimedia consulta la [documentación nativa](https://developer.emma.io/es/ios/integracion-sdk#creando-la-notification-service-extension) para crear un nuevo servicio de notificaciones.

## Powlink y Deeplink

Para añadir el Powlink (Universal Link) dirigete a la sección `Capabilities` dentro del *target* de la app en Xcode y añade `Associate domains`. Seguidamente, añadiremos el Powlink con el formato `applinks:customsubdomain.powlink.io`.  

> Nótese que `customdomain` es el subdominio configurado en el Dashboard de EMMA.
{.is-warning}

![powlink_adding_capabilities](/react-native/powlink_adding_capabilities.png)

![added_powlink_capabilities](/react-native/added_powlink_capabilities.png)

En el caso del deeplink básico, es necesario añadir el esquema en la sección `Info` dentro del *target* de la app en Xcode y en `URL Types` añade una nueva entrada dónde puedes definir el esquema de la app.

![added_urltype_info.png](/react-native/added_urltype_info.png)

Para más información sobre los dominios de powlink y cómo configurar los dominios en el Dashboard, dirígete [aquí](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu).

# Powlink

Para obtener el deeplink o powlink una vez abierta la aplicación, añade el siguiente método:

```dart
// The DeepLink/Powlink is caught in this handler.
EmmaFlutterSdk.shared.setDeepLinkHandler((url) {
    print(url);
});
```


# Notificaciones Push

Añade el `startPush` en el archivo `main.dart` después de haber realizado el inicio de sesión con `startSession`:

```dart
Future<void> initEMMA() async {
  await EmmaFlutterSdk.shared
      .startSession('<SESSION_KEY>', debugEnabled: true);

  await EmmaFlutterSdk.shared.startPushSystem('notificationIcon');
}
```

> `notificationIcon` es un string que contendrá el nombre del icono de la carpeta `drawable` o `drawable-xxx`. Por tanto, es importante que el nombre que se incluya en este parámetro coincida con el png del drawable.
{.is-warning}

# Mensajes In-App

## NativeAd

Devuelve un objeto JSON que contiene el NativeAd.

```dart
EmmaFlutterSdk.shared
    .setReceivedNativeAdsHandler((List<EmmaNativeAd> nativeAds) {
    nativeAds.forEach((nativeAd) {
      print(nativeAd.toMap());
  });
});

var request = new EmmaInAppMessageRequest(InAppType.nativeAd);
request.batch = true;
request.templateId = "template1";
await EmmaFlutterSdk.shared.inAppMessage(request);
```

> El parámetro `batch` a `true` representa que se van descargar todos los NativeAds disponibles asociados a un *templateId*.
{.is-info}

## StartView

El StartView configurado se inyecta en la vista al realizar esta llamada.

```dart
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.startview));
```

## AdBall

El AdBall configurado se inyecta en la vista al realizar esta llamada.

```dart
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.adBall));
```

## Banner

El Banner configurado se inyecta en la vista al realizar esta llamada.

```dart
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.banner));
```

> El formato Banner solo está soportado en Android.
{.is-info}

## Strip

El Strip configurado se inyecta en la vista al realizar esta llamada.

```dart
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.strip));
```

# Eventos

## Registro

```dart
await EmmaFlutterSdk.shared
            .registerUser("userflutter#23243", "emma@flutter.dev");
```

## Login

```dart
await EmmaFlutterSdk.shared
            .loginUser("userflutter#23243", "emma@flutter.dev");
```

## Evento personalizado

Envía un evento personalizado con un token creado en el panel de Eventos de EMMA.

``` dart
await EmmaFlutterSdk.shared.trackEvent("2eb78caf404373625020285e92df446b");
```

## Evento de compra

Consta de los métodos `startOrder`, `addProduct`, `trackOrder` y `cancelOrder`.

```dart
// Start order
var order = new EmmaOrder("EMMA", 100, "1");
await EmmaFlutterSdk.shared.startOrder(order);
// Add products
var product = new EmmaProduct('SDK', 'SDK', 1, 100);
await EmmaFlutterSdk.shared.addProduct(product);
// Commit order
await EmmaFlutterSdk.shared.trackOrder();
//Cancel order
await EmmaFlutterSdk.shared.cancelOrder('EMMA');
```

# Envío de etiquetas de usuario

Envía un elemento Clave/Valor como etiqueta de usuario de cara a obtener mejor segmentación en el filtrado `Users with tag`.

```dart
await EmmaFlutterSdk.shared
        .trackExtraUserInfo({'TEST_TAG': 'TEST VALUE'});
```