---
title: EMMA Flutter SDK
description: SDK de EMMA para flutter
published: true
date: 2025-02-16T21:35:56.148Z
tags: 
editor: markdown
dateCreated: 2021-05-03T08:34:30.267Z
---

# Instalación

EMMA tiene unos bindings nativos para Flutter que conforman el SDK de EMMA para Flutter.

- [📦 EMMA Flutter SDK *Package publicado*](https://pub.dev/packages/emma_flutter_sdk)
{.links-list}

En el `pubspec.yaml` añade la siguiente línea:

```yaml
dependencies:
  emma_flutter_sdk: 1.6.0
```

Para importar la librería en el código Dart:

```dart
    import 'package:emma_flutter_sdk/emma_flutter_sdk.dart';
```

- [📱 App de ejemplo *Implementación completa de EMMA en Flutter*](https://github.com/EMMADevelopment/emma_flutter_sdk/tree/master/example)
{.links-list}

## Implementación actual

- [x] Session Tracking (Start Session)
- [x] Event Tracking
- [x] Update User Profile
- [x] Default events
  - [x] Login
  - [x] Register
  - [x] Purchase
- [x] Powlink Support
- [x] Push System Support
  - [x] Receive Push
  - [ ] Notification Color
  - [x] Mark push as opened
  - [x] Rich Push
- [ ] InApp Messages
	- [x] NativeAd
  - [x] StartView
  - [x] Banner
  - [x] AdBall
  - [x] Strip
  - [ ] Dynamic Tab
  - [ ] Coupon

# Integración Android

Es importante que en `AndroidManifest.xml` la *Activity* `MainActivity` tenga como atributo `launchMode="singleTask"`:

```xml
<activity
    android:name=".MainActivity"
    android:label="@string/app_name"
    android:configChanges="keyboard|keyboardHidden|orientation|screenSize|uiMode"
    android:launchMode="singleTask"
    android:windowSoftInputMode="adjustResize">
```

## Push

En Android debemos incluir el icono de la notificación como *drawable*.

1. Abrir Runner en Android Studio.
2. Poner los ficheros del icono en la carpeta `/app/res/drawable` del proyecto de Android Studio.

Para integrar el push hay que añadir el servicio push de EMMA en el `AndroidManifest.xml` de la app de Android, dentro del tag `<application>`. Este servicio es el que monta y muestra la notificacion push:

```xml
<service
    android:name="io.emma.android.push.EMMAFcmMessagingService"
    android:enabled="true"
    android:exported="false">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT"/>
    </intent-filter>
</service>
```

Indica el uso del plugin de `google-services` en `android/build.gradle`:

```groovy
buildscript {
    ....
    repositories {
        ....
        google()
    }
    dependencies {
       ...
        classpath 'com.google.gms:google-services:4.3.15'
    }
}
```

Una vez el servicio esté incluido añadimos las dependencias de push y aplicamos el plugin de `google-services` en el archivo `android/app/build.gradle`:

```groovy
dependencies {
    ....
    implementation 'com.google.firebase:firebase-messaging:21.0.1'
}

apply plugin: 'com.google.gms.google-services'
```

> No olvidar añadir el `google-services.json` obtenido en la consola de [Firebase](https://console.firebase.google.com/) en la ruta `android/app/`.{.is-warning}

> Puedes encontrar más información sobre el Push [aquí](https://developer.emma.io/es/android/integracion-sdk#integraci%C3%B3n-notificaciones-push).
{.is-info}

## Powlink y Deeplinking

Para utilizar el Powlink o Deeplinking en Android es necesario añadir la siguiente *Activity* al `AndroidManifest.xml` de la app, dentro de `<application>`:

```xml
  <activity
            android:name="io.emma.android.activities.EMMADeepLinkActivity"
            android:noHistory="true"
            android:exported="true"
            android:theme="@android:style/Theme.NoDisplay">

            <intent-filter>
                <action android:name="android.intent.action.VIEW"/>

                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.BROWSABLE"/>

                <data android:scheme="${DEEPLINK_SCHEME}"/>
            </intent-filter>

            <intent-filter android:autoVerify="true">
                <action android:name="android.intent.action.VIEW"/>

                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.BROWSABLE"/>

                <data
                    android:host="${CUSTOM_POWLINK_DOMAIN}.powlink.io"
                    android:scheme="https"/>

                <data
                    android:host="${CUSTOM_SHORT_POWLINK_DOMAIN}.pwlnk.io"
                    android:scheme="https"/>

            </intent-filter>
        </activity>
```

- Reemplazar `${DEEPLINK_SCHEME}` por el esquema de deeplink de la app, por ejemplo: emmarn (de emmarn://). El esquema básico de deeplink se utiliza para realizar acciones desde comunicación In-App, también para utilizar la *landing* de redirección de deeplinks en AppTracker.

- Reemplazar `${CUSTOM_POWLINK_DOMAIN}` y `${CUSTOM_SHORT_POWLINK_DOMAIN}` por los dominios configurados en el dashboard de EMMA en la sección de preferencias de la app.

Debajo de la *Activity*, en el mismo `AndroidManifest.xml`, hay que añadir también el siguiente `<meta-data>`:

```xml
<meta-data
    android:name="io.emma.DEEPLINK_OPEN_ACTIVITY"
    android:value="${PACKAGE_NAME}.MainActivity"/>

```

- Reemplazar `${PACKAGE_NAME}` por la ruta del fichero `MainActivity`, por ejemplo (com.example.MainActivity). Este meta-data indica al SDK de EMMA qué *Activity* tiene que abrir al recibir un deeplink, en el caso de los SDKs híbridos solo suele haber una *Activity*.

######
- [📄 Configurar Powlink y pwlnk *Más información sobre los dominios de powlink*](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu)
{.links-list}

# Integración iOS

Asegúrate al arrancar el proyecto que la dependencia de EMMA está instalada. Para ello, ejecuta `flutter build ipa`.

## Push

Para integrar las notificaciones push en iOS hay que activar la `Capability > Push Notification`

Para poder enriquecer las notificaciones con contenido multimedia consulta la [documentación nativa](https://developer.emma.io/es/ios/integracion-sdk#creando-la-notification-service-extension) para crear un nuevo servicio de notificaciones.

## Powlink y Deeplinking

Para añadir el Powlink (Universal Link) dirígete a la sección `Capabilities` dentro del *target* de la app en Xcode y añade `Associate domains`. Seguidamente, añadiremos el Powlink con el formato `applinks:customsubdomain.powlink.io`.  

> Nótese que `customdomain` es el subdominio configurado en el Dashboard de EMMA.
{.is-warning}

![powlink_adding_capabilities](/react-native/powlink_adding_capabilities.png)

![added_powlink_capabilities](/react-native/added_powlink_capabilities.png)

En el caso del deeplink básico, es necesario añadir el esquema en la sección `Info` dentro del *target* de la app en Xcode y en `URL Types` añade una nueva entrada dónde puedes definir el esquema de la app.

![added_urltype_info.png](/react-native/added_urltype_info.png)

Para más información sobre los dominios de powlink y cómo configurar los dominios en el Dashboard, dirígete [aquí](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu).

## Localización

Para usar la localización es necesario añadir los siguientes permisos en el archivo `Info.plist` de la App:

```xml
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
<key>NSLocationAlwaysUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
<key>NSLocationWhenInUseUsageDescription</key>
	<string>$(PRODUCT_NAME) needs location access</string>
```

## Solicitar el IDFA

Para solicitar el IDFA es necesario añadir el siguiente permiso al `Info.plist`:

```xml
<key>NSUserTrackingUsageDescription</key>
	<string>Uso del indentificador para redes de terceros</string>
```

# Integración en Dart

Los ejemplos de está sección se realizan sobre archivos `dart`.

Para importar el SDK en el código usa el siguiente *import*:

```dart
import 'package:emma_flutter_sdk/emma_flutter_sdk.dart';
```

## Inicio de sesión

En el archivo `main.dart` añadimos, por ejemplo:

```dart
 @override
  void initState() {
    super.initState();
    initPlatformState();
    initEMMA()
  }
  
  final startSessionParams = StartSession(
    sessionKey: "<EMMAKey>",
    queueTime: 10,
    isDebug: true,
    customShortPowlinkDomains: ["emma.shortLink.mycompany.com"],
    customPowlinkDomains: ["emma.link.mycompany.com"],
    trackScreenEvents: false,
    familiesPolicyTreatment: (Platform.isAndroid) ? true : false,
  );

  Future<void> initEMMA() async {
    await EmmaFlutterSdk.shared
        .startSession(startSessionParams);
  }
```

Parámetros de configuración para el inicio de sesión:

| Parámetro  |  Tipo | Descripción |
|---|---|---|
| *sessionKey*  | String  | [Clave de EMMA](https://docs.emma.io/es/configuracion#general-emma-key-y-api-key) para identificación en el inicio de sesión. |
| *apiUrl*  | String (Optional) | Añade la URL si se usa un proxy.   |
| *queueTime*  | Int (Optional)  | Tiempo para vaciar la cola de operaciones.  |
| *isDebug* | Boolean (Optional) | Activa/desactiva los logs en el SDK. |
| *customPowlinkDomains* | String (Optional) | Añade los dominios si la app usa dominios custom en EMMA (los subdominios que se configuran en preferencias de la app no son dominios custom). |
| *customShortPowlinkDomains* | String (Optional) |  Añade los dominios cortos si la app usa dominios custom en EMMA  (los subdominios que se configuran en preferencias de la app no son dominios custom).|
| *trackScreenEvents* | Boolean (Optional)| Activa/desactiva el seguimiento de pantallas automáticas. | 
| *familiesPolicyTreatment* | Boolean (Optional) (Android)| Activa/desactiva el cumplimiento de la política de familias asociada al programa "Designed for Families". Solo necesario para Android. |

## Push

Añade el `startPush` en el archivo `main.dart` después del `startSession`:

```dart
  Future<void> initEMMA() async {
    await EmmaFlutterSdk.shared
        .startSession('<SESSION_KEY>', debugEnabled: true);

    await EmmaFlutterSdk.shared.startPushSystem('icimage');
  }
```

Parámetros del `startPushSystem`:

| Parámetro | Tipo | Descripción |
|---|---|---|
| *notificationIcon* | String | El icono que contendrá la notificación en la status bar y en el menú de notificaciones. El SDK busca ese icono de la carpeta drawable o drawable-xxx. Por tanto, es importante que el nombre que se incluya en este parámetro coincida con el png de drawable. |
| *notificationChannelId* | String (Optional) | Si la aplicación usa un canal de notificaciones ya existente, sino creará uno nuevo. |
| *notificationChannel* | String (Optional) | Añade el nombre del nuevo canal.  |

> El uso del `startPushSystem` es válido para Android e iOS, aunque los parámetros solo sean usados en Android ya que en iOS coge los valores por defecto de la App.
{.is-info}

## Powlink y Deeplinking

Para obtener el deeplink o powlink una vez abierta la aplicación, añade el siguiente *handler* después de iniciar sesión:

```dart
    // The DeepLink/Powlink is caught in this handler.
    EmmaFlutterSdk.shared.setDeepLinkHandler((url) {
        print(url);
    });
```

## Eventos

EMMA permite el envío de eventos personalizados y de varios eventos por defecto. Por ejemplo, el evento por defecto `Apertura` se envía al iniciar sesión en el SDK. Sin embargo, el evento `login` y `registro` son eventos que hay que añadir.

### Eventos personalizados

Para realizar el envío de un evento personalizado con un token creado en el panel de Eventos de EMMA, empleamos el siguiente método:

```dart
await EmmaFlutterSdk.shared.trackEvent("2eb78caf404373625020285e92df446b");
```

### Registro y login

Para realizar el registro y/o login es necesario enviar el id del usuario en la app (Customer ID):

```dart
    await EmmaFlutterSdk.shared
                .loginUser("userflutter#23243", "emma@flutter.dev");
    

    await EmmaFlutterSdk.shared
                .registerUser("userflutter#23243", "emma@flutter.dev");
```

## Propiedades del usuario

EMMA permite el envío de propiedades clave/valor asociadas al dispositivo:

```dart
await EmmaFlutterSdk.shared
        .trackExtraUserInfo({'TEST_TAG': 'TEST VALUE'});
```

## Localización

Para medir la localización del usuario añade el siguiente método:

```dart
await EmmaFlutterSdk.shared.trackUserLocation();
```

La localización del usuario solo se recogerá una vez al iniciar la app.

## Compras

El proceso de compra consta de varios métodos: `startOrder`, `addProduct` y `trackOrder`.

```dart
  // Start order
  var order = new EmmaOrder("EMMA", 100, "1");
  await EmmaFlutterSdk.shared.startOrder(order);
  // Add products
  var product = new EmmaProduct('SDK', 'SDK', 1, 100);
  await EmmaFlutterSdk.shared.addProduct(product);
  // Commit order
  await EmmaFlutterSdk.shared.trackOrder();
```

En el caso de cancelar una compra que ya se ha realizado usa el método `cancelOrder`.

```dart
await EmmaFlutterSdk.shared.cancelOrder('EMMA');
```

## Mensajes In-App

EMMA permite el uso de varios formatos de comunicación:

- NativeAd
- StartView
- AdBall
- Banner
- Strip

> El formato Banner solo está soportado en Android.
{.is-info}

### NativeAd

El formato NativeAd devuelve un objeto JSON que contiene el NativeAd. Ejemplo de uso:

```dart
  EmmaFlutterSdk.shared
      .setReceivedNativeAdsHandler((List<EmmaNativeAd> nativeAds) {
      nativeAds.forEach((nativeAd) {
        print(nativeAd.toMap());
    });
  });

  var request = new EmmaInAppMessageRequest(InAppType.nativeAd);
  request.batch = true;
  request.templateId = "template1";
  await EmmaFlutterSdk.shared.inAppMessage(request);
```

### Resto de formatos

Cualquier formato que no sea NativeAd no espera un resultado ya que se inyecta automáticamente en la vista. Ejemplos de uso:

```dart
// StartView
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.startview));

// AdBall
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.adBall));

// Banner
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.banner));

// Strip
await EmmaFlutterSdk.shared.inAppMessage(new EmmaInAppMessageRequest(InAppType.strip));
```

## Eventos In-App

Los formatos `StartView`, `AdBall`, `Banner` y `Strip`, al ser inyectados, envían los eventos `Impresión` y `Clic` de forma automática cuando se realizan dichas acciones. En el caso del `NativeAd`, al ser un formato gestionado por el desarrollador, para notificar estas acciones hay que añadir estos métodos:

```dart
// When impression is done
await EmmaFlutterSdk.shared
        .sendInAppImpression(InAppType.nativeAd, nativeAd.id);
  
// When click is done
await EmmaFlutterSdk.shared
        .sendInAppClick(InAppType.nativeAd, nativeAd.id);
```

El método `openNativeAd` permite abrir un NativeAd según la configuración de este. Si se usa este método, no hace falta usar el `sendInAppClick` ya que está incluido internamente.

```dart
await EmmaFlutterSdk.shared.openNativeAd(nativeAd);
```

## Envío del idenficador de cliente

Si en la app no se usan los eventos login/registro para notificar el `customerId`, puedes enviarlo con el siguiente método:

```dart
await EmmaFlutterSdk.shared.setCustomerId("user#12345");
```

## Solicitar el IDFA (solo iOS)

Para solicitar el identificador IDFA, usa el siguiente método:

```dart
if (Platform.isIOS) {
 await EmmaFlutterSdk.shared.requestTrackingWithIdfa();
} 
```

## Solicitar permiso de notificaciones (solo Android)

Desde Android 13, para recibir notificationes es necesario solicitar un permiso al usuario. A partir de la versión 1.2.0 se ha añadido el siguiente método para solicitar el permiso:

```dart
if (Platform.isAndroid) {
  // Request notification permission for Android 13
 EmmaFlutterSdk.shared.setPermissionStatusHandler((status) {
   print('Notifications permission status: ' + status.toString());
 });
 
 await EmmaFlutterSdk.shared.requestNotificationsPermission();
}
```

## GDPR

Para controlar el uso de la ley de protección de datos, EMMA pone a disposición varios métodos para activar/desactivar el seguimiento de los usuarios.

```dart
// Enable user tracking
await EmmaFlutterSdk.shared.enableUserTracking();

//Disable user tracking
await EmmaFlutterSdk.shared.disableUserTracking(false);
```

El método `disableUserTracking` pone a disposición un *flag* para eliminar el usuario de forma permanente en el caso de que se deshabilite el seguimiento. 

> Hay que tener en cuenta que está acción es irreversible y todos los datos asociados al usuario se perderán.
{.is-warning}

Para comprobar si el seguimiento está activado o desactivado usa el siguiente método:

```dart
var isEnabled = await EmmaFlutterSdk.shared.isUserTrackingEnabled();
```
