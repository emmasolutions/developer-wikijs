---
title: Guía de integración rápida
description: Guía de integración rápida para el SDK de EMMA en la plataforma Android
published: true
date: 2025-02-03T12:46:51.591Z
tags: sdk, android, quick-start, quick-wins
editor: markdown
dateCreated: 2024-05-27T11:18:05.497Z
---

# Referencias

## Documentación

- [📖 Integración completa *Detalle para integrar el SDK de EMMA en la plataforma Android*](https://developer.emma.io/es/android/integracion-sdk)
{.links-list}

## Repositorios de ejemplo

- [☕️ Java - XML UI *App de ejemplo en GitHub*](https://github.com/EMMADevelopment/eMMaSampleApp-Android)
- [🚀 Kotlin - Jetpack Compose *App de ejemplo en GitHub*](https://github.com/EMMADevelopment/emma-android-app) 
{.links-list}

# Descarga de EMMA

- Añade al archivo `/app/build.gradle` o `/app/settings.gradle` el repositorio en el que se encuentra EMMA.

```
repositories {
  maven { url 'https://repo.emma.io/emma' }
}
```

- Añade la dependencia.

#### Tabs {.tabset}
##### Groovy DSL
```groovy
dependencies {
  	implementation 'io.emma:eMMaSDK:4.15.+'		    
}
```
##### Kotlin DSL
```kotlin
dependencies {
  	implementation("io.emma:eMMaSDK:4.15.+")		    
}
```
###

> La versión más reciente del SDK es la **4.15.2**. Consulta esta [página](https://developer.emma.io/es/android/sdk-upgrades) para detalles sobre las actualizaciones.
{.is-info}

# Integración básica

## Inicialización

- Asegúrate de obtener el Session Key de EMMA ([EMMA Key](https://docs.emma.io/es/configuracion#general-emma-key-y-api-key)).
- Inicializa EMMA en tu clase `Application`:

```kotlin
val configuration = EMMA.Configuration.Builder(this)
          .setSessionKey("<SESSIONKEY>")
          .setDebugActive(BuildConfig.DEBUG)
          .build()

EMMA.getInstance().startSession(configuration)
```

## Desactivar envío de pantallas

- Para desactivar el envío de pantallas, configura `trackScreenEvents` en `false` en la configuración. Por defecto es `true`.

```kotlin
val configuration = EMMA.Configuration.Builder(this)
    .setSessionKey("example0ikl98")
    .trackScreenEvents(false)
    .setDebugActive(BuildConfig.DEBUG)
    .build()
```

# Integración de Powlink

- Para soportar Powlink en Android, consulta la [guía de soporte para configurar Powlink](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu) en el dashboard de EMMA.
- Conociendo el subdominio que corresponde a tus POWLINKS, añadir una Activity nueva al `AndroidManifest.xml`:

```kotlin
<activity
     android:name="io.emma.android.activities.EMMADeepLinkActivity"
     android:noHistory="true"
     android:exported="true"
     android:theme="@android:style/Theme.NoDisplay">

     <intent-filter>
          <action android:name="android.intent.action.VIEW"/>

          <category android:name="android.intent.category.DEFAULT"/>
          <category android:name="android.intent.category.BROWSABLE"/>

          <data android:scheme="{YOUR_DEEPLINK_SCHEME}"/>
     </intent-filter>

     <intent-filter android:autoVerify="true">
         <action android:name="android.intent.action.VIEW"/>

         <category android:name="android.intent.category.DEFAULT"/>
         <category android:name="android.intent.category.BROWSABLE"/>

          <data
             android:host="subdomain.powlink.io"
             android:scheme="https"/>

          <data
             android:host="shortsubdomain.pwlnk.io"
             android:scheme="https"/>

     </intent-filter>
</activity>
```

Como se detalla en la sección [Rich push URL uso DeepLinking](https://developer.emma.io/es/android/integracion-sdk#rich-push-url-uso-deeplinking) de la documentación, debemos añadir un metadato dentro del tag `<application>` para gestionar la URL del POWLINK.

```kotlin
<meta-data
    android:name="io.emma.DEEPLINK_OPEN_ACTIVITY"
    android:value="com.your.package.CustomDeeplinkActivity"/>
```

# Notificaciones push

- Una vez tengas configuradas las dependencias necesarias, inicia el sistema push debajo del inicio de sesión en `Application`.

```kotlin
override fun onCreate() {
    super.onCreate();

    EMMA.Configuration configuration = new EMMA.Configuration.Builder(this)
            .setSessionKey("example0ikl98")
            .setDebugActive(BuildConfig.DEBUG)
            .build();

    EMMA.getInstance().startSession(configuration);

    val pushOpt = EMMAPushOptions.Builder(PushActivity::class.java, R.drawable.notification_icon)
                .setNotificationColor(ContextCompat.getColor(this, R.color.yellow))
                .setNotificationChannelName("Mi custom channel")
                .build()

    EMMA.getInstance().startPushSystem(pushOpt);
}
```

- Desde Android 13, las apps deben pedir permiso al usuario para enviar notificaciones. EMMA agregó el método `requestNotificationsPermission` en su SDK (versión 4.12+) para esto, que debe llamarse en un *Activity*. Si la app ya solicita el permiso o tiene una alerta personalizada, no es necesario usarlo. 

```kotlin
class HomeActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        EMMA.getInstance().requestNotificationsPermission();
    }
}
```

- Añadir el método `onNewIntent()` llamando a `EMMA.onNewNotification()`, que verificará si el usuario ha recibido una notificación cuando la app está abierta.

```kotlin
override fun onNewIntent(intent: Intent) {
    super.onNewIntent(intent);
    EMMA.getInstance().onNewNotification(this, intent, true);
}
```

# Mensajes In-App

## NativeAd

```kotlin
class NativeAdsActivity: BaseActivity(),
    EMMAInAppMessageInterface,
    EMMANativeAdInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun getNativeAd(templateId: String) {
        val nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        EMMA.getInstance().getInAppMessage(nativeAdRequest, this)
    }

    override fun onReceived(nativeAd: EMMANativeAd) {
        val content = nativeAd.nativeAdContent
        val title = content["Title"]?.fieldValue
        if (title !=  null) {
            print("Received NativeAd with Title: $title")
            EMMA.getInstance().sendInAppImpression(CommunicationTypes.NATIVE_AD, nativeAd)
        }
    }

    fun openNativeAd(nativeAd: EMMANativeAd) {
        EMMA.getInstance().openNativeAd(nativeAd)
    }

    fun sendNativeAdClick(nativeAd: EMMANativeAd) {
        EMMA.getInstance().sendInAppClick(CommunicationTypes.NATIVE_AD, nativeAd)
    }

    override fun onShown(campaign: EMMACampaign) {
        print("Invocado método onShown")
    }

    override fun onHide(campaign: EMMACampaign) {
        print("Invocado método onHide")
    }

    override fun onClose(campaign: EMMACampaign) {
        print("Invocado método onClose")
    }
}
```

En el caso de querer obtener todos los NativeAds activos bajo un mismo *templateId*, debemos:

- Implementar la interfaz `EMMABatchNativeAdInterface`.
- Realizar una *request* al igual que hemos hecho en el método `getNativeAd` anterior pero incluyendo `nativeAdRequest.isBatch = true`.
- Así, recibimos nuestro lote de NativeAds bajo el mismo *templateId* en `onBatchReceived`.

`EMMANativeAd` contiene todos los campos configurados en EMMA para esta plantilla de NativeAd, para obtenerlos se usará el siguiente método:

```kotlin
val content = nativeAd.nativeAdContent
val title = content["Title"]?.fieldValue
val image = content["Main picture"]?.fieldValue
val cta = content["CTA"]?.fieldValue
printSide(title, image, cta)
```

Una vez obtenidos todos los campos requeridos, ya se puede crear la vista para pintar este NativeAd en pantalla en función del diseño que se le quiera aplicar. Una vez pintado el NativeAd en pantalla es necesario llamar a este método para poder obtener las impresiones en el reporting:

```kotlin
EMMA.getInstance().sendInAppImpression(CommunicationTypes.NATIVE_AD, nativeAd)
```

## StartView

El StartView configurado se inyecta en la vista al realizar esta llamada.

```kotlin
fun getStartView() {
  val startViewRequest = EMMAInAppRequest(EMMACampaign.Type.STARTVIEW)
  EMMA.getInstance().getInAppMessage(startViewRequest)
}
```

## AdBall

El AdBall configurado se inyecta en la vista al realizar esta llamada.

```kotlin
fun getAdBall() {
  val adBallRequest = EMMAInAppRequest(EMMACampaign.Type.ADBALL)
	EMMA.getInstance().getInAppMessage(adBallRequest)
}
```

## Banner

El Banner configurado se inyecta en la vista al realizar esta llamada.

```kotlin
fun getBanner() {
	val bannerRequest = EMMAInAppRequest(EMMACampaign.Type.BANNER)
	EMMA.getInstance().getInAppMessage(bannerRequest);
}
```

## Strip

El Strip configurado se inyecta en la vista al realizar esta llamada.

```kotlin
fun getStrip() {
  val stripRequest = EMMAInAppRequest(EMMACampaign.Type.STRIP)
	EMMA.getInstance().getInAppMessage(stripRequest);
}
```

## Coupon

```kotlin
class CouponsActivity: BaseActivity(), EMMACouponsInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EMMA.getInstance().addCouponsCallback(this)
        getCoupons()
    }

    private fun getCoupons() {
        EMMA.getInstance().getInAppMessage(EMMAInAppRequest(EMMACampaign.Type.COUPON))
    }

    private fun getSingleCoupon() {
        val couponsRequest = EMMAInAppRequest(EMMACampaign.Type.COUPON)
        couponsRequest.inAppMessageId = "<COUPON_ID>"
        EMMA.getInstance().getInAppMessage(couponsRequest)
    }

    private fun redeemCoupon() {
        val redeemCouponRequest = EMMAInAppRequest(EMMACampaign.Type.REDEEM_COUPON)
        redeemCouponRequest.inAppMessageId = "<COUPON_ID>"
        EMMA.getInstance().getInAppMessage(redeemCouponRequest)
    }

    private fun cancelCoupon() {
        val cancelCouponRequest = EMMAInAppRequest(EMMACampaign.Type.CANCEL_COUPON)
        cancelCouponRequest.inAppMessageId = "<COUPON_ID>"
        EMMA.getInstance().getInAppMessage(cancelCouponRequest)
    }

    private fun couponValidRedeems() {
        val couponValidRedeems = EMMAInAppRequest(EMMACampaign.Type.COUPON_VALID_REDEEMS)
        couponValidRedeems.inAppMessageId = "<COUPON_ID>"
        EMMA.getInstance().getInAppMessage(couponValidRedeems)
    }

    override fun onCouponsReceived(coupons: List<EMMACoupon>) {
        coupons.let {
            // Show coupons
            coupons.forEach { coupon ->
                EMMA.getInstance().sendInAppImpression(CommunicationTypes.COUPON, coupon)
            }
        }
    }

    override fun onCouponsFailure() {
        print("An error has occurred obtaining coupons")
    }

    override fun onCouponRedemption(success: Boolean) {
        print("Coupon redemption success: $success")
    }

    override fun onCouponCancelled(success: Boolean) {
        print("Coupon cancelled success: $success")
    }

    override fun onCouponValidRedeemsReceived(numRedeems: Int) {
        print("Coupon redeems: $numRedeems")
    }
}
```

## Plugins personalizados

La clase principal del nuevo formato debe extender de la clase abstracta `EMMAInAppPlugin`.

```kotlin
class CustomInAppPlugin: EMMAInAppPlugin() {

	override fun getId(): String = "emma-plugin-custom"

	override fun show(context: Activity?, nativeAd: EMMANativeAd) {
		// Process data
	}

	override fun dismiss() { }
```

Añade el plugin en el SDK después del inicio de sesión con `addInAppPlugin`.

```kotlin
class App: Application {

  override fun onCreate() {
    super.onCreate()
    ....
    EMMA.getInstance().startSession(configuration)
    EMMA.getInstance().addInAppPlugin(CustomPlugin())
  }
}
```

Llama al plugin en la parte de la app que quieras como si de un NativeAd se tratase.

```kotlin
val request = EMMANativeAdRequest()
request.templateId = "emma-plugin-custom"
EMMA.getInstance().getInAppMessage(request)
```

# Eventos

## Registro

```kotlin
fun register() {
		EMMA.getInstance().registerUser("554234", "test@emma.io")
}
```

## Login

```kotlin
fun login() {
		EMMA.getInstance().loginUser("554234", "test@emma.io")
}
```

## Evento personalizado

Puedes consultar más información sobre los eventos personalizados [aquí](https://docs.emma.io/es/primeros-pasos/eventos#eventos-personalizados).

```kotlin
val eventRequest = EMMAEventRequest("f983d4bef8fc44dad43a1bb30dde9e3c")
//Optional: custom attributes
eventRequest.attributes = attributes
//Optional: request status listener
eventRequest.requestListener = requestListener
//Optional: cumtom id for request delegate
eventRequest.customId = customId

EMMA.getInstance().trackEvent(eventRequest)
```

## Evento de compra

Consta de los métodos `startOrder`, `addProduct`, `trackOrder` y `cancelOrder`.

```kotlin
fun trackTransaction() {
    EMMA.getInstance().startOrder("<ORDER_ID>","<CUSTOMER_ID>",10.0, "")
    EMMA.getInstance().addProduct("<PRODUCT_ID>", "<PRODUCT_NAME>", 1.0, 10.0)
    EMMA.getInstance().trackOrder()
    EMMA.getInstance().cancelOrder("<ORDER_ID>")
}
```

## Envío de etiquetas de usuario

En EMMA podemos enriquecer el perfil del usuario con información almacenada mediante Clave/Valor a la que llamamos tags de usuario de cara a obtener mejor segmentación en el filtrado `Users with tag`. Para ello, usamos el método `trackExtraUserInfo`.

```kotlin
val tags = ArrayMap<String, String>()
tags["MY_CUSTOM_TAG"] = "MY_CUSTOM_VALUE"
EMMA.getInstance().trackExtraUserInfo(tags)
```

## Seguimiento publicitario

### Deshabilitar

Para cumplir con la normativa RGPD y permitir a los usuarios desactivar el seguimiento, usa el siguiente método:

```kotlin
fun disableUserTracking(deleteUserData: Boolean) {
		EMMA.getInstance().disableUserTracking(deleteUserData)
}
```

> Si `deleteUserData` es `true`, los datos del usuario se eliminarán de los servidores de EMMA de forma irreversible.
{.is-warning}

### Rehabilitar

Si el usuario cambia de opinión y desea habilitar nuevamente el seguimiento, implementa:

```kotlin
fun enableUserTracking() {
		EMMA.getInstance().enableUserTracking()
}
```
