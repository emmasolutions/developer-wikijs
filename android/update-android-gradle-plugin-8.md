---
title: Compatibilidad con AGP 8
description: 
published: true
date: 2024-10-31T09:17:41.828Z
tags: 
editor: markdown
dateCreated: 2024-10-30T19:51:36.059Z
---

## 1. ¿Por qué es importante actualizar el SDK a v4.14.1 o superiores?

En el caso de la versión reciente de nuestro SDK (4.14.1), esta actualización es crítica debido a cambios en el sistema de compilación de Android que afectan directamente el funcionamiento de la app.

En SDKs anteriores a la versión 4.14.1 se ha detectado una incompatibilidad entre el **Android Gradle Plugin (AGP) versión 8** y una de las dependencias clave del SDK: **Retrofit 2**. Esta dependencia no es compatible, por defecto, con una opción de configuración avanzada del AGP 8 llamada `minifyEnabled`, que es utilizada para optimización de código. 

```build.gradle
buildTypes {
   release {
       minifyEnabled true
       proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
   }
}
```

> Por el momento, toda la batería de pruebas y reportes indica que el problema solo ocurre si el campo `minifyEnabled` está configurado como `true` en el archivo `build.gradle`, si el `minifyEnabled` está a `false` el problema no ocurre aunque desde EMMA recomendamos la actualización.
{.is-warning}


### Implicaciones específicas:

- **Fallo en la API de llamadas**: Al ejecutar la app en modo release con `minifyEnabled` a `true`, las llamadas a la API provocan el cierre inesperado de la aplicación. Esto significa que la app no podrá abrirse en modo producción.
- **Optimización agresiva con R8/Proguard fullMode**: El AGP 8 habilita por defecto un modo de optimización avanzada, lo que genera conflictos de compatibilidad con la configuración actual del SDK. Estas optimizaciones buscan mejorar el rendimiento, pero si el SDK no está adaptado, pueden afectar el comportamiento de la app.

## 2. Solución: Nueva versión 4.14.1 del SDK o superiores

Para evitar estos problemas, se ha lanzado la versión **4.14.1 del SDK** con una configuración específica de Proguard, conocida como **Consumer Proguard**. Esta configuración permite al SDK manejar de manera interna las reglas de optimización, evitando que el cliente tenga que gestionarlas manualmente y asegurando la compatibilidad.

### ¿Qué es el Consumer Proguard?

Consumer Proguard permite que el SDK defina sus propias reglas de optimización, integrándose sin problemas con la configuración de Proguard/R8 de la app del cliente. Al actualizar a la versión 4.14.1 del SDK:

- **Se resuelve el problema de cierre inesperado de la app.**
- **Se asegura la compatibilidad** con AGP 8 y sus optimizaciones, evitando posibles fallos de compatibilidad en futuras actualizaciones del Gradle.

## 3. ¿Cómo probar si la actualización funciona?

Para garantizar que la aplicación funcione correctamente con la nueva versión del SDK, recomendamos seguir estos pasos:

1. **Actualizar el SDK a la versión 4.14.1 o superiores.**
2. **Configurar `minifyEnabled` a `true`** para que las optimizaciones se ejecuten.
3. **Compilar el APK en modo release** para probar la app en condiciones de producción.
4. **Instalar y ejecutar el APK limpio** en un dispositivo físico o emulador para verificar que no existan cierres inesperados.

Este proceso garantiza que los cambios de compatibilidad en el SDK están funcionando correctamente y que la app es estable para los usuarios finales.

## 4. Resumen de los beneficios de actualizar el SDK

Actualizar el SDK a la versión 4.14.1 asegura:

- **Estabilidad de la app**: evita cierres inesperados y mejora la experiencia de los usuarios.
- **Compatibilidad** con nuevas versiones de Android y futuras actualizaciones del AGP.
- **Optimización automática**: con el Proguard adecuado configurado en el SDK, sin necesidad de ajustes adicionales.

## 5. Riesgos de no actualizar el SDK

Si se decide no actualizar el SDK, los riesgos incluyen:

- **Inestabilidad**: la aplicación puede cerrarse inesperadamente, lo que impacta en la satisfacción del usuario y aumenta las tasas de desinstalación.
- **Falta de compatibilidad** con futuras versiones del AGP o de Android, lo cual puede requerir ajustes importantes en el futuro.
- **Posibles problemas de rendimiento**: al no contar con optimizaciones adaptadas, la app podría consumir más recursos y funcionar de forma menos eficiente.






