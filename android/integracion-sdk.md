---
title: Integración SDK Android
description: Documentación para integrar el SDK de EMMA en la plataforma Android
published: true
date: 2025-02-20T10:24:11.159Z
tags: sdk, android
editor: markdown
dateCreated: 2020-11-30T15:08:49.561Z
---

# Descarga e integración básica

## Descarga EMMA Android SDK

### Gradle

- Añade al archivo `/app/build.gradle` o `/app/settings.gradle`, dependiendo de tu proyecto, el repositorio en el cual se encuentra EMMA.
	```groovy
	repositories {
      maven { url 'https://repo.emma.io/emma' }
	}
	``` 

- Añade la dependencia en el archivo `build.gradle`.

> Si estás usando la nueva versión 8 del Android Gradle Plugin y tienes la opción de minifyEnabled a true es necesario que actualices el SDK a la versión 4.14.1 o superiores para evitar un posible crash con una de nuestras dependencias. Si no es posible la actualización modifica las reglas de Proguard en la sección de más abajo.
{.is-warning}

#### Tabs {.tabset}
##### Groovy DSL
```groovy
dependencies {
  	implementation 'io.emma:eMMaSDK:4.15.+'		    
}
```
##### Kotlin DSL
```kotlin
dependencies {
  	implementation("io.emma:eMMaSDK:4.15.+")		    
}
```
####

- Si usas proguard consulta la [documentación sobre proguard](/es/android/integracion-sdk#proguard).

- Por último, compilar el proyecto con gradle.

> La versión más reciente del SDK es la **4.15.3**. Consulta esta [página](https://developer.emma.io/es/android/sdk-upgrades) para detalles sobre las actualizaciones.
{.is-info}

## Integración básica

### Dependencias
- **Obtener el `Session Key` de EMMA**
	Consulta la documentación para ver donde puedes contrar tu `EMMA Key` en la [página de documentación de EMMA](https://docs.emma.io/es/configuracion#general-emma-key)

### Permisos requeridos por el SDK

El SDK contiene por defecto los siguientes permisos obligatorios. Estos permisos **NO** se tienen que añadir en el `AndroidManifest.xml` de la aplicación, ya que es el mismo SDK quien los añade:

#### Tabs {.tabset}
##### AndroidManifest.xml

```xml
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
<uses-permission android:name="android.permission.VIBRATE"/>
<uses-permission android:name="com.google.android.gms.permission.AD_ID"/>
```
####

Si quieres habilitar la localización, tienes que añadir los siguientes permisos al `AndroidManifest.xml` de tu aplicación:

#### Tabs {.tabset}
##### AndroidManifest.xml

```xml
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
```


### Huawei

Si la aplicación se va a subir a AppGallery o va a ser ejecutada en dispositivos compatibles con HMS, es necesario añadir la conexión con Huawei y la librería para obtener un identificador único para el dispositivo.

En el `build.gradle` del projecto añade el plugin de Huawei.

#### Tabs {.tabset}
##### build.gradle  
```groovy
      buildscript { 
       repositories { 
          google()`   
          jcenter() 
          maven { url 'http://developer.huawei.com/repo/' }
       } 
       dependencies {  
          classpath 'com.android.tools.build:gradle:7.4.1' 
          classpath 'com.huawei.agconnect:agcp:1.6.2.300' 
       }
      }
      allprojects { 
       repositories { 
          google()
          jcenter()
          maven {url 'https://developer.huawei.com/repo/'} 
       }  
      }
```
####

En el `build.gradle` de app, añade la siguiente dependencia y aplica el plugin para recoger la información de configuración de la app en Huawei.

#### Tabs {.tabset}
##### build.gradle  
```groovy
        dependencies {  
     // other dependencies  
     implementation 'com.huawei.hms:ads-identifier:3.4.62.300'    
    }
    apply plugin: 'com.huawei.agconnect'
```


#### Obtener credenciales

Para obtener las credenciales primero hay que crear una cuenta en [developer.huawei.com](https://developer.huawei.com/consumer/en/console) y convertirse en desarrollador.

Una vez creada una cuenta hay que añadir una aplicación en *Huawei AppGallery Connect*. La app se crea en modo *draft*, para hacer la integración de push es su cliente.

Una vez creada la app ya podemos activar el Push Kit (esto es opcional solo si se requiere usar notificaciones push) en la sección *develop*. Dentro de *Project Settings* aparecerá la información referente a la app.

![huawei_push_developers.png](/android/huawei_push_developers.png)

Es necesario añadir el *fingerprint* del certificado que se usa para la firma de la app.

Descargamos el `agconnect-services.json` y lo añadimos en `/app`. De esta forma el servicio de HMS podrá autenticar la app.

Añadimos el *appId* y *appSecret* en la sección de preferencias de la app en el dashboard de EMMA.

![huawei_push_app_preferences_emma.png](/android/huawei_push_app_preferences_emma.png)


### Inicializar la librería

En tu clase `Application`, añade lo siguiente:

#### Tabs {.tabset}
##### Kotlin
```kotlin
import android.app.Application
import io.emma.android.EMMA

class ExampleApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        val configuration = EMMA.Configuration.Builder(this)
            .setSessionKey("example0ikl98")
            .setDebugActive(BuildConfig.DEBUG)
            .build()

        EMMA.getInstance().startSession(configuration)
    }
}
```
##### Java
```java
import android.app.Application;
import io.emma.android.EMMA;

public class ExampleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        
        EMMA.Configuration configuration = new EMMA.Configuration.Builder(this)
                .setSessionKey("example0ikl98")
                .setDebugActive(BuildConfig.DEBUG)
                .build();

        EMMA.getInstance().startSession(configuration);
    }
}
```

### Desactivar envío de pantallas

El envio de pantallas está activo por defecto en el SDK de EMMA. Para desactivarlo usa la siguiente configuración.

```kotlin
import android.app.Application
import io.emma.android.EMMA

class ExampleApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        val configuration = EMMA.Configuration.Builder(this)
            .setSessionKey("example0ikl98")
            .trackScreenEvents(false)
            .setDebugActive(BuildConfig.DEBUG)
            .build()

        EMMA.getInstance().startSession(configuration)
    }
}
```

### Aplazar el inicio del SDK (Opcional)

En lugar de hacerlo desde tu clase `Application`, puedes aplazar la inicialización del SDK llamando a `startSession` desde una clase `Activity`.

> Si la aplicación invoca `startSession` desde una clase `Activity`, es necesario proporcionar el contexto de dicha actividad al SDK con `setCurrentActivity()`.
{.is-warning}

Ejemplo de uso:

```kotlin
class ExampleActivity : ComponentActivity() {

    override fun onCreate() {
        super.onCreate()

        val configuration = EMMA.Configuration.Builder(this)
            .setSessionKey("example0ikl98")
            .setDebugActive(BuildConfig.DEBUG)
            .build()

        EMMA.getInstance().startSession(
             configuration,
             EMMASessionStartListener {
                 EMMA.getInstance().setCurrentActivity(this) 
             }
        )
    }
}
```

## Actualizaciones de versiones anteriores

Si se está actualizando de una versión anterior del SDK, consulta antes esta [página](https://developer.emma.io/es/android/sdk-upgrades) para ver posibles cambios de implementación.

# Integración Acquisition

## Integración del POWLINK

### Dependencias
- **Configurar sub-dominio para el POWLINK**

	Para soportar Powlink en Android, consulta la [guia de soporte para configurar Powlink](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu) en el dashboard de EMMA.


### Añadir Activity para procesar Deeplinks
Conociendo el subdominio que corresponde a tus POWLINKS, debes añadir una activity nueva al `AndroidManifest.xml` de tu aplicación tal y como indicamos a continuación. Esta actividad es la que se tiene que utilizar también para el deeplink, como explicamos más abajo.

#### Tabs {.tabset}
##### AndroidManifest.xml
```xml
<activity
     android:name="io.emma.android.activities.EMMADeepLinkActivity"
     android:noHistory="true"
     android:exported="true"
     android:theme="@android:style/Theme.NoDisplay">

     <intent-filter>
          <action android:name="android.intent.action.VIEW"/>

          <category android:name="android.intent.category.DEFAULT"/>
          <category android:name="android.intent.category.BROWSABLE"/>

          <data android:scheme="{YOUR_DEEPLINK_SCHEME}"/>
     </intent-filter>

     <intent-filter android:autoVerify="true">
         <action android:name="android.intent.action.VIEW"/>

         <category android:name="android.intent.category.DEFAULT"/>
         <category android:name="android.intent.category.BROWSABLE"/>

          <data
             android:host="subdomain.powlink.io"
             android:scheme="https"/>

          <data
             android:host="shortsubdomain.pwlnk.io"
             android:scheme="https"/>

     </intent-filter>
</activity>
```
###

Como se detalla más adelante en la sección [Rich push URL uso DeepLinking](https://developer.emma.io/es/android/integracion-sdk#rich-push-url-uso-deeplinking), es necesario añadir un metadato dentro del tag `<application>` que especifique la actividad encargada de gestionar la URL del POWLINK.

#### Tabs {.tabset}
##### AndroidManifest.xml
```xml
<meta-data
    android:name="io.emma.DEEPLINK_OPEN_ACTIVITY"
    android:value="com.your.package.CustomDeeplinkActivity"/>
```
###

Como decimos, en el apartado [Rich push URL uso DeepLinking](https://developer.emma.io/es/android/integracion-sdk#rich-push-url-uso-deeplinking) se muestra cómo puedes manejar los POWLINKS o Deeplinks recibidos para procesarlos y mostrar la parte correspondiente de tu aplicación.

## POWLINK con dominio propio
Si estas usando un tracker con un dominio que no es el de EMMA (*.powlink.io* o *.pwlnk.io*), es necesario añadir el dominio al iniciar la librería.

#### Tabs {.tabset}
##### Kotlin
```kotlin
val configuration = EMMA.Configuration.Builder(this)
        .setSessionKey("example0ikl98")
        .setDebugActive(BuildConfig.DEBUG)
        .setShortPowlinkDomains("emma.link.mycompany.com")
        .setPowlinkDomains("emma.link.mycompany.com")
        .build()

 EMMA.getInstance().startSession(configuration)
```
###

# Integración Notificaciones Push

> Para diferenciar una notificación de nuestro sistema de push respecto a otros sistemas, el payload enviado por EMMA contiene un flag denominado “eMMa”. 
{.is-info}

EMMA ofrece un completo sistema de envío y reporting de Notificaciones Push fácil de integrar usando Firebase Cloud Messaging (FCM) en Android. 

> Obtén en primer lugar tu propio Sender ID y Server Key para FCM como se específica en este [artículo](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#habilita-fcm-android-para-las-notificaciones-push) y establece estos parámetros para la app en su cuenta de EMMA. 
{.is-danger}

Para integrar FCM en tu app sigue los siguientes pasos:

-   Hay que añadir el siguiente ***service*** al `AndroidManifest.xml`:  
   
#### Tabs {.tabset}
##### AndroidManifest.xml
```xml
<service  
    android:name="io.emma.android.push.EMMAFcmMessagingService"
    android:enabled="true"
    android:exported="false"> 
    <intent-filter>
    <action android:name="com.google.firebase.MESSAGING_EVENT"/>
    </intent-filter>  
</service>
```
###

-   Añadimos la siguiente dependencia al `build.gradle` situado en la raíz del proyecto:  

#### Tabs {.tabset}
##### Groovy DSL
```groovy
// build.gradle file
buildscript {
    // ...
    dependencies {
    // ...  
    classpath 'com.google.gms:google-services:4.3.10' // google-services plugin`  
    }
} 
allprojects {
    // ...  
    repositories {  
    // ...
    google() // Google's Maven repository`  
    }  
}
```
##### Kotlin DSL
```kotlin
// <project>/build.gradle.kts file
plugins {
    // Add the dependency for the Google services Gradle plugin
    id("com.google.gms.google-services") version "4.3.10" apply false
}
    
// <project>/<app-module>/build.gradle.kts file
plugins {
    // Add the Google services Gradle plugin
    id("com.google.gms.google-services")
}
```
###

- En el `build.gradle` de la app añadimos la dependencia de Firebase:

#### Tabs {.tabset}
##### Groovy DSL
```groovy
apply plugin: 'com.android.application'

android {
    // ...
}

dependencies {
    //  
    // ...
    implementation 'io.emma:eMMaSDK:4.12.+'
    implementation 'com.google.firebase:firebase-messaging:23.1.0'
}
// ADD THIS AT THE BOTTOM
apply plugin: 'com.google.gms.google-services'
```
##### Kotlin DSL
```kotlin
dependencies {
    // ...
    implementation("com.google.firebase:firebase-messaging:24.0.1")
}
```
####

  -   Añadir el `google-services.json` en del directorio de `/app` dentro del proyecto. Puedes obtener [aquí](https://support.google.com/firebase/answer/7015592?hl=en#zippy=%2Cin-this-article) más información para obtener el fichero. Sin este fichero las notificaciones push no van a funcionar, es obligatorio añadirlo.
  -   En el caso de usar una ***server key “Legacy”***, todavía es válida y soportada por Google, aunque lo recomendable es usar la ***FCM Server Key***.

- Inicie el sistema de push debajo del inicio de sesión en `Application`:

  #### Tabs {.tabset}
  ##### Kotlin
  ``` kotlin
  override fun onCreate() {
      super.onCreate();

      EMMA.Configuration configuration = new EMMA.Configuration.Builder(this)
              .setSessionKey("example0ikl98")
              .setDebugActive(BuildConfig.DEBUG)
              .build();

      EMMA.getInstance().startSession(configuration);

      val pushOpt = EMMAPushOptions.Builder(PushActivity::class.java, R.drawable.notification_icon)
                  .setNotificationColor(ContextCompat.getColor(this, R.color.yellow))
                  .setNotificationChannelName("Mi custom channel")
                  .build()

      EMMA.getInstance().startPushSystem(pushOpt);
  }
  ```
  ###

  - **MyActivity.class**: Actividad que desea que se abra cuando se recibe el Push.

  - **R.drawable.icon**: (Opcional) El *resource id* de la imagen que se desea mostrar con la notificación Push. Si no se especifica ningún icono, el Push utilizará el icon de la app.

  - **customDialog**: (Opcional) Establecerlo para asegurar que se desea utilizar su Custom Dialog.


- Desde **Android 13** para recibir notificaciones es necesario solicitar un permiso al usuario. Para ello, EMMA ha añadido un método al SDK disponible en la versión **4.12 o superiores**. Para el correcto funcionamiento este método tiene que ser llamado en un Activity. Si la app ya cuenta con una alerta personalizada o ya integra la solicitud del permiso por defecto no hace falta usar este método:

  #### Tabs {.tabset}
  ##### Kotlin
  ``` kotlin
  class HomeActivity : Activity() {
      override fun onCreate(savedInstanceState: Bundle?) {
          super.onCreate(savedInstanceState);
          EMMA.getInstance().requestNotificationsPermission();
      }
  }
  ```
  ###


- Añadir el método `onNewIntent()` llamando a `EMMA.onNewNotification()`, que verificará si el usuario ha recibido una notificación cuando la app está abierta.


  #### Tabs {.tabset}
  ##### Kotlin
  ``` kotlin
  override fun onNewIntent(intent: Intent) {
      super.onNewIntent(intent);
      EMMA.getInstance().onNewNotification(this, intent, true);
  }
  ```
  ###

  > El boolean que tiene como parámetro el método `onNewNotification` comprueba que el push contiene `richPushUrl` en el caso de estar a `true`.
  {.is-info}




- Para comprobar el `richPushUrl` cuando se abre la app desde la notificación, añade en la *Activity* de push lo siguiente:

  #### Tabs {.tabset}
  ##### Kotlin
  ``` kotlin
  class HomeActivity : Activity() {
      override fun onCreate(savedInstanceState: Bundle?) {
          super.onCreate(savedInstanceState);
          EMMA.getInstance().checkForRichPushUrl();
      }
  }
  ```
  ###

  > El `checkForRichPushUrl` también se puede utilizar en cualquier parte de la app si se prefiere.
  {.is-info}

- Si tu aplicación se basa en un WebView, tienes que añadir el `checkForRichPushUrl` en el siguiente método:

  #### Tabs {.tabset}
  ##### Kotlin
  ``` kotlin
  webView.webViewClient = object : WebViewClient() {
      ...
       override fun onPageFinished(view: WebView, url: String) {
          EMMA.getInstance().checkForRichPushUrl();
      }
  }
  ```
  ###

- Opcionalmente, si deseas controlar qué se recibe de un Push, su Activity debe implementar la interfaz `EMMANotificationInterface` y el método `onPushOpen` que será llamado cuando el usuario abra la notificación.

  #### Tabs {.tabset}
  ##### Kotlin
  ``` kotlin
  class HomeActivity : Activity(), EMMANotificationInterface {
      override fun onCreate(savedInstanceState: Bundle?) {
          super.onCreate(savedInstanceState);
          EMMA.getInstance().getNotificationInfo();
      }

      override fun onPushOpen(pushCampaign: EMMAPushCampaign) {
          //Do whatever you want with push campaign
      }
  }
  ```
  ###

Para inhabilitar las notificaciones de un dispositivo, use `EMMA.getInstance().unregisterPushService()` y asegúrate de no llamar de nuevo a `EMMA.getInstance().startPushSystem(...)`.


## Push con colores personalizados

Puedes seleccionar el color que quieras en las notificaciones push añade el `setNotificationColor` a la opciones de push:

#### Tabs {.tabset}
##### Kotlin
``` kotlin
    val pushOpt = EMMAPushOptions.Builder(PushActivity::class.java, R.drawable.notification_icon)
                .setNotificationColor(ContextCompat.getColor(this, R.color.yellow))
                .build()

    EMMA.getInstance().startPushSystem(configuration);
```


## Sonidos personalizados en las notificaciones push

Se necesita la versión del SDK 2.5.5 o una superior.

Para usar sonidos personalizados en las notificaciones que envíes con EMMA, tienes que añadir los archivos de sonido que quieras a la carpeta raw de los recursos de tu app (carpeta *res*). Recuerda que debes emplear para los sonidos los mismos nombres de archivo en iOS y Android.

## Rich push URL uso DeepLinking

Puedes redireccionar las aperturas de las notificaciones push a una sección en tu app. Para ello debes usar una estructura como esta:

**scheme://host/page1/page2/page3...**

La versión 4.2 trae cambios referentes a la gestión del deeplink. Para integrarlo sigue los siguientes pasos:

-   Añade la siguiente actividad en el `AndroidManifest.xml`:  
    
    #### Tabs {.tabset}
    ##### AndroidManifest.xml
    ``` xml
        <activity   
            android:name="io.emma.android.activities.EMMADeepLinkActivity" android:theme="@android:style/Theme.NoDisplay">     
        <intent-filter>  
            <action android:name="android.intent.action.VIEW"/>  
            <category android:name="android.intent.category.DEFAULT"/>   
            <category android:name="android.intent.category.BROWSABLE"/>  
            <data android:scheme="YOUR_SCHEME"`  
            android:host="YOUR_HOST"/>
        </intent-filter>
    </activity>
    ```
    ###

-   Añade la actividad a ser lanzada, en forma de `<meta-data>` dentro del tag `<application>` en `AndroidManifest.xml`. Esta actividad será lanzada cuando el SDK ejecute un deeplink:  
    
    #### Tabs {.tabset}
    ##### AndroidManifest.xml
    ``` xml
    <meta-data 
        android:name="io.emma.DEEPLINK_OPEN_ACTIVITY"   
        android:value="com.your.package.CustomDeeplinkActivity"/> 
    ```
    ### 
      
    Recuerda que en caso de ser una actividad que se pueda encontrar abierta en la app cuando un deeplink se ejecute, se tiene que declarar en el `AndroidManifest.xml` como `singleTask`:  
    
    #### Tabs {.tabset}
    ##### AndroidManifest.xml   
    ``` xml
    <activity`  
        android:name=".activities.MainActivity"
        android:launchMode="singleTask"/>  
    ```
    ###
    
    En este caso, si la actividad esta en la pila cuando se ejecuta el deeplink, el intent con la información de este llegará al método de la actividad `onNewIntent`. En caso de no tener el `launchMode` como `singleTask` ocasiona que la actividad se instancie nuevamente, lo que produce duplicaciones.

-   Para que el deeplink que se introduce en el campo Rich Push URL (en la web de EMMA) se ejecute correctamente, es importante añadir el siguiente método en la actividad que es abierta por el push o alguna otra que sea consecuente de esta, pero que se ejecute al abrir la app desde la notificación:  
    
    #### Tabs {.tabset}
    ##### Kotlin
    ``` kotlin
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        EMMA.getInstance().checkForRichPush();
    }
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent);
        EMMA.getInstance().onNewNotification(intent, true);
    }
    ```
    ###
    El método `checkForRichPush` y `onNewNotification` comprueban el campo Rich Push enviado en el payload de la notificación y realizan las acciones pertinentes. En el caso de ser un deeplink lo ejecutarán abriendo la actividad definida en el metadato del `AndroidManifest.xml`.
-   Definir el comportamiento de la actividad que ha de abrir el deeplink. En este caso `CustomDeepLinkActivity`:  
    
    #### Tabs {.tabset}
    ##### Kotlin
    ``` kotlin
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view);

        if (intent != null && intent.data != null ) {
            processDeepLink(intent.data);
        }
        finish();
    }

    private fun processDeepLink(uri: Uri) {
        if (uri.host.equals("home")) {
            goHome();
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent);
        if (intent != null && intent.data != null ) {
            processDeepLink(intent.data);
        }
    }
    ```
    ###
    
## Usar un servicio personalizado

EMMA da la posibilidad de integrar las notificaciones push fuera de su propio servicio.

```kotlin
class CustomService: FirebaseMessagingService() {
    
    private fun isPushFromEMMA(remoteMessage: RemoteMessage): Boolean {
        return remoteMessage.data["eMMa"] == "1"
    }
    
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (isPushFromEMMA(remoteMessage)) {
            EMMAPushNotificationsManager.handleNotification(applicationContext, remoteMessage.data)
        }
    }

    override fun onNewToken(token: String) {
        EMMAPushNotificationsManager.refreshToken(applicationContext, token, EMMAPushType.FCM)
    }
}
```

## Botones con acciones

A partir de la versión 4.10.x, se ha desarrollado la funcionalidad de añadir botones con acciones en las notificaciones. Para añadir está funcionalidad a Android, simplemente hay que tener el SDK actualizado a esta versión.

Para la configuración en la interfaz consulta [aquí](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#contenido).


# Integración Notificaciones Push con Huawei

El 19 de mayo de 2019, Google decidió finalizar con Huawei aquellos negocios que requieran transferencia de hardware y software, menos aquellos cubiertos por licencias open source. Esto se traduce en que los dispositivos fabricados a partir de esta fecha no contarán con los servicios de Google como es la Play Store, aunque sí con el equivalente, la App Gallery de Huawei.

Desde EMMA, hemos modificado nuestro SDK para adaptarlo a la tienda de aplicaciones de Huawei. Con el objetivo de medir la atribución así como permitir el envío de notificaciones Push mediante su servicio.

>La versión mínima del SDK disponible para hacer la integración de Huawei, es la versión 4.7.0. Si tienes una versión del SDK anterior, deberás actualizarla para que todo funcione debidamente.
{.is-warning}

Para realizar integración hay que seguir los siguientes pasos:

-   Añadir las dependencias
-   Añadir el servicio al AndroidManifest
-   Obtener credenciales

## Añadir las dependencias

-   Lo primero que debemos hacer es añadir las dependencias al `build.gradle` del proyecto.  
  
    #### Tabs {.tabset}
    ##### build.gradle  
    ```groovy
    buildscript { 
     repositories { 
        google()`   
        jcenter() 
        maven { url 'http://developer.huawei.com/repo/' }
     } 
     dependencies {  
        classpath 'com.android.tools.build:gradle:7.4.1' 
        classpath 'com.huawei.agconnect:agcp:1.6.2.300' 
     }
    }
    allprojects { 
     repositories { 
        google()
        jcenter()
        maven {url 'https://developer.huawei.com/repo/'} 
     }  
    }
    ```
    ###

-   A continuación añadimos las dependencias de HMS a la aplicación. La dependencia *ads-identifier* permite obtener el identificador de publicidad usado en Huawei. Le dependencia *push* permite el uso del servicio HMS para notificaciones push.   

    #### Tabs {.tabset}
    ##### build.gradle
    ```groovy  
    dependencies {  
     // other dependencies  
     implementation 'com.huawei.hms:ads-identifier:3.4.62.300'   
     implementation 'com.huawei.hms:push:6.10.0.300'  
    }
    apply plugin: 'com.huawei.agconnect'
    ```
    ###

>La integración del servicio HMS es compatible con la integración de FCM en el SDK de EMMA. La  versión mínima de Android soportada para la integración de HMS es Android 19.
{.is-warning}

## Añadir el servicio al AndroidManifest

Hay que añadir el siguiente *servicio* al `AndroidManifest.xml`:
#### Tabs {.tabset}
##### AndroidManifest.xml 
```xml
<service 
 android:name="io.emma.android.push.EMMAHmsMessagingService"
 android:exported="false">
 <intent-filter>
    <action android:name="com.huawei.push.action.MESSAGING_EVENT" />
 </intent-filter>
</service>
```
###

## Obtener credenciales

Para obtener las credenciales dirigete a la siguiente sección [aquí](#obtener-credenciales).

Por último, compilamos la app y ya podemos enviar notificaciones push a dispositivos Huawei.

# Integración Behavior
Con EMMA puedes realizar una integración completa del SDK que te permita conocer la localización de tus usuarios, cómo se registran en tu App, cuántas transacciones realizan y hasta sus características propias. Es decir, toda la información de tus usuarios que obtendrás en la sección de Behavior.
## Medición de eventos
La plataforma de EMMA hace la diferenciación entre dos tipos de eventos. Los que la plataforma incluye por defecto y los eventos Custom que quieras integrar según la estructura de tu aplicación.
### Eventos por defecto

Puedes consultar más información sobre los eventos por defecto [aquí](
https://docs.emma.io/es/primeros-pasos/eventos#eventos-por-defecto).

#### Medir Registro
El método `EMMA.getInstance().registerUser()` permite enviar información sobre los registros en la aplicación.

#### Tabs {.tabset}
##### Kotlin
```kotlin
fun register() {
	EMMA.getInstance().registerUser("554234", "test@emma.io")
}
```
##### Java
```java
function register() {
	EMMA.getInstance().registerUser("554234", "test@emma.io")
}
```
#### Medir Login
El método `EMMA.getInstance().loginUser()` permite enviar información sobre los eventos login.

Si tenemos un evento login sucesivo con los mismos datos, podemos utilizar el método `EMMA.loginDefault()`. Este método sería útil en el caso de un "Auto-Login", por ejemplo.

#### Tabs {.tabset}
##### Kotlin
```kotlin
fun login() {
	EMMA.getInstance().loginUser("554234", "test@emma.io")
}
```
##### Java
```java
public void login() {
	EMMA.getInstance().loginUser("554234", "test@emma.io");
}
```

#### Medir transacciones
EMMA permite medir cualquier transacción o compra que se realice en tu app. Este es el ejemplo para medir una transacción:

##### Tabs {.tabset}
###### Kotlin
```kotlin
    fun trackTransaction() {
        EMMA.getInstance().startOrder("<ORDER_ID>","<CUSTOMER_ID>",10.0, "")
        EMMA.getInstance().addProduct("<PRODUCT_ID>", "<PRODUCT_NAME>", 1.0, 10.0)
        EMMA.getInstance().trackOrder()
    }
```
###### Java
```java
  public void trackTransaction {
      	EMMA.getInstance().startOrder("<ORDER_ID>","<CUSTOMER_ID>",10.0, "");
        EMMA.getInstance().addProduct("<PRODUCT_ID>", "<PRODUCT_NAME>", 1.0, 10.0);
        EMMA.getInstance().trackOrder();
  }
```

##### Iniciar transacción

El método para iniciar la transacción es `EMMA.getInstance().startOrder()`.

##### Tabs {.tabset}
###### Kotlin
```kotlin
EMMA.getInstance().startOrder("<ORDER_ID>","<CUSTOMER_ID>",10.0, "")
```
###### Java
```java
EMMA.getInstance().startOrder("<ORDER_ID>","<CUSTOMER_ID>",10.0, "");
```

##### Añadir Productos a la transacción

Una vez iniciada la transacción, hay que añadir los productos a la misma. Para ello usaremos el método `EMMA.getInstance().addProduct()`.

##### Tabs {.tabset}
###### Kotlin
```kotlin
	EMMA.getInstance().addProduct("<PRODUCT_ID>", "<PRODUCT_NAME>", 1.0, 10.0)
```
###### Java
```java
	EMMA.getInstance().addProduct("<PRODUCT_ID>", "<PRODUCT_NAME>", 1.0, 10.0);
```

##### Medición de la transacción

Una vez tenemos todos los productos añadidos, ejecutamos la medición de la transacción con el método `EMMA.getInstance().trackOrder()`.

##### Tabs {.tabset}
###### Kotlin
```kotlin
EMMA.getInstance().trackOrder()
```
###### Java
```java
EMMA.getInstance().trackOrder();
```

##### Cancelar una transacción

En el caso de que se necesite cancelar el tracking de una transacción usaremos el método `EMMA.getInstance().cancelOrder()`.

##### Tabs {.tabset}
###### Kotlin
```kotlin
    fun cancelTransaction() {
        EMMA.getInstance().cancelOrder("<ORDER_ID>")
    }
```
###### Java
```java
public void cancelTransaction {
    EMMA.getInstance().cancelOrder("<ORDER_ID>");
}
```

## Eventos personalizados

Puedes consultar más información sobre los eventos personalizados [aquí](https://docs.emma.io/es/primeros-pasos/eventos#eventos-personalizados).

Ejemplo para enviar un evento propio de la aplicación:

#### Tabs {.tabset}
##### Kotlin
```kotlin
val eventRequest = EMMAEventRequest("f983d4bef8fc44dad43a1bb30dde9e3c")
//Optional: custom attributes
eventRequest.attributes = attributes
//Optional: request status listener
eventRequest.requestListener = requestListener
//Optional: cumtom id for request delegate
eventRequest.customId = customId

EMMA.getInstance().trackEvent(eventRequest)
```
##### Java
```java
EMMAEventRequest eventRequest = new EMMAEventRequest("f983d4bef8fc44dad43a1bb30dde9e3c");
//Optional: custom attributes
eventRequest.setAttributes(attributes);
//Optional: request status delegate
eventRequest.setRequestDelegate(requestDelegate);
//Optional: cumtom id for request delegate
eventRequest.setCustomId(customId);

EMMA.getInstance().trackEvent(eventRequest);
```

# Perfil del usuario

## Completar el perfil del usuario mediante TAGS

En EMMA podemos enriquecer el perfil del usuario con información almacenada mediante Clave / Valor a la que le llamamos tags de usuario.

Para enviar esta información deberemos implementar el método `trackExtraUserInfo`:

#### Tabs {.tabset}
##### Kotlin
```kotlin
    val tags = ArrayMap<String, String>()
    tags["MY_CUSTOM_TAG"] = "MY_CUSTOM_VALUE"
    EMMA.getInstance().trackExtraUserInfo(tags)
```
##### Java
```Java
	Map<String, String> tags = new HashMap<>();
tags.put("MY_CUSTOM_TAG", "MY_CUSTOM_VALUE");
EMMA.getInstance().trackExtraUserInfo(tags);
```

## Registro de la localización del usuario

Si tu aplicación cuenta con permisos de localización, EMMA puede guardar esa información en el perfil del usuario.

Si no se quiere registrar la información del usuario, hay que implementar el método `disableTrackingLocation()` antes de ejecutar la llamada `startSession`.

## Información del usuario

### Identificador de EMMA
Podemos recuperar el identificador interno de EMMA con el método `getUserID()`:
#### Tabs {.tabset}
##### Kotlin
```kotlin
    class MainActivity : BaseActivity(), EMMAUserInfoInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        EMMA.getInstance().getUserID()
    }
    
    override fun OnGetUserInfo(userInfo: JSONObject?) {
      // Not implemented
    }

    override fun OnGetUserID(id: Int) {
			Log.d("MainActivity", id.toString())
    }
}
```
##### Java
```Java
public class MainActivity extends BaseActivity implements EMMAUserInfoInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EMMA.getInstance().getUserID();
    }

    @Override
    public void OnGetUserInfo(JSONObject userInfo) {
        // Not implemented
    }

    @Override
    public void OnGetUserID(int id) {
        Log.d("MainActivity", Integer.toString(id));
    }
}
```

### Identificador de dispositivo

El formato del identificador es del tipo UUID V4. Para obtener el identificador del dispositivo usa el siguiente método:

#### Tabs {.tabset}
##### Kotlin
```kotlin
  EMMA.getInstance().getDeviceId()
```
##### Java
```java
	EMMA.getInstance().getDeviceId();
```
####

En el siguiente ejemplo, implementamos la interfaz `EMMADeviceIdListener` y usamos el método `getDeviceId()`. Se recibe el identificador del dispositivo en la función `onObtained()`, de obligada implementación por la interfaz:

#### Tabs {.tabset}
##### Kotlin
```kotlin
class MainActivity : BaseActivity(), EMMADeviceIdListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        EMMA.getInstance().getDeviceId(deviceIdListener: this)
    }
    
    override fun onObtained(deviceId: String?) {
        Log.d("MainActivity", "DeviceId: $deviceId")
    }
}
```
##### Java
```java
public class MainActivity extends BaseActivity implements EMMADeviceIdListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EMMA.getInstance().getDeviceId(this);
    }

    @Override
    public void onObtained(String deviceId) {
        Log.d("MainActivity", "DeviceId: " + deviceId);
    }
}
```

### Identificador de usuario del cliente (Customer ID)
Para enviar el customer ID independientemente del login/registro usa el siguiente método:

#### Tabs {.tabset}
##### Kotlin
```kotlin
  EMMA.getInstance().setCustomerId("<Customer ID>")
```
##### Java
```java
	EMMA.getInstance().setCustomerId("<Customer ID>");
```


### Perfil del usuario (User Info)
Si necesitamos recuperar el perfil del usuario desde la aplicación usaremos el método `getUserInfo()`:

#### Tabs {.tabset}
##### Kotlin
```kotlin
    class MainActivity : BaseActivity(), EMMAUserInfoInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        EMMA.getInstance().getUserInfo()
    }
    
    override fun OnGetUserInfo(userInfo: JSONObject?) {
      userInfo?.let {
      	// Do something with userInfo
      }
    }

    override fun OnGetUserID(id: Int) {
			// Not implemented
    }
}
```
##### Java
```Java
public class MainActivity extends BaseActivity implements EMMAUserInfoInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EMMA.getInstance().getUserInfo();
    }

    @Override
    public void OnGetUserInfo(JSONObject userInfo) {
        if (userInfo != null) {
            // Do something with userInfo
        }
    }

    @Override
    public void OnGetUserID(int id) {
        // Not implemented
    }
}
```


### Información de la atribución de la instalación

Después del proceso de atribución, EMMA pone a disposición del SDK la información de atribución del usuario.

Para obtener la información de atribución usaremos el método `getInstallAttributionInfo`:

#### Tabs {.tabset}
##### Kotlin
```kotlin
    class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
       
 				EMMA.getInstance().getInstallAttributionInfo { attribution ->
            if (attribution != null) {
               // Do something with attribution
            }
        }
    }
}
```
##### Java
```Java
public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EMMA.getInstance().getInstallAttributionInfo(new EMMAInstallAttributionInterface() {
            @Override
            public void onAttributionReceived(EMMAInstallAttribution attribution) {
                if (attribution != null) {
                    // Do something with attribution
                }
            }
        });
    }
}
```
###
Consulta la [descripción de los campos de atribución](/es/reference/user-info-fields#descripción-de-los-campos-de-atribución-del-usuario) para ver la información disponible.



# Opciones avanzadas

## Proguard

Si utilizas *proguard* o alguna alternativa compatible, a continuación tenemos un ejemplo del contenido del fichero de reglas `proguard-rules.pro`. Puede que tengas que modificar otras reglas dependiendo de cada aplicación.

```
# EMMA SDK
-keep class io.emma.android.** { *; }

# Rules for play services ads identifier
-keep class com.google.android.gms.common.ConnectionResult {
    int SUCCESS;
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient {
    com.google.android.gms.ads.identifier.AdvertisingIdClient$Info getAdvertisingIdInfo(android.content.Context);
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info {
    java.lang.String getId();
    boolean isLimitAdTrackingEnabled();
}

# Rule for google play referrer
-keep public class com.android.installreferrer.** { *; }

# Keep generic signatures; needed for correct type resolution
-keepattributes Signature

# Keep Gson annotations
-keepattributes RuntimeVisibleAnnotations,AnnotationDefault
-if class com.google.gson.reflect.TypeToken
-keep,allowobfuscation class com.google.gson.reflect.TypeToken
-keep,allowobfuscation class * extends com.google.gson.reflect.TypeToken
-keep,allowobfuscation,allowoptimization @com.google.gson.annotations.JsonAdapter class *
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.Expose <fields>;
  @com.google.gson.annotations.JsonAdapter <fields>;
  @com.google.gson.annotations.Since <fields>;
  @com.google.gson.annotations.Until <fields>;
}

-keepclassmembers class * extends com.google.gson.TypeAdapter {
  <init>();
}
-keepclassmembers class * implements com.google.gson.TypeAdapterFactory {
  <init>();
}
-keepclassmembers class * implements com.google.gson.JsonSerializer {
  <init>();
}
-keepclassmembers class * implements com.google.gson.JsonDeserializer {
  <init>();
}

-if class *
-keepclasseswithmembers,allowobfuscation class <1> {
  @com.google.gson.annotations.SerializedName <fields>;
}
-if class * {
  @com.google.gson.annotations.SerializedName <fields>;
}
-keepclassmembers,allowobfuscation,allowoptimization class <1> {
  <init>();
}

# Rules for retrofit2
-keepattributes Signature, InnerClasses, EnclosingMethod
-keepattributes RuntimeVisibleAnnotations, RuntimeVisibleParameterAnnotations
-keepattributes AnnotationDefault
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn javax.annotation.**
-dontwarn kotlin.Unit
-dontwarn retrofit2.KotlinExtensions
-dontwarn retrofit2.KotlinExtensions$*
-if interface * { @retrofit2.http.* <methods>; }
-keep,allowobfuscation interface <1>

-if interface * { @retrofit2.http.* <methods>; }
-keep,allowobfuscation interface * extends <1>

-keep,allowobfuscation,allowshrinking class kotlin.coroutines.Continuation
-if interface * { @retrofit2.http.* public *** *(...); }
-keep,allowoptimization,allowshrinking,allowobfuscation class <3>

-keep,allowobfuscation,allowshrinking class retrofit2.Response

# Rules for okhttp3
-keepattributes Signature
-keepattributes Annotation
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontwarn okio.**

# Rules for glide (used to display images by sdk)
-keep class com.bumptech.glide.** { *; }
-dontwarn com.bumptech.glide.**

# Rules for push
-keep class com.google.firebase.** { *; }

# Rules for Huawei hms push and odid identifier
-ignorewarnings
-keepattributes *Annotation*
-keepattributes Exceptions
-keepattributes InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
-keep class com.huawei.hianalytics.**{*;}
-keep class com.huawei.updatesdk.**{*;}
-keep class com.huawei.hms.**{*;}

# Rules for Huawei ads-identifier and ads referrer
-keep class com.huawei.hms.ads.** { *; }
-keep interface com.huawei.hms.ads.** { *; }
```

## Uso de custom proxy / custom webservice url

En algunos casos, es necesario cambiar la URL de API EMMA (p.e. proxies), para ello hay que añadir lo siguiente justo antes del método `startSession(...)`:

#### Tabs {.tabset}
##### Kotlin
```kotlin
val configuration = EMMA.Configuration.Builder(this)
      .setSessionKey("example0ikl98")
      .setWebServiceUrl("https://www.your_proxy_url.com/")
      .setDebugActive(BuildConfig.DEBUG)
      .build()
```

##### Java
```java
EMMA.Configuration configuration = new EMMA.Configuration.Builder(this)
      .setSessionKey("example0ikl98")
      .setDebugActive(BuildConfig.DEBUG)
      .setWebserviceUrl("https://www.your_proxy_url.com/")
      .build();
```
# Integración In-App Messaging

EMMA incluye 7 formatos comunicativos diferentes que puedes integrar para impactar a tus usuarios en Android:

- **NativeAd** - Formato que integra tus comunicaciones respetando siempre el formato y estilo de los contenidos de la App.
- **StartView** - Formato que se despliega en toda la pantalla mostrando contenido web.
- **AdBall** - Formato comunicativo con forma de burbuja, cuyo click provoca el despliegue de un contenido web.
- **Banner** - Formato básico para tus comunicaciones que puedes utilizar para comunicar mensajes específicos tanto arriba como abajo de la pantalla.
- **Strip** - Formato comunicativo que ocupa la barra de notificaciones del dispositivo y que muestra un mensaje de derecha a izquierda.
- **Coupon** - Formato que te da la opción de canjear cupones si tienes un lugar físico controlando el número de redenciones, el código promocional y la fecha de activación y finalización.
- **Plugins personalizados** - Formato de comunicación propio a través de la tecnología de NativeAd.
{.grid-list .body-2}

## NativeAd

EMMA NativeAd te permite obtener la información de un NativeAd correspondiente a una plantilla que se haya definido y configurado en la plataforma de EMMA.

```kotlin
class NativeAdsActivity: BaseActivity(),
    EMMAInAppMessageInterface,
    EMMABatchNativeAdInterface,
    EMMANativeAdInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun getNativeAd(templateId: String) {
        val nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        EMMA.getInstance().getInAppMessage(nativeAdRequest, this)
    }

    fun getNativeAdBatch(templateId: String) {
        val nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        nativeAdRequest.isBatch = true
        EMMA.getInstance().getInAppMessage(nativeAdRequest, this)
    }

    override fun onBatchReceived(nativeAds: MutableList<EMMANativeAd>) {
        nativeAds.forEach { nativeAd ->
            nativeAd.tag?.let { tag ->
                print("Received batch nativead with tag: $tag")
            }
        }
    }

    override fun onReceived(nativeAd: EMMANativeAd) {
        val content = nativeAd.nativeAdContent
        val title = content["Title"]?.fieldValue
        if (title !=  null) {
            print("Received NativeAd with Title: $title")
            EMMA.getInstance().sendInAppImpression(CommunicationTypes.NATIVE_AD, nativeAd)
        }
    }

    fun openNativeAd(nativeAd: EMMANativeAd) {
        EMMA.getInstance().openNativeAd(nativeAd)
    }

    fun sendNativeAdClick(nativeAd: EMMANativeAd) {
        EMMA.getInstance().sendInAppClick(CommunicationTypes.NATIVE_AD, nativeAd)
    }

    override fun onShown(campaign: EMMACampaign) {
        print("Invocado método onShown")
    }

    override fun onHide(campaign: EMMACampaign) {
        print("Invocado método onHide")
    }

    override fun onClose(campaign: EMMACampaign) {
        print("Invocado método onClose")
    }
}
```

Obtendremos toda la información del NativeAd disponible para el usuario referente al *templateId*, según las condiciones que se hayan configurado en la plataforma de EMMA.

Se llama a **onReceived** en el caso de que exista un NativeAd correspondiente a la plantilla con identificador *templateId*.

Se llama a **onBatchReceived** cuando se realiza la llamada de NativeAd con el parámetro `batch` a `true` y existen uno o más NativeAds correspondientes a la plantilla con identificador *templateId*.

`EMMANativeAd` contiene todos los campos configurados en EMMA para esta plantilla de NativeAd, para obtenerlos se usará el siguiente método:

```kotlin
		val content = nativeAd.nativeAdContent
    val title = content["Title"]?.fieldValue
    val image = content["Main picture"]?.fieldValue
    val cta = content["CTA"]?.fieldValue
    printSide(title, image, cta)
````

En el caso de configurar una plantilla contenedor se pueden obtener los valores del NativeAd de la siguiente forma:

```kotlin
   val content = nativeAd.nativeAdContent
   val container = content["container"]
   container?.fieldContainer?.forEach { containerSide ->
     val title = containerSide["Title"]?.fieldValue
     val image = containerSide["Main picture"]?.fieldValue  
     val cta = containerSide["CTA"]?.fieldValue
     printSide(title, image, cta)
   }
````

Una vez obtenidos todos los campos requeridos, ya se puede crear la vista para pintar este NativeAd en pantalla en función del diseño que se le quiera aplicar. Una vez pintado el NativeAd en pantalla es necesario llamar a este método para poder obtener las impresiones en el reporting:

```kotlin
     EMMA.getInstance().sendInAppImpression(CommunicationTypes.NATIVE_AD, nativeAd)

````
### NativeAd único

Para obtener un NativeAd único hay que utilizar el método `getInAppMessage` pasandole una instancia de `EMMANativeAdRequest` con el *templateId* especifico que se quiere usar. En el caso de tener varios NativeAds bajo el mismo *templateId* con este método se obtiene el último creado. 

```kotlin
    fun getNativeAd(templateId: String) {
        val nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        EMMA.getInstance().getInAppMessage(nativeAdRequest, this)
    }
```
### NativeAd múltiple (Batch)

Método usado para obtener todos los NativeAds activos bajo un mismo *templateId*.

```kotlin
    fun getNativeAdBatch(templateId: String) {
        val nativeAdRequest = EMMANativeAdRequest()
        nativeAdRequest.templateId = templateId
        nativeAdRequest.isBatch = true
        EMMA.getInstance().getInAppMessage(nativeAdRequest, this)
    }
```

### Abrir NativeAd

El SDK proporciona un método para realizar la apertura de un NativeAd, este método se pone a modo de CTA cuando se haga click o se realice alguna acción sobre el NativeAd. Internamente este método lanza un WebView inapp o abre el browser con el CTA configurado en el Dashboard y seguidamente traquea el clic.

```kotlin
fun openNativeAd(nativeAd: EMMANativeAd) {
	EMMA.getInstance().openNativeAd(nativeAd)
}
```
Alternativamente, si no se usa este método, se puede enviar el click llamando al método:

```kotlin
	EMMA.getInstance().sendInAppClick(nativeAd)
```

## StartView

La StartView es un formato de comunicación que te permite mostrar un contenido HTML, alojado en una URL, en un WebView a pantalla completa.

```kotlin
fun getStartView() {
  val startViewRequest = EMMAInAppRequest(EMMACampaign.Type.STARTVIEW)
  EMMA.getInstance().getInAppMessage(startViewRequest)
}
```

## AdBall

El AdBall es una pequeña vista circular que muestra una imagen. Esta vista se puede arrastrar por toda la pantalla y eliminar de ella en cualquier momento, contiene un CTA que es una URL con contenido HTML que lanza un WebView al hacer clic en la ella.

```kotlin
fun getAdBall() {
  val adBallRequest = EMMAInAppRequest(EMMACampaign.Type.ADBALL)
  EMMA.getInstance().getInAppMessage(adBallRequest)
}
```

## Banner

El banner es un formato de comunicación que permite adaptar una imagen o GIF en formato banner dentro de una pantalla de la aplicación. Este banner se puede mostrar en el de la pantalla dónde se muestra o en el *bottom* de esta. El banner contiene un CTA configurable en el Dashboard de EMMA y puede ser un *deeplink* o una URL *https*. En el caso de ser la segunda opción, al hacer hacer clic se abre un WebView con el contenido de la URL.

>Se recomienda hacer la llamada después de que todos los elementos de la pantalla estén cargados.
{.is-warning}

```kotlin
fun getBanner() {
  val bannerRequest = EMMAInAppRequest(EMMACampaign.Type.BANNER)
  EMMA.getInstance().getInAppMessage(bannerRequest);
}
```

## Strip

El strip te permite mostrar un banner en lo alto de la pantalla del dispositivo con un texto que va pasando a modo de carrusel. Variables como el tiempo de duración de la rotación o el tiempo de visualización son configurables desde el Dashboard.

```kotlin
fun getStrip() {
  val stripRequest = EMMAInAppRequest(EMMACampaign.Type.STRIP)
  EMMA.getInstance().getInAppMessage(stripRequest);
}
```

## Coupon

EMMA Coupons te permite obtener, verificar y canjear cupones que se hayan definido y configurado en la plataforma de EMMA.

```kotlin
class CouponsActivity: BaseActivity(), EMMACouponsInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EMMA.getInstance().addCouponsCallback(this)
        getCoupons()
    }

    private fun getCoupons() {
        EMMA.getInstance().getInAppMessage(EMMAInAppRequest(EMMACampaign.Type.COUPON))
    }

    private fun getSingleCoupon() {
        val couponsRequest = EMMAInAppRequest(EMMACampaign.Type.COUPON)
        couponsRequest.inAppMessageId = "<COUPON_ID>"
        EMMA.getInstance().getInAppMessage(couponsRequest)
    }

    private fun redeemCoupon() {
        val redeemCouponRequest = EMMAInAppRequest(EMMACampaign.Type.REDEEM_COUPON)
        redeemCouponRequest.inAppMessageId = "<COUPON_ID>"
        EMMA.getInstance().getInAppMessage(redeemCouponRequest)
    }

    private fun cancelCoupon() {
        val cancelCouponRequest = EMMAInAppRequest(EMMACampaign.Type.CANCEL_COUPON)
        cancelCouponRequest.inAppMessageId = "<COUPON_ID>"
        EMMA.getInstance().getInAppMessage(cancelCouponRequest)
    }

    private fun couponValidRedeems() {
        val couponValidRedeems = EMMAInAppRequest(EMMACampaign.Type.COUPON_VALID_REDEEMS)
        couponValidRedeems.inAppMessageId = "<COUPON_ID>"
        EMMA.getInstance().getInAppMessage(couponValidRedeems)
    }

    override fun onCouponsReceived(coupons: List<EMMACoupon>) {
        coupons.let {
            // Show coupons
            coupons.forEach { coupon ->
                EMMA.getInstance().sendInAppImpression(CommunicationTypes.COUPON, coupon)
            }
        }
    }

    override fun onCouponsFailure() {
        print("An error has occurred obtaining coupons")
    }

    override fun onCouponRedemption(success: Boolean) {
        print("Coupon redemption success: $success")
    }

    override fun onCouponCancelled(success: Boolean) {
        print("Coupon cancelled success: $success")
    }

    override fun onCouponValidRedeemsReceived(numRedeems: Int) {
        print("Coupon redeems: $numRedeems")
    }
}
```
### Obtener cupones

Con esta llamada, obtendremos toda la información de los coupons disponibles para el usuario, según las condiciones que se hayan configurado en la plataforma de EMMA.

```kotlin
private fun getCoupons() {
  EMMA.getInstance().getInAppMessage(EMMAInAppRequest(EMMACampaign.Type.COUPON))
}
```

Se devolverá un listado de los cupones existentes, listándose primero los cupones automáticos ordenados de más reciente a más antiguo y después se listarán los cupones clásicos ordenados también de más reciente a más antiguo.

Se llama a `EMMACouponsInterface.onCouponsReceived` en el caso de que el usuario tenga disponibles cupones para canjear o a `EMMACouponsInterface.onCouponsFailure` si el usuario no tiene cupones disponibles. `EMMACoupon` contiene toda la información relativa al cupón: id (identificador interno de EMMA), código, número máximo de canjeos, número de veces canjeado, título, descripción, imagen...

Si se quiere enviar a EMMA la información de cuando se ha hecho click en el cupón o cuando este se muestra en la pantalla, hay que añadir los siguientes métodos cuando se realicen ambas acciones en la app:

```kotlin
EMMA.getInstance().sendInAppImpression(CommunicationTypes.COUPON, couponCampaign);
EMMA.getInstance().sendInAppClick(CommunicationTypes.COUPON, couponCampaign);
```

### Cancelar cupones

```kotlin
private fun cancelCoupon() {
  val cancelCouponRequest = EMMAInAppRequest(EMMACampaign.Type.CANCEL_COUPON)
  cancelCouponRequest.inAppMessageId = "<COUPON_ID>"
  EMMA.getInstance().getInAppMessage(cancelCouponRequest)
}
```

Con esta llamada se puede cancelar el canjeo de un cupón realizado con anterioridad.

El parámetro *couponId* debe ser el identificador interno de EMMA de un identificador que se puede obtener de una llamada a `getCoupons` hecha con anterioridad. Se llama a `EMMACouponsInterface.onCouponCancelled`, donde *success* indica si se ha cancelado el cupón.

## Plugins personalizados

### Crear plugin

A partir de la versión 4.9.0 se ha añadido la posibilidad de poder añadir plugins in-app al SDK. Los plugins inapp funcionan a través de la tecnología de NativeAd.

Puedes crear tu propio formato de comunicación y convertirlo en un plugin in-app, para ello es necesario que la clase principal del nuevo formato extienda de la clase abstracta `EMMAInAppPlugin`, esta clase obliga a sobrescribir dos métodos:

```kotlin
class CustomInAppPlugin: EMMAInAppPlugin() {

 override fun getId(): String = "emma-plugin-custom"
 
 override fun show(context: Activity?, nativeAd: EMMANativeAd) {
 // Process data
 }

 override fun dismiss() {
 }
```
El método `show()` es el método principal ya que es el que lanza el SDK cuando recibe el NativeAd correspondiente al plugin. El SDK pasa al plugin como contexto la *activity* actual que hay visible en la app y el NativeAd con el contenido acorde a la plantilla marcada como plugin en el Dashboard. Con estos parámetros se puede crear cualquier formato de comunicación adaptado al contenido del NativeAd.

El método `dismiss()` es para ocultar el plugin automáticamente. De momento el SDK no tiene incorporado esta funcionalidad, deja el control de ocultado al propio plugin.

El método `getId()` devuelve el identificador del plugin que corresponde con el *templateId* generado en la plantilla.

La clase `EMMAInAppPlugin` contiene varios métodos estáticos cómo `sendInAppImpression` y `sendInAppClick`. Al igual que con un NativeAd, puedes enviar estas acciones. También puedes invocar al/los `inappMessageListener` con los métodos `invokeShownListeners`, `invokeCloseListeners` y `invokeHideListeners`.

Puedes consultar el ejemplo de plugin [aquí](https://github.com/EMMADevelopment/inapp-plugin-prism-android)

### Integrar plugin

Para integrar un plugin es necesario añadirlo en el sdk después del inicio de sesión, para ello es necesario utilizar el método `addInAppPlugin`.

```kotlin
class App: Application {

 override fun onCreate() {
   super.onCreate()
   ....
   EMMA.getInstance().startSession(configuration)
   EMMA.getInstance().addInAppPlugins(CustomPlugin())
 }
 
}

```

Una vez el sdk tenga el plugin simplemente hay que llamarlo en la parte de la app donde se requiera igual que si de un NativeAd se tratase.

```kotlin
class MainActivity: AppCompatActivity() {

 override fun onCreate(savedInstanceState: Bundle?) {
   super.onCreate(savedInstanceState)
   setContentView(R.layout.activity_main)

   val request = EMMANativeAdRequest()
   request.templateId = "emma-plugin-custom"
   EMMA.getInstance().getInAppMessage(request)
 }
 
}
```

## Otras opciones de In-App Messaging

### EMMA Whitelist

Con esta funcionalidad podemos limitar las URLs que abrirá el SDK de EMMA, de modo que en las comunicaciones In-App, sólo se mostrarán en un Webview aquel contenido que empiece por alguna de las URLs que hayamos indicado en la *whitelist*.

Si no indicamos ninguna URL en la *whitelist*, cualquier URL está permitida. Esta funcionalidad afectaría a los Push (Rich URL), Banners, StartViews y AdBalls. Las comunicaciones (Banners, StartViews y AdBalls) que pueden cargar contenido externo a la app mediante un Webview y para los Banners y AdBalls podrían cargar imágenes externas que también serían controladas por la *whitelist*.

Para un Push con Rich URL, si ésta no cumple, no se abriría la Webview correspondiente, pero el push sí que llega a la aplicación. Hay que tener en cuenta que si en vez de una URL se emplea un deeplink, el *scheme* del deeplink debe ser añadido a la *whitelist* para poder abrirlo.

``` kotlin
fun enableWhitelist() {
	EMMA.getInstance().whitelist = mutableListOf("https://mydomain.com", "https://mydomain2.com")
}
```

#### Ejemplos

Si mi *whitelist* es “http://mydomain.com”.

1. Subimos al dashboard de EMMA un Banner con Target URL https://mydomain.com.
	El Banner no se mostrará, deberíamos añadir a la *whitelist* https://mydomain.com.


2. Configuramos en el dashboard de EMMA una StartView con StartView URL http://www.mydomain.com.
	La StartView no se mostrará, deberíamos añadir a la *whitelist* http://www.mydomain.com.

3. Configuramos en el dashboard de EMMA un Banner con Target URL http://mydomain.com/my/url y SmartPhone Banner URL http://subdomain.mydomain.com/my/image.
	El Banner no se mostrará, la url de la imagen no cumple con la whitelist, deberíamos añadir a la *whitelist* http://subdomain.mydomain.com.

4. Subimos al dashboard de EMMA un Banner con Target URL http://mydomain.com/my/url/.
	El Banner se mostrará porque la URL introducida en el campo Target URL empieza por el mismo protocolo y dominio que la URL de la *whitelist*.

5. Configuramos en el dashboard de EMMA una StartView con StartView URL http://mydomain.com/mypage.html&param=value.
	La StartView se mostrará porque la URL introducida en el campo StartView URL empieza por el mismo protocolo y dominio que la URL de la *whitelist*.

### Recuperar parámetros de campaña

Antes de continuar, es necesario añadir los parámetros a la comunicación en el *dashboard* de EMMA para que el SDK los recoja y los ponga a disposición de la aplicación. [Para saber más](https://docs.emma.io/es/mensajes-inapp#propiedades). 

#### Resto de formatos

Para los formatos StartView, AdBall, Strip y Banner, la obtención de los parámetros de campaña se realiza a través de los métodos proporcionados por la interfaz `EMMAInAppMessageInterface`: `onShown`, `onHide` y `onClose`.

Cada uno de estos métodos recibe como parámetro una instancia de `EMMACampaign`, a través de la cual es posible acceder a los parámetros de la campaña mediante la propiedad `campaign.params`.

Ejemplo de uso:

```kotlin
// Implementación de EMMAInAppMessageInterface
class MainActivity : BaseActivity(), EMMAInAppMessageInterface {
	// ...
	
	override fun onShown(campaign: EMMACampaign) {
        if (campaign.params != null && campaign.params.isNotEmpty()) {
					 print(campaign.params)
        }
    }

    override fun onHide(campaign: EMMACampaign) {
        // Acceso a campaign.params si es necesario
    }

    override fun onClose(campaign: EMMACampaign) {
        // Acceso a campaign.params si es necesario
    }
}
```

# Deshabilitar seguimiento publicitario

EMMA permite inhabilitar el trackeo o seguimiento para aquellos usuarios que manifiesten este deseo.

Este método es la mejor forma de adaptarse a la nueva RGPD (Reglamento General de la Protección de Datos).

La comunicación del usuario para expresar esta opción de no seguimiento debe ser tratada por la app haciendo una llamada al siguiente método:

#### Tabs {.tabset}
##### Kotlin
```kotlin
    public fun disableUserTracking(deleteUserData: Bool) {
        /*
         disableUserTracking() shuts down all communication with EMMA servers
         If deleteUserData is true; EMMA will remove this device data from
         their servers. This is a unrecoverable action.
        */
        EMMA.getInstance().disableUserTracking(deleteUserData)
    }
```

##### Java
```java
 public void disableUserTracking(boolean deleteUserData) {
    /*
     disableUserTracking() shuts down all communication with EMMA servers
     If deleteUserData is true; EMMA will remove this device data from
     their servers. This is a unrecoverable action.
    */
    EMMA.getInstance().disableUserTracking(deleteUserData)
}

```
####

En el caso de que se quiera volver a activar las comunicaciones del usuario, se puede utilizar este otro método:
#### Tabs {.tabset}
##### Kotlin
```kotlin
    public func enableUserTracking() {
        EMMA.getInstance().enableUserTracking()
    }
```
##### Java
```Java
- public void enableUserTracking {
	  EMMA.getInstance().enableUserTracking()
}
```

# Política de familias

Para todas aquellas apps que están dentro del programa "Designed for Families" necesitan cumplir una serie de requisitos respecto a la información a compartir. Para ello EMMA ha habilitado una propiedad en el arranque de sesión para asegurar el cumplimiento de esta política.

## Tabs {.tabset}
### Kotlin
```kotlin
import android.app.Application
import io.emma.android.EMMA

class ExampleApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        val configuration = EMMA.Configuration.Builder(this)
            .setSessionKey("example0ikl98")
            .setDebugActive(BuildConfig.DEBUG)
            .setFamiliesPolicyTreatment(true)
            .build()

        EMMA.getInstance().startSession(configuration)
    }
}
```
### Java
```java
import android.app.Application;
import io.emma.android.EMMA;

public class ExampleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        
        EMMA.Configuration configuration = new EMMA.Configuration.Builder(this)
                .setSessionKey("example0ikl98")
                .setFamiliesPolicyTreatment(true)
                .setDebugActive(BuildConfig.DEBUG)
                .build();

        EMMA.getInstance().startSession(configuration);
    }
}
```

Además, también es importante deshabilitar el permiso para recolectar el Google Advertasing ID en el AndroidManifest.

## Tabs {.tabset}
### AndroidManifest.xml
```xml
<uses-permission android:name="com.google.android.gms.permission.AD_ID"
 tools:node="remove"/>
```