---
title: Actualizaciones EMMA SDK Android
description: 
published: true
date: 2025-02-10T08:18:07.283Z
tags: 
editor: markdown
dateCreated: 2025-01-08T10:42:40.660Z
---

# 4.15.3 - 10/02/2025
* **IMPROVE** Ahora el StartView se muestra por encima del Prisma y cualquier formato de comunicación.

# 4.15.2 - 29/12/2024

* **IMPROVE** Optimización de reglas de Proguard.

# 4.15.1 - 11/10/2024

* **FIX** Solucionado el problema de ANR al enviar etiquetas cuando la conexión del dispositivo es deficiente o inexistente. En algunos casos, esto podía causar un ANR o un fallo en caso de pérdida total de conexión.

# 4.15.0 - 26/09/2024

* **NEW** Añadido un nuevo callback de información de atribución de instalación en `startSession`.  
* **IMPROVE** Las sesiones con una duración menor a 10 segundos son descartadas.  
* **FIX** Miniaturas de notificación en Android solo aparecen cuando la notificación se colapsa.  

# 4.14.1 - 20/08/2024

* **FIX** Solucionado un fallo crítico con la dependencia `retrofit2` cuando la aplicación utiliza Android Gradle Plugin v8 y `minifyEnabled` está en `true`.  

# 4.14.0 - 11/07/2024

* **NEW** Añadido el nuevo método `sendDismissedClick` para enviar clics cuando un mensaje in-app es descartado.  
* **IMPROVE** Los datos personales (PII) guardados en preferencias ahora están cifrados.  

# 4.13.0 - 05/02/2024

* **NEW** Eliminado el método `setCurrencyCode` y el parámetro `currencyCode` de `startOrder`. El código de moneda se selecciona ahora en el panel de control de EMMA. Todos los precios enviados en el pedido no serán convertidos por EMMA a una moneda específica, se interpretarán como un valor unitario.  
* **NEW** Actualizado el `minSdkTarget` a Android 21 (Lollipop).  
* **FIX** Corregido un problema donde algunos clics en tiempo real (RT) podían fallar si el SDK se iniciaba antes de que se procesara el clic.  

# 4.12.1 - 28/06/2023

* **NEW** Añadido soporte para notificaciones con título.  

# 4.12.0 - 22/02/2023

* **NEW** Añadido el nuevo método `requestNotificationsPermission` para solicitar permisos de notificaciones en Android 13.  
* **NEW** Añadido el nuevo método `areNotificationsEnabled` para comprobar si las notificaciones están habilitadas.  
* **NEW** Actualizado el `minSdkTarget` a Android 19 (KitKat).  
* **FIX** Correcciones y mejoras menores.  

# 4.11.4 - 15/11/2022

* **NEW** Añadida la posibilidad de cerrar un WebView desde la web utilizando el deeplink `://startview/close`.  

# 4.11.3 - 10/10/2022

* **FIX** Corregido el error `ConcurrentModificationException` que podía ocurrir durante la atribución al enviar eventos en diferentes hilos.  

# 4.11.2 - 05/07/2022

* **NEW** Añadida compatibilidad con la política de Familias.  

# 4.11.1 - 16/06/2022

* **NEW** Añadido soporte para el referrer de instalación de Facebook.  

# 4.11.0 - 05/05/2022

* **NEW** Añadidos tipos de clic para comunicaciones in-app: `Adball` y `StartView`.  
* **IMPROVE** Mejorado el rendimiento en la atribución de eventos. Ahora los eventos se almacenan en caché y se han reducido las lecturas de disco.  

# 4.10.2 - 01/02/2022

* **IMPROVE** Actualizada la política de ID de publicidad de Google. **ACTUALIZACIÓN IMPORTANTE** para evitar la pérdida de dispositivos.  

# 4.10.0 - 20/01/2022

* **NEW** Añadido soporte para notificaciones con botones de acción.  

# 4.9.6 - 01/02/2022

* **IMPROVE** Actualizada la política de ID de publicidad de Google. **ACTUALIZACIÓN IMPORTANTE** para evitar la pérdida de dispositivos.  

# 4.9.5 - 10/11/2021

* **IMPROVE** Actualizado el target SDK de Android a 31.  

# 4.9.3 - 07/10/2021

* **FIX** Actualizada la versión de la dependencia `network`.  

# 4.9.2 - 19/08/2021

* **IMPROVE** Añadido soporte para `FCM > 22.0.0` para obtener el token de push.  

# 4.9.1 - 01/08/2021

* **NEW** Añadido el nuevo método `handleLink` para gestionar clics en enlaces de aplicaciones desde otros puntos de entrada.  

# 4.9.0 - 15/03/2021

* **NEW** Añadido un nuevo método para integrar plugins in-app.  

# 4.8.1 - 18/02/2021

* **FIX** Corregido un problema donde las solicitudes de reinicio podían provocar fallos en algunos casos.  

# 4.8.0 - 12/11/2020

* **IMPROVE** Migración a Android X.  
* **NEW** Añadido un nuevo método para actualizar el `customerId` sin necesidad de iniciar sesión o registrar un evento de usuario.  
* **FIX** Corregido un problema con el `UDID` autogenerado que se marcaba como `Huawei OAID`.  
* **FIX** Correcciones y mejoras menores.
