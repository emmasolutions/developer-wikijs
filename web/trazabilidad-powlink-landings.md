---
title: Trazabilidad de Powlinks en landings
description: 
published: true
date: 2023-09-05T21:04:50.736Z
tags: 
editor: markdown
dateCreated: 2023-09-04T17:24:05.221Z
---

# Trazabilidad Powlinks en landings

En muchas campañas de powlinks es necesario mantener la trazabilidad a través de una landing, evitando enlazar múltiples powlinks de varias campañas.

![flow_powlinks.png](/web/flow_powlinks.png)

Las campañas de powlinks se identifican con un parámetro en la url: `entw`. Para mantener la trazabilidad entre el primer clic y el objetivo final, la store y la posterior instalación de la app, hay que propagar este parámetro entre la landings intermedia siguiendo estos pasos:

1. Creamos una campaña en AppTracker o editamos una existente. Si la campaña no tiene sources creamos una para obtener un enlace de powlink.

2. Añadimos la macro {{CID}} como parámetro entw a nuestras urls de redirección de la campaña. Está macro se reemplazara en la redirección de la campaña en función de la source de origen.

![concat_entw_to_redirect.png](/web/concat_entw_to_redirect.png)

3. Dentro del código de nuestra landing de redirección (punto 2) añadimos el siguiente código javascript para capturar el parámetro y propagarlo al enlace puesto en el CTA.

```javascript
    <script>
      // Obtenemos los parámetros de la url
      const urlParams = new URLSearchParams(window.location.search);
      // Obtenermos el elemento cta
      const cta = document.getElementById("cta");
      // Subdominio usado en EMMA o el dominio custom asignado.
      const powlinkDomain = "https://mydomain.powlink.io/";
      // Obtenemos el valor del param entw
      const entw = urlParams.get("entw") || "";
      if (entw) {
        cta.href = powlinkRTdomain + "?" + "entw=" + entw;
      } else {
        cta.href = powlinkDomain;
      }
    </script>

```


Una vez esté el código en nuestra landing el Powlink de EMMA añadido en el CTA tendrá los mismos parámetros que la campaña original, por lo tanto la atribución irá dirigida a la misma campaña, de esta forma se conservará la trazabilidad.
