---
title: Dashboard embebido
description: 
published: true
date: 2022-10-19T11:37:15.073Z
tags: 
editor: markdown
dateCreated: 2022-10-19T11:05:25.148Z
---

Desde EMMA damos la posibilidad de tener nuestro panel web embebido en un iframe, de esta forma el cliente puede acoplarlo como una herramienta más dentro de su entorno web. Para añadir EMMA embebido a tu web necesitas lo siguiente:

- Un javascript y un iframe en el html de tu web.
- Token/s de indentificación de usuario/s.

# Obtener tokens de usuario

Para obtener los tokens de usuario es necesario tener creado el usuario dentro de EMMA y este debe estar asociado a una app especifica. Si se elimina el usuario desde el panel de EMMA automáticamente el token generado queda invalidado. 

**Una vez el usuario esté creado el token se solicita a través de una petición a soporte@emma.io.**

# Integración
Para asociar el token de identificación a la sesión generada en el iframe, es necesario utilizar el paso de mensajes entre dominios que ofrece javascript. Para ello en el código javascript de la web primero definimos la función postMessage con el token a autenticar.

``` javascript
 var ORIGIN = 'https://ng.emma.io';
 function iframeSendMessage(event) {
    event.target.contentWindow.postMessage({ emmaToken: '<AUTH TOKEN>' }, ORIGIN);
 }
````

Una vez obtenido el token y generada la función a lanzar para pasar el mensaje entre dominios, creamos el iframe también en el código javascript. El iframe debe apuntar al dominio de emma en la ruta de auto-login para poder validar el token de autenticación, también debe llevar el parámetro embed=true para que la web muestre el estilo embebido.

``` javascript
let iframe = document.createElement('iframe');
iframe.width = "1200";
iframe.height = "900";
iframe.src="https://ng.emma.io/es/auto-login?embed=true";
iframe.addEventListener('load', iframeSendMessage, { once: true });
document.body.appendChild(iframe);
```

En este caso cuando el iframe esté cargado se enviará automaticamente el mensaje con el token, importante que el evento solo se lance una vez para ello hay que marcar el evento como “once”: true. Si el token es válido, el dashboard automaticamente redirigirá al usuario a la pantalla principal.

![embeded_emma_test.png](/web/embeded_emma_test.png)
