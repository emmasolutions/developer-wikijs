---
title: EMMA REST API
description: Conexión a la API de EMMA
published: true
date: 2021-08-09T09:49:48.919Z
tags: api, rest
editor: markdown
dateCreated: 2021-08-06T09:41:28.491Z
---

# Conexión a la API

La API de EMMA utiliza el protocolo de autentificación OAuth2. La autentificación se realiza obteniendo un token con un usuario que tenga acceso a las apps con las que queremos interactuar mediante la API.

## URL de la API
La URL base de la api es `https://ws.emma.io`

## Obtener el EMMA Key

Antes de realizar la conexión con la API de EMMA debemos conocer el `API Key` de nuestra aplicación.

Consulta cobo obtener el EMMA Key en https://docs.emma.io/es/configuracion#general-emma-key

## Obtener el token de sesión

Para obtener el token primero deberemos realizar una llamada al endpoint `/oauth2/login` con los siguientes parámetros:

```json
{
  "username": "username",
  "password": "password",
}
```

Ejemplo de la llamada mediante `cURL`

```bash
curl -X POST "https://ws.emma.io/oauth2/login" -H "Content-Type:application/json" -d "{\"username\": \"your-username\", \"password\": \"your-password\"}"

```

Ejemplo de respuesta:

```json
{
  "access_token":"KCREUKQ01PzIlobcmxCtLJAjGC5VB8jiDD36ceHW1aXjpmnHTk4A64E7eyiT5CXAznuId8mFHtQEk3H68gY3YLfWu0FsTO5VPlc9GFOhZIv4IuPHRyo7fumkZJKILfwEaHcR4MYMOvoqtVdVnnjpRSSFv8j5HoZFyUIo9KU9UNsVqVqE4XH1MxTQGkpcmxbuwh4JFzhlfEEmISK17E80Fvml0HnFS4Yr92gOrOvMdSYmYtnC5BrL9evWpSSxgVHI",  
  "refresh_token":"4LVP2Bm0r8o4VDIOcdpzS2AuAEUdcjCDXBJjHCw99PXXcYnia4xN4DGGcAnVZHVlEKlBhPXInDlSsf5wtM7Rc3yJIqrQ6VpoOyUGAwHRplB65u3UEg4NS6DM1LAPXlzBIGqIz82CE6KMw6XUJJINMCpkGN8l32bTEfb87lyopHcRHT0lsvv2zpCibd3TbXvukI7kK6opnXnmxSYKiMloL4Lc9L8xbH9nideVq28FqUAJ1RNzQKsoSGuilUlz4h2M",
  "expires_in":"2021-08-06 11:33:05",
  "token_type":"Bearer"
}
```

## Autentificar una llamada a la api

Para las llamadas autentificadas de la API deberemos enviar el header `apiKey` con el contenido de nuestra API Key y el header `Authorization` con el contenido del `token_type` y `access_token` que hemos obtenido de la llamada de `/oauth2/login`

Ejemplo:
```
Authorization: Bearer hLOQPeU3fXBdq45LiMgS3Q3L2BcjK9BzSNtSKdHK6gcF0oh5fhCGqjGNjwTAMNCjNbNMOkoupmHy5ifCAvb11PcXJsRmucAVcOfIftFDIQRAY2e6dhb21XHG6duKs30YMhAhWb8HaH1RK9hHA1DKhX6FjznATT7hTm28whAU0zpWQCq8jiT9mHY2Zp2FhraJGoAn90VBaDsRR80jAJrQ7IPKX3hoMFXs5fVLXP2krzHp01FSnfFKb0JEbaz9iFgB
```

### Ejemplo para enviar un evento server 2 server (cURL)

```bash
curl --location --request POST 'https://ws.emma.io/user/events' \
--header 'apiKey: 0051718341426c8c3dd2cdc7e155d44f' \
--header 'Authorization: Bearer hLOQPeU3fXBdq45LiMgS3Q3L2BcjK9BzSNtSKdHK6gcF0oh5fhCGqjGNjwTAMNCjNbNMOkoupmHy5ifCAvb11PcXJsRmucAVcOfIftFDIQRAY2e6dhb21XHG6duKs30YMhAhWb8HaH1RK9hHA1DKhX6FjznATT7hTm28whAU0zpWQCq8jiT9mHY2Zp2FhraJGoAn90VBaDsRR80jAJrQ7IPKX3hoMFXs5fVLXP2krzHp01FSnfFKb0JEbaz9iFgB' \
--header 'Content-Type: application/json' \
--data-raw '[
  {
    "token": "126tgsl9lm6402n42234",
    "attributes": {
      "spend": "20",
      "product": "red jacket"
    },
    "created_at": "2020-03-30 14:45:42",
    "emma_id": 5215,
    "device_id": "439e5015-xxxx-46ad-xxxx-08a30c85xxxx",
    "customer_id": "user123"
  }
]'
```
