---
title: Perfil del usuario en EMMA
description: Descripción de la respuesta de la llamada del user info. Valores del perfil del usuario
published: true
date: 2024-10-01T07:47:52.648Z
tags: 
editor: markdown
dateCreated: 2021-07-15T11:12:20.877Z
---

# Campos del perfil de usuario (User Info)
> `id`	 El mismo ID único que en el método (getUserID)

> `udid`	ID único por instalación de la app (IDFA/AAID habitualmente)

> `emma_build`	 Identifica la versión del SDK de EMMA que se está usando

> `email`	Email del usuario en caso de que el registro o el login estén integrados

> `customerid`	 ID asignado al usuario tras el registro o login

> `device`	 Modelo del dispositivo del usuario

> `app_version`	 Versión de la app

> `os_version`	 Versión del sistema operativo

> `os_build`	 Build del SO

> `token`	 Identificador del push (token)

> `latitude`	Latitud del usuario en el caso de que se permita y mida la geolocalización

> `longitude`	Longitud del usuario en el caso de que se permita y mida la geolocalización

> `emma_carrier`	 Operador de telefonía

> `emma_connection`	 Tipo de conexión (wifi o 3g)

> `emma_city`	Ciudad del usuario en el caso de que se permita y mida la geolocalización

> `emma_country`	País del usuario en el caso de que se permita y mida la geolocalización

> `emma_state`	País del usuario en el caso de que se permita y mida la geolocalización

> `emma_accept_language` 	 Lenguaje establecido en la app

> `emma_timezone_name`	 TimeZone del dispositivo

> `{tagX....tagY}`	Tags de usuario que se traquean en la llama trackExtraUserInfo

> `eat_sub{x}`	Valor que recoge el parámetro eat_sub{x} si se usa en un powlink

# Información sobre la atribución del usuario

## Descripción de los campos de atribución del usuario

| Nombre                | Tipo     | Descripción                                                                                                                                                                                                                                                           |
|------------------------|:--------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `status`                 | String   | Nos devuelve el estado de la atribución. Éste puede tener varios tipos: 
|  |   | - **pending:** Pendiente de atribución (ventana de 2 días).
|  |   | - **campaign:** Atribuida a una campaña específica. 
|  |   | - **organic:** Atribución orgánica (no vinculada a campaña)
| `campaign.id`           | Number   | Id de la campaña.                                                                                                                                               
| `campaign.name`         | String   | Nombre de la campaña.                                                                                                                                                                                                                                                        |
| `campaign.clickParams`       | Dictionary| Parámetros del click.                                                                                                                                                                                                                                                        |
| `campaign.source.id`              | Number   | Id de la fuente.                                                                                                                                                                                                                                                           |
| `campaign.source.name`           | String   | Nombre de la fuente.                                                                                                                                                                                                                                                         |
| `campaign.source.channel`           | String   | Nombre del canal. Disponible a partir de la versión del SDK 4.15.0.                                                                                                                                                                                                                                                         |
| `campaign.source.params`          | Dictionary   | Parámetros estáticos añadidos en la fuente. Disponible a partir de la versión del SDK 4.15.0.                                               
| `campaign.source.provider.id` | Number | Id del proveedor
| `campaign.source.provider.name` | String | Nombre del proveedor


